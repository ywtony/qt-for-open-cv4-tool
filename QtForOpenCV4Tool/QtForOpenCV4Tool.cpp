#include "QtForOpenCV4Tool.h"

QtForOpenCV4Tool::QtForOpenCV4Tool(QWidget* parent)
	: QMainWindow(parent)
{

	qRegisterMetaType<cv::Mat>("cv::Mat");
	qRegisterMetaType<cv::Mat*>("cv::Mat*");
	ui.setupUi(this);
	this->resize(QSize(320, 480));
	this->setWindowTitle("OpenCV Tool");
	this->setWindowIcon(QIcon("images/opencv.png"));
	createListView();
}

void QtForOpenCV4Tool::createListView() {
	listView = new CommonListView(this);
	connect(listView, &CommonListView::itemClicked, [=](QListWidgetItem* item) {
		CommonListViewItem* item2 = dynamic_cast<CommonListViewItem*>(item);
		qDebug() << "itemPos:" << item2->mPos;
		switch (item2->mPos) {
		case 8://基础操作
			baseOptionWindow.show();
			break;
		case 9://图像基本操作案例
			imageBasicOptionExampleWindow.show();
			break;
		case 0://特征检测
			featuresDetectionWindow.show();
			break;
		case 1://图像分割
			imageSegmentationWindow.show();
			break;
		case 2://级联分类器的使用与训练
			harrLbpWindow.show();
			break;
		case 3:
			videoAnalysisAndObjectTrackWindow.show();
			break;
		case 4:
			aiWindow.show();
			break;
		case 5:
			faceRecognizer.show();
			break;
		case 6:
			videoPlayerWindow.show();
			break;
		case 7:
			customExampleListWindow.show();
			break;
		case 10://网络列表相关
			networkWindow.show();
			break;
		case 11:
			imageCompressTool.show();
			break;
		case 12://opengl相关
			openGLSampleWindow.show();
			break;
		case 13://ffmpeg相关
			fFMpegWindow.show();
			break;
		}
		});
	new CommonListViewItem("基础操作", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 8, listView);
	new CommonListViewItem("图像基础操作案例", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 9, listView);
	new CommonListViewItem("特征检测", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 0, listView);
	new CommonListViewItem("图像分割", QIcon("images/images_cli.png"), QIcon("images/images_cli.png"), 1, listView);
	new CommonListViewItem("级联分类器", QIcon("images/jilianfenleiqi.png"), QIcon("images/jilianfenleiqi.png"), 2, listView);
	new CommonListViewItem("视频分析与对象跟踪", QIcon("images/object_genzong.png"), QIcon("images/object_genzong.png"), 3, listView);
	new CommonListViewItem("AI人工智能算法", QIcon("images/ai_suanfa.png"), QIcon("images/ai_suanfa.png"), 4, listView);
	new CommonListViewItem("人脸识别", QIcon("images/face_shibie.png"), QIcon("images/face_shibie.png"), 5, listView);
	new CommonListViewItem("视频编辑器", QIcon("images/video_edit.png"), QIcon("images/video_edit.png"), 6, listView);
	new CommonListViewItem("自定义组件", QIcon("images/video_edit.png"), QIcon("images/video_edit.png"), 7, listView);
	new CommonListViewItem("Qt网络编程", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 10, listView);
	new CommonListViewItem("图片压缩工具", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 11, listView);
	new CommonListViewItem("OpenGL", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 12, listView);
	new CommonListViewItem("FFMPEG相关案例", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 13, listView);

}

QtForOpenCV4Tool::~QtForOpenCV4Tool()
{}
