#include "image_segmentation_window.h"

Image_Segmentation_Window::Image_Segmentation_Window(QWidget* parent)
	: QWidget(parent)
{
	this->setFixedSize(QSize(320, 480));
	createListView();
}

void Image_Segmentation_Window::createListView() {
	listView = new CommonListView(this);
	connect(listView, &CommonListView::itemClicked, [=](QListWidgetItem* item) {
		CommonListViewItem* item2 = dynamic_cast<CommonListViewItem*>(item);
		qDebug() << "itemPos:" << item2->mPos;
		switch (item2->mPos) {
		case 0:
			colorsClassification.show();
			break;
		case 1:
			gmmImageDivision.show();
			break;
		case 2:
			watershedDivision.show();
			break;
		case 3:
			watershedImageDivision.show();
			break;
		case 4://抠图
			grabCutMatting.show();
			break;
		case 5://证件照背景替换小案例
			idPhotoBackgroundReplacement.show();
			break;
		case 6://视频绿幕背景替换
			videoGreenCurtainBackgroundReplacement.show();
			break;
		}
		});
	new CommonListViewItem("K-Means", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 0, listView);
	new CommonListViewItem("GMM", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"),1, listView);
	new CommonListViewItem("分水岭算法实现图像计数", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 2, listView);
	new CommonListViewItem("分水岭算法实现图像分割", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 3, listView);
	new CommonListViewItem("grabCut抠图", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 4, listView);
	new CommonListViewItem("证件照背景替换小案例", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 5, listView);
	new CommonListViewItem("视频绿幕背景替换", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 6, listView);
}
