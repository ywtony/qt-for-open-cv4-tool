#ifndef AI_IMAGE_DISTINGUISH_H
#define AI_IMAGE_DISTINGUISH_H

#include <QWidget>
#include "../../common/CommonGraphicsView.h"
#include <iostream>
#include <fstream>
#include <opencv2/dnn.hpp>

using namespace std;
using namespace dnn::dnn4_v20210301;

/**
 * 机器学习--->图像识别
 * @brief The AI_Image_Distinguish class
 */
class AI_Image_Distinguish : public CommonGraphicsView
{
    Q_OBJECT
public:
    explicit AI_Image_Distinguish(QWidget *parent = nullptr);
    void showAiImageDistinguish(const char* filePath);
    vector<String> readLabels();
protected:
    void dropEvent(QDropEvent *event) override;
private:
    QString path;
    QString labelFilePath;

signals:

};

#endif // AI_IMAGE_DISTINGUISH_H
