#ifndef AI_GOOGLE_NET_IMAGE_DIVISION_H
#define AI_GOOGLE_NET_IMAGE_DIVISION_H

#include <QWidget>
#include "../../common/CommonGraphicsView.h"
#include <opencv2/dnn.hpp>
#include <iostream>
#include <string>
#include <QFile>
#include <QTextStream>
#include <fstream>

using namespace dnn::dnn4_v20210301;
using namespace std;


/**
 * 使用GoogleNet实现图像分割
 * @brief The AI_Google_Net_Image_Division class
 */
class AI_Google_Net_Image_Division : public CommonGraphicsView
{
    Q_OBJECT
public:
    explicit AI_Google_Net_Image_Division(QWidget *parent = nullptr);
    void showImageClassificatio(const char * filePath);
    /**
     * 读取分类标签
     * @brief readLabels
     * @return
     */
    vector<String> readLabels();
protected:
    void dropEvent(QDropEvent * event) override;
private:
    QString path;
    QString modelPath;//模型文件
    QString prototxtPath;//描述文件
    QString labelPath;//标签文件

signals:

};

#endif // AI_GOOGLE_NET_IMAGE_DIVISION_H
