#include "ai_image_distinguish.h"

AI_Image_Distinguish::AI_Image_Distinguish(QWidget *parent)
    : CommonGraphicsView{parent}
{
    this->setWindowTitle("AI--->图像识别");
    labelFilePath = "asdfasdf";//标签文件路径

}


void AI_Image_Distinguish::dropEvent(QDropEvent *event){
    path = event->mimeData()->urls().at(0).toLocalFile();
    showAiImageDistinguish(path.toStdString().c_str());
}


void AI_Image_Distinguish::showAiImageDistinguish(const char *filePath){
    Mat src = imread(filePath);
    if(src.empty()){
        qDebug()<<"图形为空";
        return;
    }
    imshow("src",src);


}

/**
 * 获取标签数组
 * @brief AI_Image_Distinguish::readLabels
 * @return
 */
vector<String> AI_Image_Distinguish::readLabels(){
    vector<String> objNames;
    ifstream fp;
    fp.open(labelFilePath.toStdString().c_str());
    if(!fp.is_open()){
        qDebug()<<"不能打开标签文件";
    }

    string name;
    while(!fp.eof()){
        getline(fp,name);//读取一行数据
        if (name.length() && (name.find("display_name:") == 0)) {
            string temp = name.substr(15);
            temp.replace(temp.end() - 1, temp.end(), "");
            objNames.push_back(temp);
        }

    }
    return objNames;

}

