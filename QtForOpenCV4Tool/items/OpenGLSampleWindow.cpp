#include "OpenGLSampleWindow.h"

OpenGLSampleWindow::OpenGLSampleWindow(QWidget* parent)
	: QMainWindow(parent)
{
	this->resize(QSize(320, 480));
	this->setWindowTitle("OpenCV Tool");
	this->setWindowIcon(QIcon("images/opencv.png"));
	createListView();
}

void OpenGLSampleWindow::createListView() {
	listView = new CommonListView(this);
	connect(listView, &CommonListView::itemClicked, [=](QListWidgetItem* item) {
		CommonListViewItem* item2 = dynamic_cast<CommonListViewItem*>(item);
		qDebug() << "itemPos:" << item2->mPos;
		switch (item2->mPos) {
		case 1://基础操作
			glTriangleWindow.show();
			break;
		case 2:
			glSquareWindow.show();
			break;
		case 3:
			glTextureWindow.show();
			break;
		case 4:
			glTwoTriangleWindow.show();
			break;
		case 5:
			glTwoVAOTriangleWindow.show();
			break;
		case 6:
			glTwoShaderTriangleWindow.show();
			break;
		case 7:
			glColorDataFromVertexWindow.show();
			break;
		case 8:
			glTextureGirlAndFaceWindow.show();
			break;
		case 9:
			glTextureColorWindow.show();
			break;
		case 10:
			glTextureMixWindow.show();
			break;
		case 11:
			glTextureTranslateRotateScaleWindow.show();
			break;
		case 12:
			glTextureMvpTestWindow.show();
			break;
		case 13:
			glTextureMVP3DWindow.show();
			break;
		}
		});
	new CommonListViewItem("三角形", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 1, listView);
	new CommonListViewItem("正方形", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 2, listView);
	new CommonListViewItem("纹理", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 3, listView);
	new CommonListViewItem("一个顶点数组绘制两个三角形", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 4, listView);
	new CommonListViewItem("两个VAO操作两个三角形", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 5, listView);
	new CommonListViewItem("使用两个Shader绘制两个不同颜色的三角形", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 6, listView);
	new CommonListViewItem("从顶点着色器传递颜色数据到片元着色器", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 7, listView);
	new CommonListViewItem("纹理贴图，美女和笑脸", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 8, listView);
	new CommonListViewItem("贴图与颜色混合", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 9, listView);
	new CommonListViewItem("纹理融合", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 10, listView);
	new CommonListViewItem("纹理的平移、旋转、缩放", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 11, listView);
	new CommonListViewItem("平面纹理+MVP测试", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 12, listView);
	new CommonListViewItem("第一个3D纹理", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 13, listView);

}

OpenGLSampleWindow::~OpenGLSampleWindow()
{
}
