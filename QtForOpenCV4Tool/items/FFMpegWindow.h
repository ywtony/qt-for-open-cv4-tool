#pragma once

#include <QtWidgets/QMainWindow>
#include "../common/CommonListView.h"
#include "../common/CommonListView.h"
#include <QSize>
#include <QIcon>
#include "ffmpeg/FFMpegSamplePlayerWindow.h"
#include "ffmpeg/FFMpegDemuxerWindow.h"
#include "ffmpeg/XVideoWindow.h"
#include "ffmpeg/xplayer/XVideoPlayerWindow.h"
#include "ffmpeg/xplayer/XPlay2.h"
#include "ffmpeg/FFMpegRecordInterfaceWindow.h"
#include "ffmpeg/rtmp_pusher/RtmpPusherWindow.h"
#include "ffmpeg/rtmp_pusher/OpenCVRtspToRtmpWindow.h"
#include "ffmpeg/rtmp_pusher_audio/UseQtRecordAudioWindow.h"
#include "ffmpeg/rtmp_pusher_audio/RtmpPushAudioUseFFmpegWindow.h"

#include <QMetaType>

class FFMpegWindow : public QMainWindow
{
	Q_OBJECT

public:
	FFMpegWindow(QWidget *parent = nullptr);
	~FFMpegWindow();

public:
	void createListView();//创建一个ListView

private:
	CommonListView* listView;
	FFMpegSamplePlayerWindow fFMpegSamplePlayerWindow;
	FFMpegDemuxerWindow fFMpegDemuxerWindow;
	XVideoWindow xVideoWindow;
	XVideoPlayerWindow xVideoPlayerWindow;
	//xplayer
	XPlay2 xPlay2;
	//封装
	FFMpegRecordInterfaceWindow fFMpegRecordInterfaceWindow;
	//rtmp推流
	RtmpPusherWindow rtmpPusherWindow;
	//利用opencv将rtsp转rtmp推流
	OpenCVRtspToRtmpWindow openCVRtspToRtmpWindow;
	//利用qt录制rtmp音频小案例
	UseQtRecordAudioWindow useQtRecordAudioWindow;
	//利用qt录制音频，使用ffmpeg将音频推送至rtmp服务器
	RtmpPushAudioUseFFmpegWindow rtmpPushAudioUseFFmpegWindow;
};
