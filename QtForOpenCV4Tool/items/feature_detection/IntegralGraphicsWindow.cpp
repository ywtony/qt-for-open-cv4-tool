#include "IntegralGraphicsWindow.h"

IntegralGraphicsWindow::IntegralGraphicsWindow(QWidget* parent)
	: BaseSceneView(parent)
{
	this->setWindowTitle("积分图");
	this->setFixedSize(1000, 480);
	this->setGraphicsViewSize(1000, 480);


	//选择图片
	ChoiceImageWidget* choiceImageWidget = new ChoiceImageWidget(this);
	choiceImageWidget->setFixedWidth(200);
	choiceImageWidget->setCallback([=](QString filePath) {
		this->filePath = filePath;
		qDebug() << "选择图片完成开始打印路径：" << filePath;
		this->execute();
		});


	//添加显示图片
	imageTips[0] = new ImageTip(this);
	imageTips[0]->setItemsTitle("积分图");
	imageTips[0]->resizeWH(this->width(), this->height());

	QHBoxLayout* hLayout = new QHBoxLayout(this);
	QVBoxLayout* vLayout = new QVBoxLayout(this);


	vLayout->addWidget(choiceImageWidget);
	vLayout->setAlignment(Qt::AlignTop);

	QVBoxLayout* vLayout2 = new QVBoxLayout(this);
	QHBoxLayout* hTopLayout = new QHBoxLayout(this);
	QHBoxLayout* hTopLayout2 = new QHBoxLayout(this);
	hTopLayout->addWidget(imageTips[0]);
	vLayout2->addLayout(hTopLayout);
	//vLayout2->addLayout(hTopLayout2);


	hLayout->addLayout(vLayout, 1);
	hLayout->addLayout(vLayout2, 3);

}

QPixmap IntegralGraphicsWindow::handle() {
	Mat src = imread(filePath.toStdString().c_str());
	cv::resize(src, src, cv::Size(src.cols / 4, src.rows / 4));
	//imshow("src", src);
	//积分图像：宽高必须为（width+1）*（height+1），且必须为32位或64位浮点数
	Mat sum;
	//平方像素值的积分图像，他的宽高必须是（width+1）*(height+1)且为64位浮点数
	Mat sqsum;
	integral(src, sum, sqsum);//计算积分图像及平方像素值积分图像
	//
	Mat result;
	//将平方图像归一化0~255之间
	normalize(sum, result, 0, 255, NORM_MINMAX, CV_8UC1, Mat());
	//imshow("result", result);
	imageTips[0]->setItemsPixmap(ImageUtils::getPixmap8(result, src.cols , src.rows ));
	//imageTips[1]->setItemsPixmap(ImageUtils::getPixmap8(result, src.cols, src.rows));
	return NULL;
}

IntegralGraphicsWindow::~IntegralGraphicsWindow()
{
}
