#pragma once

#include <QWidget>
#include <iostream>
#include "opencv2/opencv.hpp"
using namespace std;
using namespace cv;

/*
* 角点检测
*/
class CornerHarris : public QWidget
{
	Q_OBJECT

public:
	CornerHarris(QWidget *parent = nullptr);
	~CornerHarris();

private:
    Mat src, gray;
    int thresh = 130;//阀值
    int max_count = 255;//最大像素
    int blockSize = 2;//角点检测中窗口大小
    int ksize = 3;//sobel算子大小
    double k = 0.04;//取值范围一般在[0.04~0.06]
public:
    void showCornerHarris(const char* filePath, Mat& result);
    /**
     * 更新角点检测参数
     * @brief updateParams 方法名
     * @param thresh 阀值，大于此值的值会被保留，并当做角点
     * @param blockSize 角点检测中窗口的大小
     * @param ksize sobel算子的大小 3，5，7
     * @param k 取值范围一般是0.04~0.06
     */
    void updateParams(int thresh, int blockSize, int ksize, double k);
};
