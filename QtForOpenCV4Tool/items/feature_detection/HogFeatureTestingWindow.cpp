#include "HogFeatureTestingWindow.h"

HogFeatureTestingWindow::HogFeatureTestingWindow(QWidget* parent)
	: BaseSceneView(parent)
{

	this->setWindowTitle("Hog特征检测，检测行人");
	this->setFixedSize(1000, 480);
	this->setGraphicsViewSize(1000, 480);


	//选择图片
	ChoiceImageWidget* choiceImageWidget = new ChoiceImageWidget(this);
	choiceImageWidget->setFixedWidth(200);
	choiceImageWidget->setCallback([=](QString filePath) {
		this->filePath = filePath;
		qDebug() << "选择图片完成开始打印路径：" << filePath;
		testHog();
		this->execute();
		});


	//添加显示图片
	imageTips[0] = new ImageTip(this);
	imageTips[0]->setItemsTitle("行人检测");
	imageTips[0]->resizeWH(this->width(), this->height());

	QHBoxLayout* hLayout = new QHBoxLayout(this);
	QVBoxLayout* vLayout = new QVBoxLayout(this);


	vLayout->addWidget(choiceImageWidget);
	vLayout->setAlignment(Qt::AlignTop);

	QVBoxLayout* vLayout2 = new QVBoxLayout(this);
	QHBoxLayout* hTopLayout = new QHBoxLayout(this);
	QHBoxLayout* hTopLayout2 = new QHBoxLayout(this);
	hTopLayout->addWidget(imageTips[0]);
	vLayout2->addLayout(hTopLayout);
	//vLayout2->addLayout(hTopLayout2);


	hLayout->addLayout(vLayout, 1);
	hLayout->addLayout(vLayout2, 3);

}
void HogFeatureTestingWindow::testHog() {

}
QPixmap HogFeatureTestingWindow::handle() {
	Mat src = imread(filePath.toStdString().c_str());
	cv::resize(src, src, cv::Size(src.cols / 2, src.rows / 2));
	Mat gray;
	cvtColor(src, gray, COLOR_BGR2GRAY);
	//hog+svm实现行人检测
	HOGDescriptor hog = HOGDescriptor();
	hog.setSVMDetector(hog.getDefaultPeopleDetector());//使用hog svm实现行人检测
	//多尺度空间检测
	vector<Rect> foundLocations;//检测到行人的矩形区域
	hog.detectMultiScale(src, foundLocations);//后面的参数使用默认就行，这是opencv的经验值
	//hog.detectMultiScale(src, foundLocations, 0, Size(1, 1), Size(8, 8), 1.05, 2);
	//将行人矩形框绘制出来 
	Mat result = src.clone();
	for (size_t i = 0;i < foundLocations.size();i++) {
		rectangle(result, foundLocations[i], Scalar(0, 0, 255), 2, 8, 0);
	}
	imageTips[0]->setItemsPixmap(ImageUtils::getPixmap(result, src.cols / 2, src.rows / 2));
	//imshow("result", result);





/*Mat mats[8];
imageTips[0]->setItemsPixmap(ImageUtils::getPixmap(mats[0], 200, 200));
imageTips[1]->setItemsPixmap(ImageUtils::getPixmap8(mats[1], 200, 200));
imageTips[2]->setItemsPixmap(ImageUtils::getPixmap8(mats[2], 200, 200));
imageTips[3]->setItemsPixmap(ImageUtils::getPixmap(mats[3], 200, 200));
imageTips[4]->setItemsPixmap(ImageUtils::getPixmap(mats[4], 200, 200));
imageTips[5]->setItemsPixmap(ImageUtils::getPixmap8(mats[5], 200, 200));
imageTips[6]->setItemsPixmap(ImageUtils::getPixmap8(mats[6], 200, 200));
imageTips[7]->setItemsPixmap(ImageUtils::getPixmap(mats[7], 200, 200));*/
	return NULL;
}

HogFeatureTestingWindow::~HogFeatureTestingWindow()
{
}
