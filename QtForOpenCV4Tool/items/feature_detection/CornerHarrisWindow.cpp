#include "CornerHarrisWindow.h"

void runFun(CornerHarrisWindow* window) {
	qDebug() << "显示角点";
	window->showCornerHarris();
}

CornerHarrisWindow::CornerHarrisWindow(QWidget* parent)
	: BaseSceneView(parent)
{
	this->setWindowTitle("角点检测(cornerHarris)");
	cornerHarris = new CornerHarris();
	QLabel* blockSizeLabel = new QLabel(this);
	blockSizeLabel->setText("block_size：");
	blockSizeLabel->setFixedSize(QSize(80, 30));
	//创建一个输入框
	edit_blockSize = new QLineEdit(this);
	edit_blockSize->setPlaceholderText("请输入block_size");
	edit_blockSize->setText("2");
	edit_blockSize->setFixedSize(QSize(50, 30));
	edit_blockSize->move(blockSizeLabel->x() + blockSizeLabel->width() + 10, 0);

	QLabel* kLabel = new QLabel(this);
	kLabel->setText("k");
	kLabel->setFixedSize(QSize(80, 30));
	kLabel->move(0, edit_blockSize->y() + edit_blockSize->height() + 10);
	edit_k = new QLineEdit(this);
	edit_k->setPlaceholderText("请输入k");
	edit_k->setText("0.04");
	edit_k->setFixedSize(QSize(50, 30));
	edit_k->move(kLabel->x() + kLabel->width() + 10, edit_blockSize->y() + edit_blockSize->height() + 10);

	QLabel* kSizeLabel = new QLabel(this);
	kSizeLabel->setText("ksize:");
	kSizeLabel->setFixedSize(QSize(80, 30));
	kSizeLabel->move(0, edit_k->y() + edit_k->height() + 10);
	edit_kSize = new QLineEdit(this);
	edit_kSize->setText("3");
	edit_kSize->setFixedSize(QSize(50, 30));
	edit_kSize->move(kSizeLabel->x() + kSizeLabel->width() + 10, edit_k->y() + edit_k->height() + 10);

	QLabel* threshLabel = new QLabel(this);
	threshLabel->setText("thresh:");
	threshLabel->setFixedSize(QSize(80, 30));
	threshLabel->move(0, edit_kSize->y() + edit_kSize->height() + 10);
	edit_thresh = new QLineEdit(this);
	edit_thresh->setText("130");
	edit_thresh->setFixedSize(QSize(50, 30));
	edit_thresh->move(threshLabel->x() + threshLabel->width() + 10, edit_kSize->y() + edit_thresh->height() + 10);

	btn_submit = new QPushButton(this);
	btn_submit->setText("开始检测");
	btn_submit->move(0, edit_thresh->y() + edit_thresh->height() + 10);

	connect(btn_submit, &QPushButton::clicked, [=]() {
		qDebug() << "您点击了检测按钮:";
		qDebug() << "thresh:" << edit_thresh->text().toInt();
		qDebug() << "block_size:" << edit_blockSize->text().toInt();
		qDebug() << "ksize:" << edit_kSize->text().toInt();
		qDebug() << "k:" << edit_k->text().toDouble();
		this->execute();
		});
}
void CornerHarrisWindow::dropEvent(QDropEvent* event) {
	QString mPath = event->mimeData()->urls().at(0).toLocalFile();
	qDebug() << mPath;
	QFileInfo file(mPath);
	filePath = file.absoluteFilePath();
	this->execute();
}
/**
 * 角点检测
 * @brief CornerHarrisWindow::showCornerHarris
 */
QPixmap CornerHarrisWindow::showCornerHarris() {
	cornerHarris->updateParams(edit_thresh->text().toInt(),
		edit_blockSize->text().toInt(),
		edit_kSize->text().toInt(),
		edit_k->text().toDouble());
	cornerHarris->showCornerHarris(filePath.toStdString().c_str(), resultImage);
	QImage image = ImageUtils::matToQImage(resultImage);
	QPixmap pixmap = QPixmap::fromImage(image);
	return pixmap;
}

QPixmap CornerHarrisWindow::handle() {
	return showCornerHarris();
}

CornerHarrisWindow::~CornerHarrisWindow()
{

}
