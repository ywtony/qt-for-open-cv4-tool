#include "CornerHarris.h"

CornerHarris::CornerHarris(QWidget *parent)
	: QWidget(parent)
{

}
void CornerHarris::showCornerHarris(const char* filePath, Mat& resultImg) {
    src = imread(filePath);
    if (src.empty()) {
        cout << "图片不能为空" << endl;
        return;
    }
    //    imshow("src",src);
    cvtColor(src, gray, COLOR_BGR2GRAY);
    //    imshow("gray",gray);
    Mat dst, norm_dst, normScaleDst;
    cornerHarris(gray, dst, blockSize, ksize, k, BORDER_DEFAULT);//dst输出参数一般32位浮点
    //将dst数据归一化到0~255之间
    normalize(dst, norm_dst, 0, 255, NORM_MINMAX, CV_32FC1, Mat());
    //    imshow("normalize",norm_dst);
        //取绝对值
    convertScaleAbs(norm_dst, normScaleDst);
    resultImg = src.clone();
    for (int row = 0; row < resultImg.rows; row++) {
        uchar* currentRow = normScaleDst.ptr(row);
        for (int col = 0; col < resultImg.cols; col++) {
            int value = (int)*currentRow;

            if (value > thresh) {
                circle(resultImg, Point(col, row), 2, Scalar(0, 0, 255), 2, LINE_8, 0);
            }
            currentRow++;//用指针速度快
        }
    }
    //    imshow("result",resultImg);

}

void CornerHarris::updateParams(int thresh, int blockSize, int ksize, double k) {
    this->thresh = thresh;
    this->blockSize = blockSize;
    this->ksize = ksize;
    this->k = k;
}

CornerHarris::~CornerHarris()
{

}
