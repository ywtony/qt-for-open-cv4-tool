#include "PlaneDetectionWindow.h"

PlaneDetectionWindow::PlaneDetectionWindow(QWidget *parent)
	: BaseSceneView(parent)
{
	this->setWindowTitle("平面检测");
	this->setFixedSize(1000, 480);
	this->setGraphicsViewSize(1000, 480);


	//选择图片
	ChoiceImageWidget* choiceImageWidget = new ChoiceImageWidget(this);
	choiceImageWidget->setFixedWidth(200);
	choiceImageWidget->setCallback([=](QString filePath) {
		this->filePath = filePath;
		qDebug() << "选择图片完成开始打印路径：" << filePath;
		this->execute();
		});



	QHBoxLayout* hLayout = new QHBoxLayout(this);
	QVBoxLayout* vLayout = new QVBoxLayout(this);


	vLayout->addWidget(choiceImageWidget);
	vLayout->setAlignment(Qt::AlignTop);

	QVBoxLayout* vLayout2 = new QVBoxLayout(this);
	QHBoxLayout* hTopLayout = new QHBoxLayout(this);
	QHBoxLayout* hTopLayout2 = new QHBoxLayout(this);
	vLayout2->addLayout(hTopLayout);
	//vLayout2->addLayout(hTopLayout2);


	hLayout->addLayout(vLayout, 1);
	hLayout->addLayout(vLayout2, 3);

}


// 判断四个点是否构成矩形
bool isRectangle(const std::vector<cv::Point>& points) {
	double d1 = cv::norm(points[0] - points[1]);
	double d2 = cv::norm(points[1] - points[2]);
	double d3 = cv::norm(points[2] - points[3]);
	double d4 = cv::norm(points[3] - points[0]);
	double d5 = cv::norm(points[0] - points[2]);
	double d6 = cv::norm(points[1] - points[3]);

	double eps = 5.0; // 允许的误差范围

	return (std::abs(d1 - d2) < eps && std::abs(d2 - d3) < eps && std::abs(d3 - d4) < eps && std::abs(d4 - d1) < eps &&
		std::abs(d5 - d6) < eps);
}

QPixmap PlaneDetectionWindow::handle() {
	Mat src = imread(filePath.toStdString().c_str());
	cv::Mat gray;
	cv::cvtColor(src, gray, cv::COLOR_BGR2GRAY);
	cv::Mat edges;
	cv::Canny(gray, edges, 50, 150);

	std::vector<std::vector<cv::Point>> contours;
	cv::findContours(edges, contours, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE);

	for (const auto& contour : contours) {
		std::vector<cv::Point> approx;
		cv::approxPolyDP(contour, approx, cv::arcLength(contour, true) * 0.02, true);

		if (approx.size() == 4) {
			if (isRectangle(approx)) {
				cv::drawContours(src, std::vector<std::vector<cv::Point>>{approx}, -1, cv::Scalar(0, 255, 0), 2);
			}
		}
	}

	cv::imshow("Detected Rectangles", src);
	cv::waitKey(0);














	//// 创建 ORB 特征检测器
	//cv::Ptr<cv::FeatureDetector> detector = cv::ORB::create();

	//// 检测特征点
	//std::vector<cv::KeyPoint> keypoints;
	//detector->detect(src, keypoints);

	//// 绘制特征点
	//cv::Mat outImage;
	//cv::drawKeypoints(src, keypoints, outImage, cv::Scalar::all(-1), cv::DrawMatchesFlags::DEFAULT);

	//cv::imshow("特征点", outImage);
	//cv::waitKey(0);

	/*qDebug() << "呵呵";
	imshow("src", src);*/
	//cv::resize(src, src, cv::Size(src.cols / 4, src.rows / 4));
	//imshow("src", src);
	//积分图像：宽高必须为（width+1）*（height+1），且必须为32位或64位浮点数
	//Mat sum;
	//平方像素值的积分图像，他的宽高必须是（width+1）*(height+1)且为64位浮点数
	//Mat sqsum;
	//integral(src, sum, sqsum);//计算积分图像及平方像素值积分图像
	//
	//Mat result;
	//将平方图像归一化0~255之间
	//normalize(sum, result, 0, 255, NORM_MINMAX, CV_8UC1, Mat());
	//imshow("result", result);
	//imageTips[0]->setItemsPixmap(ImageUtils::getPixmap8(result, src.cols, src.rows));
	//imageTips[1]->setItemsPixmap(ImageUtils::getPixmap8(result, src.cols, src.rows));
	return NULL;
}

PlaneDetectionWindow::~PlaneDetectionWindow()
{

}
