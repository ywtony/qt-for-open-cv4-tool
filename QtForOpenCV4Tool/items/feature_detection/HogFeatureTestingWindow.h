#pragma once

#include <QWidget>
#include "../../common/base/BaseSceneView.h"
#include "../../common/button/Button.h"
#include "../../common/edittext/EditText.h"
#include "../../common/seekbar/Seekbar.h"
#include "../../common/minus_plus/MinusPlusWidget.h"
#include "../../common/seekbar/SeekBarTipsWidget.h"
#include "../../common/choice_image/ChoiceImageWidget.h"
#include "../../common/image_tip/ImageTip.h"
#include "../../common/layout/VariableGridView.h"

#include <QFileDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QVBoxLayout>
#include <iostream>
#include <QGridLayout>
#include <QButtonGroup>
#include <QRadioButton>
#include <QMetaType> 
#include <QMessageBox>
#include <iostream>

using namespace std;

//Hog特征检测检测行人，并把行人框出来
class HogFeatureTestingWindow : public BaseSceneView
{
	Q_OBJECT

public:
	HogFeatureTestingWindow(QWidget *parent = nullptr);
	~HogFeatureTestingWindow();
	void testHog();

protected:
	QPixmap handle();

private:
	QString filePath;
	ImageTip* imageTips[1];

};
