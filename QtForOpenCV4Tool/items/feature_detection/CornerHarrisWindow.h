#pragma once

#include <QWidget>
#include "CornerHarris.h"
#include "../../common/CommonGraphicsView.h"
#include <QPixmap>
#include <QLineEdit>
#include <QPushButton>
#include <QHBoxLayout>
#include <QLabel>
#include "../../common/base/Handler.h"
#include "../../common/base/BaseSceneView.h"
using namespace std;

class CornerHarrisWindow : public BaseSceneView
{
	Q_OBJECT

public:
	CornerHarrisWindow(QWidget* parent = nullptr);
	~CornerHarrisWindow();

protected:
	QPixmap handle();

private:
	Mat src, gray, resultImage;
	CornerHarris* cornerHarris;
	int thresh = 130;


	QLineEdit* edit_blockSize;
	QLineEdit* edit_kSize;
	QLineEdit* edit_k;
	QLineEdit* edit_thresh;
	QPushButton* btn_submit;
	QString filePath;

public:
	QPixmap showCornerHarris();
	void dropEvent(QDropEvent* event) override;

};
