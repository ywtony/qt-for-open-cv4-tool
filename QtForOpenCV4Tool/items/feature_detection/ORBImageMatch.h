#pragma once

#include <QWidget>
#include "opencv2/opencv.hpp"
#include "../../common/button/Button.h"
#include <QDebug>
#include <QString>
#include <QVBoxLayout>
#include <QSize>
#include <QFileDialog>
#include <QDir>
#include <QStringList>
#include <Qt>

using namespace std;
using namespace cv;

class ORBImageMatch : public QWidget
{
	Q_OBJECT

public:
	ORBImageMatch(QWidget* parent = nullptr);
	~ORBImageMatch();
	void matchImage();
	//批量生成图像特征文件
	void batchGenerateImageCharacteristics(QString directory,QStringList fileList);
	void searchImage();

private:
	QString filePath1;
	QString filePath2;
};
