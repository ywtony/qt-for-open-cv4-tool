#ifndef SURF_WINDOW_H
#define SURF_WINDOW_H

#include <QWidget>
//#include "opencv2/xfeatures2d.hpp"
#include "../../common/CommonGraphicsView.h"

//using namespace cv::xfeatures2d;

class surf_window : public CommonGraphicsView
{
    Q_OBJECT
public:
    explicit surf_window(QWidget *parent = nullptr);
    void dropEvent(QDropEvent *event);
    void showSURF(const char* filePath);
private:
    Mat src,resultImage;

signals:

};

#endif // SURF_WINDOW_H
