#ifndef GOODFEATURESTOTRACK_H
#define GOODFEATURESTOTRACK_H

#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include "../../common/base/BaseSceneView.h"
#include <iostream>
using namespace std;

/**
 * 更好的角点检测
 * @brief The GoodFeaturesToTrack class
 */
class GoodFeaturesToTrack : public BaseSceneView
{
    Q_OBJECT
public:
    explicit GoodFeaturesToTrack(QWidget *parent = nullptr);
protected:
    void dropEvent(QDropEvent *event) override;
    QPixmap showGoodFeaturesToTrack();
    /**
     * 更新角点检测参数
     * @brief updateParams
     * @param maxCorners 检测的最大角点数，输出检测信号最强的前maxCorners个角点
     * @param quality_level 质量水平系数一般是从0.01~1
     * @param min_distance 用于区分相邻两个角点之间的距离，小鱼这个距离的角点会被忽略
     * @param block_size 卷积核大小，ps：可以根据分辨率动态调整，如果是大分辨率可以适当调整大一点，必须是奇数
     */
    void updateParams(double min_distance,int block_size);
    QPixmap handle();
private:
    Mat src,gray;
    QPushButton * btnLevelPlus,*btnLevelMinus;
    QLineEdit *min_distanceEidt,*block_sizeEdit;
    int maxCorners =100;//检测强度最强的100个角点
    double quality_level = 0.01;//质量水平系数一般是从0.01到1之间
    double min_distance = 3.0;//用于区分相邻两个角点之间的最小距离
    int block_size = 3;//卷积核的大小，ps：可以根据分辨率动态调整，如果是大分辨率可以适当调整大一点，必须是奇数
    bool use_harris = false;//是否使用角点检测，默认为false
    double k = 0.04;//此值只有当use_harris = true时才会生效，其值的范围一般是0.04~0.06（经验值）
    QString filePath;

signals:

};

#endif // GOODFEATURESTOTRACK_H
