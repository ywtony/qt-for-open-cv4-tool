#include "goodfeaturestotrack.h"

GoodFeaturesToTrack::GoodFeaturesToTrack(QWidget *parent)
    : BaseSceneView(parent){
    this->setWindowTitle("更好的角点检测（goodFeaturesToTrack）");

    /*质量水平系数*/
    QLabel *label = new QLabel(this);
    label->setText("质量水平系数:");
    label->setFixedSize(QSize(100,30));
    btnLevelPlus = new QPushButton(this);
    btnLevelPlus->setFixedSize(QSize(50,30));
    btnLevelPlus->setText("+");
    btnLevelPlus->move(label->x()+label->width()+10,0);
    btnLevelMinus = new QPushButton(this);
    btnLevelMinus->setFixedSize(QSize(50,30));
    btnLevelMinus->setText("-");
    btnLevelMinus->move(btnLevelPlus->x()+btnLevelPlus->width()+10,0);
    /*角点之间的最小距离*/
    QLabel *min_distance_label = new QLabel(this);
    min_distance_label->setFixedSize(QSize(100,30));
    min_distance_label->setText("角点间最小距离：");
    min_distance_label->move(0,label->y()+label->height()+10);
    min_distanceEidt = new QLineEdit(this);
    min_distanceEidt->setText("2");
    min_distanceEidt->setFixedSize(QSize(50,30));
    min_distanceEidt->move(min_distance_label->x()+min_distance_label->width()+10,label->y()+label->height()+10);
    /*Sobel算子的大小*/
    QLabel *label_block_size = new QLabel(this);
    label_block_size->setText("Sobel算子大小：");
    label_block_size->setFixedSize(QSize(100,30));
    label_block_size->move(0,min_distanceEidt->y()+min_distanceEidt->y()+10);
    block_sizeEdit = new QLineEdit(this);
    block_sizeEdit->setFixedSize(QSize(50,30));
    block_sizeEdit->setText("3");
    block_sizeEdit->move(label_block_size->x()+label_block_size->width()+10,min_distanceEidt->y()+min_distanceEidt->y()+10);
    connect(btnLevelPlus,&QPushButton::clicked,[=](){//增大水平质量系数
        quality_level+=0.01;
        if(quality_level>=1){
            quality_level = 1;
        }
        qDebug()<<quality_level;
        //showGoodFeaturesToTrack();
        this->execute();
    });
    connect(btnLevelMinus,&QPushButton::clicked,[=](){//减小竖屏质量系数
        quality_level-=0.01;
        if(quality_level<=0.01){
            quality_level=0.01;
        }
        qDebug()<<quality_level;
        //showGoodFeaturesToTrack();
        this->execute();
    });
}

void GoodFeaturesToTrack::dropEvent(QDropEvent *event){
    QFileInfo file(event->mimeData()->urls().at(0).toLocalFile());
    filePath = file.absoluteFilePath();
    qDebug()<<"文件路径:"<<filePath;
    //showGoodFeaturesToTrack();
    this->execute();
}
/**
 * 执行更好的角点检测
 * @brief GoodFeaturesToTrack::showGoodFeaturesToTrack
 */
QPixmap GoodFeaturesToTrack::showGoodFeaturesToTrack(){
    updateParams(min_distanceEidt->text().toDouble(),block_sizeEdit->text().toInt());
    src = imread(filePath.toStdString().c_str());//加载原图
    if(src.empty()){
        qDebug()<<"Error file is empty";
        return NULL;
    }
//    imshow("src",src);
    cvtColor(src,gray,COLOR_BGR2GRAY);//转为灰度图
//    imshow("gray",gray);
    //执行角点检测
    vector<Point2f> corners;//检测出的角点
    goodFeaturesToTrack(gray,corners,maxCorners,quality_level,min_distance,Mat(),block_size,use_harris,k);
    //将角点绘制出来
    for(size_t t = 0;t<corners.size();t++){
        circle(src,corners[t],3,Scalar(0,0,255),1,LINE_8);
    }
    QImage image = ImageUtils::matToQImage(src);
    QPixmap pixmap = QPixmap::fromImage(image);
    /*QGraphicsPixmapItem *item = new QGraphicsPixmapItem(pixmap.scaled(this->size(),Qt::KeepAspectRatio,Qt::SmoothTransformation));
    scene.addItem(item);*/
//    imshow("result",src);
    return pixmap;
}

QPixmap GoodFeaturesToTrack::handle(){
    return showGoodFeaturesToTrack();
}

void GoodFeaturesToTrack::updateParams(double min_distance, int block_size){
    this->maxCorners = maxCorners;
    this->quality_level=quality_level;
    this->min_distance = min_distance;
    this->block_size = block_size;
}
