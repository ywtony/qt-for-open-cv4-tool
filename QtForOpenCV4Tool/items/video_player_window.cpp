 #include "video_player_window.h"

Video_Player_Window::Video_Player_Window(QWidget *parent)
    : QWidget(parent)
{
    this->setFixedSize(QSize(320,480));
    this->setWindowTitle("QT+OpenCV4.5.5+FFmpeg实现一个视频编辑器");
    createListView();
}

void Video_Player_Window::createListView(){
    listView = new CommonListView(this);
    connect(listView, &CommonListView::itemClicked, [=](QListWidgetItem* item) {
        CommonListViewItem* item2 = dynamic_cast<CommonListViewItem*>(item);
        qDebug() << "itemPos:" << item2->mPos;
        switch (item2->mPos) {
      /*  case 0:
            videoPlayerOpenGLShowImage.show();
            break;*/
        case 1:
            videoPlayerRoateFlip.show();
            break;
        case 2:
            videoPlayerSplicingImage.show();
            break;
        }
        });
    //new CommonListViewItem("使用OpenGL显示图片", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 1, listView);
    new CommonListViewItem("图像旋转与镜像", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 1, listView);
    new CommonListViewItem("合并两张图像为一张图像", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 2, listView);
}
