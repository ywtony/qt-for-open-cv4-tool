#pragma once

#include <QWidget>
#include <QIcon>
#include <QPushButton>
#include <QDebug>
#include <QLineEdit>
#include "../../../common/choice_image/ChoiceImageWidget.h"
#include "../../../common/edittext/EditText.h"
#include "../../../common/button/Button.h"
#include "../../../common/utils/DialogUtils.h"
#include <QMessageBox>
extern "C" {
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libavutil/avutil.h>
#include <libavutil/time.h>
}

class RtmpPusherWindow : public QWidget
{
	Q_OBJECT

public:
	RtmpPusherWindow(QWidget* parent = nullptr);
	~RtmpPusherWindow();
	void printFFError(int code);
	double r2d(AVRational r);

private:
	void initViews();
	void startPusher();

private:
	EditText* etDst = NULL;//推送目标地址
	EditText* et = NULL;//源地址

};
