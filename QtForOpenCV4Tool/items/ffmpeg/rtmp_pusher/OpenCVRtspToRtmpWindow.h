#pragma once

#include <QWidget>
#include <QIcon>
#include <QPushButton>
#include <QDebug>
#include <QLineEdit>
#include "../../../common/choice_image/ChoiceImageWidget.h"
#include "../../../common/edittext/EditText.h"
#include "../../../common/button/Button.h"
#include "../../../common/bean/PushStreamBean.h"
#include "../../../common/utils/DialogUtils.h"
#include "../../../common/thread/WorkerThread.h"
#include <QMessageBox>
#include <thread>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <QThread>

extern "C" {
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libavutil/avutil.h>
#include <libavutil/time.h>
#include <libswscale/swscale.h>
}
using namespace cv;

class OpenCVRtspToRtmpWindow : public QWidget
{
	Q_OBJECT

public:
	OpenCVRtspToRtmpWindow(QWidget *parent = nullptr);
	~OpenCVRtspToRtmpWindow();

	void printFFError(int code);
	double r2d(AVRational r);
private:
	void initViews();
	void startPusher();

private:
	EditText* etDst = NULL;//推送目标地址
	EditText* et = NULL;//源地址
	WorkerThread *workerThread = NULL;
};
