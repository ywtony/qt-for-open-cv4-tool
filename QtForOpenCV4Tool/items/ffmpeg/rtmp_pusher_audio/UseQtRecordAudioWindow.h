#pragma once

#include <QWidget>
#include "../../../common/button/Button.h"
#include <qicon.h>
#include <QDebug>
#include <QAudioInput>

/// <summary>
/// 利用qt录制音频的例子
/// </summary>
class UseQtRecordAudioWindow : public QWidget
{
	Q_OBJECT

public:
	UseQtRecordAudioWindow(QWidget *parent = nullptr);
	~UseQtRecordAudioWindow();

private:
	void startRecord();
};
