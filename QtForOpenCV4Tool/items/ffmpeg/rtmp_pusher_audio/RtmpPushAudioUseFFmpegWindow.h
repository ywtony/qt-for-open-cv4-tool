#pragma once

#include <QWidget>
#include <QIcon>
#include <QPushButton>
#include <QDebug>
#include <QLineEdit>
#include "../../../common/choice_image/ChoiceImageWidget.h"
#include "../../../common/edittext/EditText.h"
#include "../../../common/button/Button.h"
#include "../../../common/bean/PushStreamBean.h"
#include "../../../common/utils/DialogUtils.h"
#include "../../../common/thread/WorkerThread.h"
#include <QMessageBox>
#include <thread>
#include <QThread>

extern "C" {
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libavutil/avutil.h>
#include <libavutil/time.h>
#include <libswscale/swscale.h>
#include <libswresample/swresample.h>
}
#include <QDebug>
#include <QAudioInput>

class RtmpPushAudioUseFFmpegWindow : public QWidget
{
	Q_OBJECT

public:
	RtmpPushAudioUseFFmpegWindow(QWidget *parent = nullptr);
	~RtmpPushAudioUseFFmpegWindow();

private:
	void startRecord();
};
