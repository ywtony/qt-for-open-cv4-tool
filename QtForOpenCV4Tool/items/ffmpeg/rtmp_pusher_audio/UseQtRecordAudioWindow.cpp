#include "UseQtRecordAudioWindow.h"

UseQtRecordAudioWindow::UseQtRecordAudioWindow(QWidget* parent)
	: QWidget(parent)
{
	this->resize(QSize(480, 480));
	this->setWindowTitle("利用Qt录制音频");
	this->setWindowIcon(QIcon("images/opencv.png"));
	Button* btnStartRecord = new Button(this);
	btnStartRecord->setText("开始录制音频");
	connect(btnStartRecord, &Button::clicked, [=]() {
		startRecord();
		});
}

void UseQtRecordAudioWindow::startRecord() {
	qDebug() << "开始录制音频";

	//设置录制音频的格式
	QAudioFormat fmt;
	fmt.setSampleRate(44100);//设置采样率(每秒采样44100次)
	fmt.setChannelCount(2);//设置双声道
	fmt.setSampleSize(2 * 8);//设置采样格式，S16，即16位，8byte
	fmt.setCodec("audio/pcm");//设置编码格式
	fmt.setByteOrder(QAudioFormat::LittleEndian);//设置字节序，设置位小端模式，ffmpeg中用的是小端模式
	fmt.setSampleType(QAudioFormat::UnSignedInt);//设置采样数据类型
	QAudioDeviceInfo info = QAudioDeviceInfo::defaultInputDevice();//获取默认的录制设备
	if (!info.isFormatSupported(fmt)) {
		qDebug() << "Audio format not supported!";
		fmt = info.nearestFormat(fmt);//如果不支持手动设置的音频参数，则寻找最接近的
	}
	QAudioInput* input = new QAudioInput(fmt);
	//开始录制音频
	QIODevice* io = input->start();

	char buf[4096] = { 0 };
	for (;;) {
		//一次取一帧音频
		if (input->bytesReady() > 4096) {
			qDebug() << "size=" << io->read(buf, sizeof(buf)) << " " << flush;
			continue;
		}
	}


}

UseQtRecordAudioWindow::~UseQtRecordAudioWindow()
{
}
