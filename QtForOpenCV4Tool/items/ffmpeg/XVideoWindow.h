#pragma once

#include <QWidget>
#include <QIcon>
#include <QPushButton>
#include <QDebug>

#include <iostream>
#include <thread>
#include <chrono>

#include <QAudioFormat>
#include <QAudioOutput>
#include <QThread>
#include <QOpenGLFunctions_3_3_Core>
#include <QOpenGLWidget>
#include <QOpenGLShaderProgram>
#include <QFile>
#include <QTimer>

class XVideoWindow : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core
{
	Q_OBJECT

public:
	XVideoWindow(QWidget *parent = nullptr);
	~XVideoWindow();
	void setOrthoMProjection(float mWidth, float mHeight);
protected:
	virtual void initializeGL();//初始化OpenGL
	virtual void resizeGL(int w, int h);//ResizeWindow大小
	virtual void paintGL();//绘制OpenGL

private://opengl相关代码
	unsigned int VAO, VBO, EBO;
	unsigned int textures[3];
	GLuint programId;

	//以下三个默认是4x4单位矩阵
	QMatrix4x4 uModelMatrix;
	QMatrix4x4 uViewMatrix;
	QMatrix4x4 uProjectionMatrix;

	//材质内存空间
	unsigned char* datas[3] = { 0 };
	int mVideoWidth = 240;
	int mVideoHeight = 128;

	FILE* fp = NULL;
public:
	GLuint getShaderId(GLenum shaderType, QString resPath);
	GLuint buildAttachShaderAndReturnProgramId(QString vertexResPath, QString fragmentResPath);
	void getLinkProgramErrorInfo(GLuint programId);
	void getCompileShaderErrorInfo(GLuint shaderId);
	QString getTexturePath(QString textureImageName);
};
