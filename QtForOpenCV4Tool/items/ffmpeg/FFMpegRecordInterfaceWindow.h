#pragma once

#include <QWidget>
#include <QIcon>
#include <QPushButton>
#include <QDebug>
#include <QLineEdit>
#include "../../common/choice_image/ChoiceImageWidget.h"
#include "../../common/edittext/EditText.h"
#include "../../common/button/Button.h"
#include "../../common/utils/DialogUtils.h"
#include <QMessageBox>
extern "C" {
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libavutil/avutil.h>
#include <libavutil/time.h>
}

//ffmpeg rtsp转rtmp推流直播
class FFMpegRecordInterfaceWindow : public QWidget
{
	Q_OBJECT

public:
	FFMpegRecordInterfaceWindow(QWidget *parent = nullptr);
	~FFMpegRecordInterfaceWindow();
private:
	void initViews();
	void startPusher();

	void FError(int errNum);
	double r2d(AVRational r);

	void yuvTest();
private:
	EditText* etRtsp = NULL;//推送目标地址
	EditText* etRtmp = NULL;//源地址
private:
};
