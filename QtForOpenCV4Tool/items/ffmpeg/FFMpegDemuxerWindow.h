#pragma once

#include <QWidget>
#include <QIcon>
#include <QPushButton>
#include <QDebug>

extern "C" {
#include<libavcodec/avcodec.h>
#include<libavformat/avformat.h>
#include<libswscale/swscale.h>
#include <libswresample/swresample.h>
#include <libavutil/avutil.h>
#include <libavutil/imgutils.h>
#include <libavutil/opt.h>
}

#include <iostream>
#include <thread>
#include <chrono>

#include <QAudioFormat>
#include <QAudioOutput>
#include <QThread>


class FFMpegDemuxerWindow : public QWidget
{
	Q_OBJECT

public:
	FFMpegDemuxerWindow(QWidget* parent = nullptr);
	~FFMpegDemuxerWindow();
public:
	void demuxer();
	void release();
public:
	void playAudio();
private:
};
