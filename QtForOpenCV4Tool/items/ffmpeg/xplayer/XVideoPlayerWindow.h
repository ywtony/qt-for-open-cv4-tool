#pragma once

#include <QWidget>
#include <QIcon>
#include <QPushButton>
#include <QDebug>
#include <iostream>
#include <thread>
#include <chrono>
#include <QAudioFormat>
#include <QAudioOutput>
#include <QThread>
#include <QOpenGLFunctions_3_3_Core>
#include <QOpenGLWidget>
#include <QOpenGLShaderProgram>
#include <QFile>
#include <QTimer>
extern "C" {
#include<libavcodec/avcodec.h>
#include<libavformat/avformat.h>
#include<libswscale/swscale.h>
#include <libswresample/swresample.h>
#include <libavutil/avutil.h>
#include <libavutil/imgutils.h>
#include <libavutil/opt.h>
}
#include "XDecodeThread.h"

class XVideoPlayerWindow : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core
{
	Q_OBJECT

public:
	XVideoPlayerWindow(QWidget* parent = nullptr);
	~XVideoPlayerWindow();
	void setOrthoMProjection(float mWidth, float mHeight);
	//不管成功与否都释放frame空间
	virtual void Repaint(AVFrame* frame);
protected:
	virtual void initializeGL();//初始化OpenGL
	virtual void resizeGL(int w, int h);//ResizeWindow大小
	virtual void paintGL();//绘制OpenGL

private://opengl相关代码
	unsigned int VAO, VBO, EBO;
	unsigned int textures[3];
	GLuint programId;

	//以下三个默认是4x4单位矩阵
	QMatrix4x4 uModelMatrix;
	QMatrix4x4 uViewMatrix;
	QMatrix4x4 uProjectionMatrix;

	//材质内存空间
	unsigned char* datas[3] = { 0 };
	int mVideoWidth = 240;
	int mVideoHeight = 128;

	std::mutex mux;
	XDecodeThread mDecodeThread;
public:
	GLuint getShaderId(GLenum shaderType, QString resPath);
	GLuint buildAttachShaderAndReturnProgramId(QString vertexResPath, QString fragmentResPath);
	void getLinkProgramErrorInfo(GLuint programId);
	void getCompileShaderErrorInfo(GLuint shaderId);
	QString getTexturePath(QString textureImageName);
};