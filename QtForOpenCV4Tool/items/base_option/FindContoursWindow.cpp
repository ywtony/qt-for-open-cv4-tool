#include "FindContoursWindow.h"

FindContoursWindow::FindContoursWindow(QWidget *parent)
	: BaseSceneView(parent)
{
	this->setWindowTitle("轮廓发现");
	this->setFixedSize(1000, 480);
	this->setGraphicsViewSize(1000, 480);

	option = new BasePixelOption(this);

	//选择图片
	ChoiceImageWidget* choiceImageWidget = new ChoiceImageWidget(this);
	choiceImageWidget->setFixedWidth(200);
	choiceImageWidget->setCallback([=](QString filePath) {
		this->filePath = filePath;
		qDebug() << "选择图片完成开始打印路径：" << filePath;
		this->execute();
		});


	

	//添加显示图片
	imageTips[0] = new ImageTip(this);
	imageTips[0]->setItemsTitle("原图");
	imageTips[1] = new ImageTip(this);
	imageTips[1]->setItemsTitle("转灰度图像");
	imageTips[2] = new ImageTip(this);
	imageTips[2]->setItemsTitle("边缘检测结果");
	imageTips[3] = new ImageTip(this);
	imageTips[3]->setItemsTitle("最后绘制结果");
	//imageTips[4] = new ImageTip(this);
	//imageTips[4]->setItemsTitle("边缘检测结果");
	//imageTips[5] = new ImageTip(this);
	//imageTips[5]->setItemsTitle("最后的结果");

	QHBoxLayout* hLayout = new QHBoxLayout(this);
	QVBoxLayout* vLayout = new QVBoxLayout(this);


	vLayout->addWidget(choiceImageWidget);
	//vLayout->addWidget(cannySobelKSizeBtn);
	//vLayout->addWidget(sobelKSizeBtn);
	/*vLayout->addWidget(seekbar1);
	vLayout->addWidget(seekbar2);
	vLayout->addWidget(rhoBtn);
	vLayout->addWidget(threshold_pBtn);
	vLayout->addWidget(minLineLengthBtn);
	vLayout->addWidget(maxLineGapBtn);*/
	vLayout->setAlignment(Qt::AlignTop);

	QVBoxLayout* vLayout2 = new QVBoxLayout(this);
	QHBoxLayout* hTopLayout = new QHBoxLayout(this);
	QHBoxLayout* hTopLayout2 = new QHBoxLayout(this);
	hTopLayout->addWidget(imageTips[0]);
	hTopLayout->addWidget(imageTips[1]);
	hTopLayout->addWidget(imageTips[2]);
	hTopLayout2->addWidget(imageTips[3]);
	//hTopLayout2->addWidget(imageTips[4]);
	//hTopLayout2->addWidget(imageTips[5]);
	vLayout2->addLayout(hTopLayout);
	vLayout2->addLayout(hTopLayout2);


	hLayout->addLayout(vLayout, 1);
	hLayout->addLayout(vLayout2, 3);

}

//耗时方法(高斯不同演示)
QPixmap FindContoursWindow::handle() {
	Mat mats[4];
	option->showFindContours(filePath.toStdString().c_str(),mats);
	imageTips[0]->setItemsPixmap(ImageUtils::getPixmap(mats[0], 200, 200));
	imageTips[1]->setItemsPixmap(ImageUtils::getPixmap8(mats[1], 200, 200));
	imageTips[2]->setItemsPixmap(ImageUtils::getPixmap8(mats[2], 200, 200));
	imageTips[3]->setItemsPixmap(ImageUtils::getPixmap(mats[3], 200, 200));
	return NULL;
}

FindContoursWindow::~FindContoursWindow()
{
}
