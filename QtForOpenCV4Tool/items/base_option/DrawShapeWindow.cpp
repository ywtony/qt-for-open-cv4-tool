#include "DrawShapeWindow.h"

DrawShapeWindow::DrawShapeWindow(QWidget *parent)
	: BaseSceneView(parent)
{
	this->setWindowTitle("绘制");
	QVBoxLayout* vLayout = new QVBoxLayout(this);

	QHBoxLayout* hLayout = new QHBoxLayout(this);
	EditText* et = new EditText(this);
	et->setEnabled(false);
	et->setFixedHeight(30);
	Button* btnChoiceBtn = new Button(this);
	btnChoiceBtn->setText("请选择图片");


	hLayout->addWidget(et);
	hLayout->addWidget(btnChoiceBtn);
	hLayout->setAlignment(Qt::AlignTop);

	QLabel* labelTips1 = new QLabel(this);
	//labelTips1->setStyleSheet("QLabel{color:white}");
	labelTips1->setText("显示原图");
	labelTips1->setFixedHeight(15);
	imageViewSrc = new QLabel(this);

	QLabel* labelTips2 = new QLabel(this);
	//labelTips2->setStyleSheet("QLabel{color:white}");
	labelTips2->setText("绘制后的图像");
	labelTips2->setFixedHeight(15);
	imageViewResult = new QLabel(this);

	vLayout->addLayout(hLayout);
	vLayout->addWidget(labelTips1);
	vLayout->addWidget(imageViewSrc);
	vLayout->addWidget(labelTips2);
	vLayout->addWidget(imageViewResult);
	vLayout->setAlignment(Qt::AlignTop);
	this->setLayout(vLayout);

	option = new BasePixelOption(this);

	connect(btnChoiceBtn, &Button::clicked, this, [=]() {
		filePath = QFileDialog::getOpenFileName(this, tr("请选择图片"), "C:/Users/DBF-DEV-103/Downloads/", tr("Image Files(*.jpg *.png *.webp *.jpeg)"));
		et->setText(filePath);
		this->execute();//执行handle耗时方法

		});

}

QPixmap DrawShapeWindow::handle() {
	Mat mats[2];
	option->drawShape(mats);
	QPixmap src = QPixmap::fromImage(ImageUtils::matToQImage(mats[0]));
	imageViewSrc->setPixmap(src.scaled(QSize(this->width() - 20, this->height() - 20), Qt::KeepAspectRatio));
	qDebug() << "结果";
	QPixmap src2 = QPixmap::fromImage(ImageUtils::matToQImage(mats[1]));
	imageViewResult->setPixmap(src2.scaled(QSize(this->width() - 20, this->height() - 20), Qt::KeepAspectRatio));
	return NULL;
}

DrawShapeWindow::~DrawShapeWindow()
{
}
