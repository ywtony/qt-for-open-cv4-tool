#include "GaussianDiffWindow.h"

GaussianDiffWindow::GaussianDiffWindow(QWidget* parent)
	: BaseSceneView(parent)
{

	this->setWindowTitle("高斯不同");
	this->setFixedSize(1000, 480);
	this->setGraphicsViewSize(1000, 480);

	option = new BasePixelOption(this);

	//选择图片
	ChoiceImageWidget* choiceImageWidget = new ChoiceImageWidget(this);
	choiceImageWidget->setFixedWidth(200);
	choiceImageWidget->setCallback([=](QString filePath) {
		this->filePath = filePath;
		qDebug() << "选择图片完成开始打印路径：" << filePath;
		this->execute();
		});
	MinusPlusWidget* firstKSizeBtn = new MinusPlusWidget(this);
	firstKSizeBtn->setMinusAndPlus("第一个高斯模糊卷积核大小", [=](int minusValue) {
		qDebug() << "-value:" << minusValue;
		this->firstKSize -= 2;
		if (this->firstKSize <= 1) {
			this->firstKSize = 1;
		}
		this->execute();
		}, [=](int plusMinus) {
			qDebug() << "+value:" << plusMinus;
			this->firstKSize += 2;
			this->execute();
			});

	MinusPlusWidget* secondKSizeBtn = new MinusPlusWidget(this);
	secondKSizeBtn->setMinusAndPlus("第二个高斯模糊卷积核大小", [=](int minusValue) {
		qDebug() << "-value:" << minusValue;
		this->secondKSize -= 2;
		if (this->secondKSize <= 1) {
			this->secondKSize = 1;
		}
		this->execute();
		}, [=](int plusMinus) {
			qDebug() << "+value:" << plusMinus;
			this->secondKSize += 2;
			this->execute();
			});


	//添加显示图片
	imageTips[0] = new ImageTip(this);
	imageTips[0]->setItemsTitle("原图");
	imageTips[1] = new ImageTip(this);
	imageTips[1]->setItemsTitle("第一个高斯模糊");
	imageTips[2] = new ImageTip(this);
	imageTips[2]->setItemsTitle("第二个高斯模糊");
	imageTips[3] = new ImageTip(this);
	imageTips[3]->setItemsTitle("第一减第二结果");
	imageTips[4] = new ImageTip(this);
	imageTips[4]->setItemsTitle("结果灰度图");
	imageTips[5] = new ImageTip(this);
	imageTips[5]->setItemsTitle("结果二值图像");

	QHBoxLayout* hLayout = new QHBoxLayout(this);
	QVBoxLayout* vLayout = new QVBoxLayout(this);


	vLayout->addWidget(choiceImageWidget);
	vLayout->addWidget(firstKSizeBtn);
	vLayout->addWidget(secondKSizeBtn);
	vLayout->setAlignment(Qt::AlignTop);

	QVBoxLayout* vLayout2 = new QVBoxLayout(this);
	QHBoxLayout* hTopLayout = new QHBoxLayout(this);
	QHBoxLayout* hTopLayout2 = new QHBoxLayout(this);
	hTopLayout->addWidget(imageTips[0]);
	hTopLayout->addWidget(imageTips[1]);
	hTopLayout->addWidget(imageTips[2]);

	hTopLayout2->addWidget(imageTips[3]);
	hTopLayout2->addWidget(imageTips[4]);
	hTopLayout2->addWidget(imageTips[5]);
	vLayout2->addLayout(hTopLayout);
	vLayout2->addLayout(hTopLayout2);


	hLayout->addLayout(vLayout, 1);
	hLayout->addLayout(vLayout2, 3);

}

//耗时方法(高斯不同演示)
QPixmap GaussianDiffWindow::handle() {
	Mat mats[6];
	option->showGaussianDiff(filePath.toStdString().c_str(), firstKSize, secondKSize, mats);
	for (int i = 0;i <= 5;i++) {
		if (i <= 3) {
			QPixmap pixmap = ImageUtils::getPixmap(mats[i], 200, 200);
			imageTips[i]->setItemsPixmap(pixmap);
		}
		else {
			QPixmap pixmap = ImageUtils::getPixmap8(mats[i], 200, 200);
			imageTips[i]->setItemsPixmap(pixmap);
		}

	}

	return NULL;
}

GaussianDiffWindow::~GaussianDiffWindow()
{

}
