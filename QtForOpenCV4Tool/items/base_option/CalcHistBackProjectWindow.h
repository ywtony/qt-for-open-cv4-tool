#pragma once

#include <QWidget>
#include "BasePixelOption.h"
#include "../../common/base/BaseSceneView.h"
#include "../../common/button/Button.h"
#include "../../common/edittext/EditText.h"
#include "../../common/seekbar/Seekbar.h"
#include "../../common/minus_plus/MinusPlusWidget.h"
#include "../../common/seekbar/SeekBarTipsWidget.h"
#include "../../common/choice_image/ChoiceImageWidget.h"
#include "../../common/image_tip/ImageTip.h"
#include "../../common/layout/VariableGridView.h"

#include <QFileDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QVBoxLayout>
#include <iostream>
#include <QGridLayout>
#include <QButtonGroup>
#include <QRadioButton>
#include <QMetaType> 
#include <QMessageBox>

//直方图反射投影
class CalcHistBackProjectWindow : public BaseSceneView
{
	Q_OBJECT

public:
	CalcHistBackProjectWindow(QWidget *parent = nullptr);
	~CalcHistBackProjectWindow();

protected:
	QPixmap handle();

private:
	QString filePath;
	QString roi_filePath;
	BasePixelOption* option;
	ImageTip* imageTips[7];
};
