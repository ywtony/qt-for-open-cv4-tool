#include "EqualizeHistWindow.h"

EqualizeHistWindow::EqualizeHistWindow(QWidget *parent)
	: BaseSceneView(parent)
{
	this->setWindowTitle("直方图均衡化");
	this->setFixedSize(1000, 480);
	this->setGraphicsViewSize(1000, 480);

	option = new BasePixelOption(this);

	//选择图片
	ChoiceImageWidget* choiceImageWidget = new ChoiceImageWidget(this);
	choiceImageWidget->setFixedWidth(200);
	choiceImageWidget->setCallback([=](QString filePath) {
		this->filePath = filePath;
		qDebug() << "选择图片完成开始打印路径：" << filePath;
		this->execute();
		});


	

	//添加显示图片
	imageTips[0] = new ImageTip(this);
	imageTips[0]->setItemsTitle("原图");
	imageTips[1] = new ImageTip(this);
	imageTips[1]->setItemsTitle("灰度图");
	imageTips[2] = new ImageTip(this);
	imageTips[2]->setItemsTitle("直方图均衡化");

	QHBoxLayout* hLayout = new QHBoxLayout(this);
	QVBoxLayout* vLayout = new QVBoxLayout(this);


	vLayout->addWidget(choiceImageWidget);
	vLayout->setAlignment(Qt::AlignTop);

	QVBoxLayout* vLayout2 = new QVBoxLayout(this);
	QHBoxLayout* hTopLayout = new QHBoxLayout(this);
	QHBoxLayout* hTopLayout2 = new QHBoxLayout(this);
	hTopLayout->addStretch();
	hTopLayout->addWidget(imageTips[0]);
	hTopLayout->addStretch();
	hTopLayout->addWidget(imageTips[1]);
	hTopLayout->addStretch();
	hTopLayout->addWidget(imageTips[2]);
	hTopLayout->addStretch();
	vLayout2->addStretch();
	vLayout2->addLayout(hTopLayout);
	vLayout2->addStretch();
	vLayout2->addLayout(hTopLayout2);
	vLayout2->addStretch();

	hLayout->addLayout(vLayout, 1);
	hLayout->addLayout(vLayout2, 3);

}

QPixmap EqualizeHistWindow::handle() {
	Mat mats[3];
	option->showEqualizeHist(filePath.toStdString().c_str(),mats);
	imageTips[0]->setItemsPixmap(ImageUtils::getPixmap(mats[0], 400, 400));
	imageTips[1]->setItemsPixmap(ImageUtils::getPixmap8(mats[1], 400, 400));
	imageTips[2]->setItemsPixmap(ImageUtils::getPixmap8(mats[2], 400, 400));
	return NULL;
}

EqualizeHistWindow::~EqualizeHistWindow()
{
}
