#pragma once

#include <QWidget>
#include "BasePixelOption.h"
#include "../../common/base/BaseSceneView.h"
#include "../../common/button/Button.h"
#include "../../common/edittext/EditText.h"
#include "../../common/seekbar/Seekbar.h"
#include "../../common/minus_plus/MinusPlusWidget.h"
#include "../../common/seekbar/SeekBarTipsWidget.h"
#include "../../common/choice_image/ChoiceImageWidget.h"
#include "../../common/image_tip/ImageTip.h"
#include "../../common/layout/VariableGridView.h"

#include <QFileDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QVBoxLayout>
#include <iostream>
#include <QGridLayout>
#include <QButtonGroup>
#include <QRadioButton>
#include <QMetaType> 
#include <QMessageBox>

//霍夫直线检测
class HoughLineWindow : public BaseSceneView
{
	Q_OBJECT

public:
	HoughLineWindow(QWidget *parent = nullptr);
	~HoughLineWindow();

protected:
	QPixmap handle();

private:
	QString filePath;
	BasePixelOption* option;
	ImageTip* imageTips[4];
	int threshold_min = 50;//下阈值
	int threshold_max = 100;//上阈值
	int cannySobelKSize = 3;//边缘检测的卷积核
	int rho = 1;//以像素为单位的距离精度。 另一种形容方式是直线搜索时的进步尺寸的单位半径
	int threshold_p = 10;//累加平面的阈值参数，即识别某部分为图中的一条直线时它在累加平面中必须达到的值。 大于阈值 threshold 的线段才可以被检测通过并返回到结果中
	double minLineLength = 100;//最低线段的长度，比这个设定参数短的线段就不能被显现出来
	double maxLineGap = 10;//允许将同一行点与点之间连接起来的最大的距离
};
