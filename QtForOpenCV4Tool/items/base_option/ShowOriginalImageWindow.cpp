#include "ShowOriginalImageWindow.h"

ShowOriginalImageWindow::ShowOriginalImageWindow(QWidget* parent)
	: BaseSceneView(parent)
{
	this->setWindowTitle("显示原图");

	QVBoxLayout* vLayout = new QVBoxLayout(this);

	QHBoxLayout* hLayout = new QHBoxLayout(this);
	EditText* et = new EditText(this);
	et->setEnabled(false);
	et->setFixedHeight(30);
	Button* btnChoiceBtn = new Button(this);
	btnChoiceBtn->setText("请选择图片");


	hLayout->addWidget(et);
	hLayout->addWidget(btnChoiceBtn);
	hLayout->setAlignment(Qt::AlignTop);

	imageView = new QLabel(this);
	imageView->setText("哈哈哈");
	imageView->setFixedWidth(this->width()-20);

	vLayout->addLayout(hLayout);
	vLayout->addWidget(imageView);
	this->setLayout(vLayout);

	option = new BasePixelOption(this);

	connect(btnChoiceBtn, &Button::clicked, this, [=]() {
		filePath = QFileDialog::getOpenFileName(this, tr("请选择图片"), "C:/Users/DBF-DEV-103/Downloads/", tr("Image Files(*.jpg *.png *.webp *.jpeg)"));
		et->setText(filePath);
		this->execute();//执行handle耗时方法

		});

}
QPixmap ShowOriginalImageWindow::handle() {
	//option->showSrcImage(filePath.toStdString().c_str());
	QPixmap pixmap = option->showSrcImage(filePath.toStdString().c_str());
	imageView->setPixmap(pixmap.scaled(imageView->size(), Qt::KeepAspectRatio));
	return NULL;
}

ShowOriginalImageWindow::~ShowOriginalImageWindow()
{

}
