#include "GaussianBlurWindow.h"

GaussianBlurWindow::GaussianBlurWindow(QWidget* parent)
	: BaseSceneView(parent)
{
	this->setWindowTitle("高斯滤波");
	this->setFixedSize(QSize(320, 800));
	QVBoxLayout* vLayout = new QVBoxLayout(this);

	QHBoxLayout* hLayout = new QHBoxLayout(this);
	EditText* et = new EditText(this);
	et->setEnabled(false);
	et->setFixedHeight(30);
	Button* btnChoiceBtn = new Button(this);
	btnChoiceBtn->setText("请选择图片");


	hLayout->addWidget(et);
	hLayout->addWidget(btnChoiceBtn);
	hLayout->setAlignment(Qt::AlignTop);

	QLabel* labelTips1 = new QLabel(this);
	//labelTips1->setStyleSheet("QLabel{color:white}");
	labelTips1->setText("显示原图");
	labelTips1->setFixedHeight(15);
	imageViewSrc = new QLabel(this);

	QLabel* labelTips2 = new QLabel(this);
	//labelTips2->setStyleSheet("QLabel{color:white}");
	labelTips2->setText("显示高斯滤波");
	labelTips2->setFixedHeight(15);

	//改变卷积核大小
	QHBoxLayout* hLayout2 = new QHBoxLayout(this);
	Button* btnMinus = new Button(this);
	btnMinus->resize(50, 30);
	btnMinus->setText("-");
	EditText* etKsize = new EditText(this);
	etKsize->resize(50, 30);
	etKsize->setText(QString::number(mKsize));
	Button* btnPlus = new Button(this);
	btnPlus->resize(50, 30);
	btnPlus->setText("+");
	hLayout2->addWidget(btnMinus, 1);
	hLayout2->addWidget(etKsize, 1);
	hLayout2->addWidget(btnPlus, 1);

	//自动改变卷积核大小
	QHBoxLayout* hLayout3 = new QHBoxLayout(this);
	Button* btnStartTimer = new Button(this);
	btnStartTimer->resize(50, 30);
	btnStartTimer->setText("逐渐模糊");
	Button* btnStopTimer = new Button(this);
	btnStopTimer->resize(50, 30);
	btnStopTimer->setText("逐渐清晰");
	hLayout3->addWidget(btnStartTimer, 1);
	hLayout3->addWidget(btnStopTimer, 1);

	imageViewResult = new QLabel(this);

	vLayout->addLayout(hLayout);
	vLayout->addWidget(labelTips1);
	vLayout->addWidget(imageViewSrc);
	vLayout->addWidget(labelTips2);
	vLayout->addLayout(hLayout2);
	vLayout->addLayout(hLayout3);
	vLayout->addWidget(imageViewResult);
	vLayout->setAlignment(Qt::AlignTop);
	this->setLayout(vLayout);

	option = new BasePixelOption(this);

	connect(btnChoiceBtn, &Button::clicked, this, [=]() {
		filePath = QFileDialog::getOpenFileName(this, tr("请选择图片"), "C:/Users/DBF-DEV-103/Downloads/", tr("Image Files(*.jpg *.png *.webp *.jpeg)"));
		et->setText(filePath);
		this->execute();//执行handle耗时方法

		});

	connect(btnMinus, &Button::clicked, this, [=]() {
		mKsize -= 2;
		if (mKsize <= 1) {
			mKsize = 1;
		}
		etKsize->setText(QString::number(mKsize));
		this->execute();//执行handle耗时方法

		});

	connect(btnPlus, &Button::clicked, this, [=]() {
		mKsize += 2;
		etKsize->setText(QString::number(mKsize));
		this->execute();//执行handle耗时方法

		});

	//写一个定时器增大/缩小卷积核，实现画面从模糊到清晰或从清晰到模糊的一个过程
	QTimer* myTimer = new QTimer(this);
	connect(myTimer, &QTimer::timeout, this, [=] {
		qDebug() << "1000毫秒后超时";
		if (isPlus) {
			mKsize += 2;
			if (mKsize >= 51) {
				mKsize = 51;
				myTimer->stop();
			}
			etKsize->setText(QString::number(mKsize));
			this->execute();//执行handle耗时方法
		}
		else {
			mKsize -= 2;
			if (mKsize <= 1) {
				mKsize = 1;
				myTimer->stop();
			}
			etKsize->setText(QString::number(mKsize));
			this->execute();//执行handle耗时方法
		}
		});

	connect(btnStartTimer, &Button::clicked, this, [=]() {
		myTimer->start(100);
		isPlus = true;

		});
	connect(btnStopTimer, &Button::clicked, this, [=]() {
		isPlus = false;
		myTimer->start(100);
		});


}
//耗时方法
QPixmap GaussianBlurWindow::handle() {
	Mat mats[2];
	option->showGaussianBlue(filePath.toStdString().c_str(), mKsize, mats);
	QPixmap src = QPixmap::fromImage(ImageUtils::matToQImage(mats[0]));
	imageViewSrc->setPixmap(src.scaled(QSize(this->width() - 20, this->height() - 20), Qt::KeepAspectRatio));
	qDebug() << "结果";
	QPixmap src2 = QPixmap::fromImage(ImageUtils::matToQImage(mats[1]));
	imageViewResult->setPixmap(src2.scaled(QSize(this->width() - 20, this->height() - 20), Qt::KeepAspectRatio));
	return NULL;
}
GaussianBlurWindow::~GaussianBlurWindow()
{
	/*map<string, int> mMap;
	mMap.insert(pair<string, int>("牛逼普拉斯", 20));
	map<string, int>::iterator pos = mMap.find("牛逼普莱斯");*/
}
