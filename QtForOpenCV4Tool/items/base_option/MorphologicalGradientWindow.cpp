#include "MorphologicalGradientWindow.h"

MorphologicalGradientWindow::MorphologicalGradientWindow(QWidget* parent)
	: BaseSceneView(parent)
{
	this->setWindowTitle("形态学梯度");
	this->setFixedSize(800, 480);
	this->setGraphicsViewSize(800, 480);

	//选择图片
	ChoiceImageWidget* choiceImageWidget = new ChoiceImageWidget(this);
	choiceImageWidget->setFixedWidth(200);
	choiceImageWidget->setCallback([=](QString filePath) {
		this->filePath = filePath;
		qDebug() << "选择图片完成开始打印路径：" << filePath;
		this->execute();
		});
	//形态学梯度卷积核大小调整
	MinusPlusWidget* minusPlusWidgetDilate = new MinusPlusWidget(this);
	minusPlusWidgetDilate->setMinusClick([=](int value) {
		qDebug() << "-value:" << value;
		this->kSize -= 2;
		if (this->kSize <= 1) {
			this->kSize = 1;
		}
		this->execute();
		});

	minusPlusWidgetDilate->setPlusClick([=](int value) {
		qDebug() << "+value:" << value;
		this->kSize += 2;
		this->execute();
		});
	minusPlusWidgetDilate->setTipsContent("形态学梯度卷积核大小调整");


	Button* btn = new Button(this);
	btn->setText("形态学梯度");
	Button* btn2 = new Button(this);
	btn2->setText("顶帽操作");
	Button* btn3 = new Button(this);
	btn3->setText("黑帽操作");

	//添加显示图片
	imageTips[0] = new ImageTip(this);
	imageTips[1] = new ImageTip(this);


	QGridLayout* gridLayout = new QGridLayout(this);//网格布局
	gridLayout->addWidget(choiceImageWidget, 0, 0, 1, 1);
	gridLayout->addWidget(minusPlusWidgetDilate, 1, 0, 1, 1);
	gridLayout->addWidget(btn, 2, 0, 1, 1);
	gridLayout->addWidget(btn2, 3, 0, 1, 1);
	gridLayout->addWidget(btn3, 4, 0, 1, 1);
	gridLayout->addWidget(imageTips[0], 0, 2, 1, 1);
	gridLayout->addWidget(imageTips[1], 0, 3, 1, 1);
	this->setLayout(gridLayout);

	option = new BasePixelOption(this);

	connect(btn, &Button::clicked, this, [=]() {
		mType = 1;
		this->execute();
		});
	connect(btn2, &Button::clicked, this, [=]() {
		mType = 2;
		this->execute();
		});
	connect(btn3, &Button::clicked, this, [=]() {
		mType = 3;
		this->execute();
		});
}

//耗时方法
QPixmap MorphologicalGradientWindow::handle() {
	qDebug() << "形态学开操作";
	Mat mats[2];
	option->showMorphologicalGradient(filePath.toStdString().c_str(), kSize,mType,mats);
	qDebug() << "开操作";
	QPixmap src = QPixmap::fromImage(ImageUtils::matToQImage(mats[0]));
	imageTips[0]->setItemsContent("原图", src.scaled(QSize(400, 300), Qt::KeepAspectRatio));
	qDebug() << "结果";
	QPixmap src1 = QPixmap::fromImage(ImageUtils::matToQImage8(mats[1]));
	imageTips[1]->setItemsContent("梯度图像", src1.scaled(QSize(400, 300), Qt::KeepAspectRatio));
	return NULL;
}

MorphologicalGradientWindow::~MorphologicalGradientWindow()
{

}
