#pragma once

#include <QWidget>
#include "BasePixelOption.h"
#include "../../common/base/BaseSceneView.h"
#include "../../common/button/Button.h"
#include "../../common/edittext/EditText.h"
#include "../../common/seekbar/Seekbar.h"
#include "../../common/minus_plus/MinusPlusWidget.h"
#include "../../common/seekbar/SeekBarTipsWidget.h"
#include "../../common/choice_image/ChoiceImageWidget.h"
#include <QFileDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QVBoxLayout>
#include <iostream>

//开操作
class OpenImageOptionWindow : public BaseSceneView
{
	Q_OBJECT

public:
	OpenImageOptionWindow(QWidget *parent = nullptr);
	~OpenImageOptionWindow();


protected:
	QPixmap handle();

private:
	QString filePath;
	BasePixelOption* option;
	QLabel* imageViews[3];
	int dilateKSize = 3;
	int erodeKsize = 5;
	int imgWidth = 600;
	int imgHeight = 200;
};
