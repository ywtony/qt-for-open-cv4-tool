#include "IncreaseBrightnessContrastRatioWindow.h"

IncreaseBrightnessContrastRatioWindow::IncreaseBrightnessContrastRatioWindow(QWidget *parent)
	: BaseSceneView(parent)
{
	this->setWindowTitle("调整图像亮度");

	this->setFixedSize(QSize(320, 800));

	QVBoxLayout* vLayout = new QVBoxLayout(this);
	//选择第一张图片
	QHBoxLayout* hLayout = new QHBoxLayout(this);
	EditText* et = new EditText(this);
	et->setEnabled(false);
	et->setFixedHeight(30);
	Button* btnChoiceBtn = new Button(this);
	btnChoiceBtn->setText("选择图片");


	hLayout->addWidget(et);
	hLayout->addWidget(btnChoiceBtn);
	hLayout->setAlignment(Qt::AlignTop);


	QLabel* labelTips1 = new QLabel(this);
	//labelTips1->setStyleSheet("QLabel{color:white}");
	labelTips1->setText("原图");
	labelTips1->setFixedHeight(15);
	imageViewStep1 = new QLabel(this);

	QLabel* labelTips2 = new QLabel(this);
	//labelTips2->setStyleSheet("QLabel{color:white}");
	labelTips2->setText("调节后的图片");
	labelTips2->setFixedHeight(15);
	imageViewStep2 = new QLabel(this);

	QLabel* labelTips3 = new QLabel(this);
	labelTips3->setText("调节对比度");
	labelTips3->setFixedHeight(15);
	Seekbar* seekbar = new Seekbar(this);
	seekbar->setValue(mAlpha);

	QLabel* labelTips4 = new QLabel(this);
	labelTips4->setText("调节亮度");
	labelTips4->setFixedHeight(15);
	Seekbar* seekbar2 = new Seekbar(this);
	seekbar2->setValueRange(0, 255, 15);
	seekbar2->setValue(mBeta);

	vLayout->addLayout(hLayout);

	vLayout->addWidget(labelTips1);
	vLayout->addWidget(imageViewStep1);
	vLayout->addWidget(labelTips2);
	vLayout->addWidget(labelTips3);
	vLayout->addWidget(seekbar);
	vLayout->addWidget(labelTips4);
	vLayout->addWidget(seekbar2);
	vLayout->addWidget(imageViewStep2);

	vLayout->setAlignment(Qt::AlignTop);
	this->setLayout(vLayout);

	option = new BasePixelOption(this);
	//选择第一张图片
	connect(btnChoiceBtn, &Button::clicked, this, [=]() {
		filePath = QFileDialog::getOpenFileName(this, tr("请选择图片"), "C:/Users/DBF-DEV-103/Downloads/", tr("Image Files(*.jpg *.png *.webp *.jpeg)"));
		et->setText(filePath);
		this->execute();
		});

	connect(seekbar, &Seekbar::valueChanged, this, [=](int value) {
		mAlpha = value / 100.0+1;
		qDebug() << "mAlpha:" << mAlpha;
		this->execute();
		});

	connect(seekbar2, &Seekbar::valueChanged, this, [=](int value) {
		mBeta = value;
		qDebug() << "mBeta:" << value;
		this->execute();
		});
}

//执行耗时任务
QPixmap IncreaseBrightnessContrastRatioWindow::handle() {
	Mat mats[2];
	option->increaseBrightnessContrastRatio(filePath.toStdString().c_str(),mBeta,mAlpha, mats);
	QPixmap src = QPixmap::fromImage(ImageUtils::matToQImage(mats[0]));
	imageViewStep1->setPixmap(src.scaled(QSize(this->width() - 20, this->height() - 20), Qt::KeepAspectRatio));
	qDebug() << "显示调节后的图片";
	QPixmap src2 = QPixmap::fromImage(ImageUtils::matToQImage(mats[1]));
	imageViewStep2->setPixmap(src2.scaled(QSize(this->width() - 20, this->height() - 20), Qt::KeepAspectRatio));
	return NULL;
}

IncreaseBrightnessContrastRatioWindow::~IncreaseBrightnessContrastRatioWindow()
{
}
