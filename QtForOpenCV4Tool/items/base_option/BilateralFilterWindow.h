#pragma once

#include <QWidget>
#include "BasePixelOption.h"
#include "../../common/base/BaseSceneView.h"
#include "../../common/button/Button.h"
#include "../../common/edittext/EditText.h"
#include "../../common/seekbar/Seekbar.h"
#include <QFileDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QVBoxLayout>
#include <iostream>

class BilateralFilterWindow : public BaseSceneView
{
	Q_OBJECT

public:
	BilateralFilterWindow(QWidget* parent = nullptr);
	~BilateralFilterWindow();

protected:
	QPixmap handle();

private:
	QString filePath;
	BasePixelOption* option;
	QLabel* imageViewSrc;
	QLabel* imageViewResult;
	int g_d = 15;//卷积核直径
	int g_sigmaColor = 20;//颜色空间方差，值越大混合的颜色越广
	int g_sigmaSpace = 50;//值越大影响的区域越大
};
