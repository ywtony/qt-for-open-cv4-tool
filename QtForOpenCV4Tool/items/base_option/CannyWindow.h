#pragma once

#include <QWidget>
#include "BasePixelOption.h"
#include "../../common/base/BaseSceneView.h"
#include "../../common/button/Button.h"
#include "../../common/edittext/EditText.h"
#include "../../common/seekbar/Seekbar.h"
#include "../../common/minus_plus/MinusPlusWidget.h"
#include "../../common/seekbar/SeekBarTipsWidget.h"
#include "../../common/choice_image/ChoiceImageWidget.h"
#include "../../common/image_tip/ImageTip.h"
#include "../../common/layout/VariableGridView.h"

#include <QFileDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QVBoxLayout>
#include <iostream>
#include <QGridLayout>
#include <QButtonGroup>
#include <QRadioButton>
#include <QMetaType> 
#include <QMessageBox>

//边缘检测
class CannyWindow : public BaseSceneView
{
	Q_OBJECT

public:
	CannyWindow(QWidget* parent = nullptr);
	~CannyWindow();

protected:
	QPixmap handle();

private:
	QString filePath;
	BasePixelOption* option;
	ImageTip* imageTips[5];
	int threshold_min = 50;
	int threshold_max = 100;
	int blurKSize = 3;
	int sobelKSize = 3;

};
