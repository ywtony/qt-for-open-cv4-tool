#include "PryUpAndDownWindow.h"

PryUpAndDownWindow::PryUpAndDownWindow(QWidget* parent)
	: BaseSceneView(parent)
{

	this->setWindowTitle("图像采样");
	this->setFixedSize(800, 480);
	this->setGraphicsViewSize(800, 480);

	option = new BasePixelOption(this);

	//选择图片
	ChoiceImageWidget* choiceImageWidget = new ChoiceImageWidget(this);
	choiceImageWidget->setFixedWidth(200);
	choiceImageWidget->setCallback([=](QString filePath) {
		this->filePath = filePath;
		qDebug() << "选择图片完成开始打印路径：" << filePath;
		this->execute();
		});
	//形态学梯度卷积核大小调整
	MinusPlusWidget* minusPlusWidgetDilate = new MinusPlusWidget(this);
	minusPlusWidgetDilate->setMinusClick([=](int value) {
		qDebug() << "-value:" << value;
		this->kSize -= 2;
		if (this->kSize <= 1) {
			this->kSize = 1;
		}
		this->execute();
		});

	minusPlusWidgetDilate->setPlusClick([=](int value) {
		qDebug() << "+value:" << value;
		this->kSize += 2;
		this->execute();
		});
	minusPlusWidgetDilate->setTipsContent("图像采样");

	//添加显示图片
	imageTips[0] = new ImageTip(this);
	imageTips[1] = new ImageTip(this);

	QHBoxLayout* hLayout = new QHBoxLayout(this);
	QVBoxLayout* vLayout = new QVBoxLayout(this);

	//
	QButtonGroup* group = new QButtonGroup(this);
	QRadioButton* btn1 = new QRadioButton(this);
	btn1->setText("上采样");
	QRadioButton* btn2 = new QRadioButton(this);
	btn2->setText("降采样");
	group->addButton(btn1, 0);
	group->addButton(btn2, 1);
	QHBoxLayout* radioLayout = new QHBoxLayout(this);
	radioLayout->addWidget(btn1);
	radioLayout->addWidget(btn2);

	vLayout->addWidget(choiceImageWidget);
	vLayout->addWidget(minusPlusWidgetDilate);
	vLayout->addLayout(radioLayout);
	vLayout->addWidget(btn2);
	vLayout->setAlignment(Qt::AlignTop);

	QVBoxLayout* vLayout2 = new QVBoxLayout(this);
	vLayout2->addWidget(imageTips[0]);
	vLayout2->addWidget(imageTips[1]);

	hLayout->addLayout(vLayout, 1);
	hLayout->addLayout(vLayout2, 3);

	connect(btn1, &QRadioButton::clicked, this, [=]() {
		//QMessageBox::information(this, "上采样", "这是一个上采样弹框");
		this->mType = 1;
		this->execute();
		});
	connect(btn2, &QRadioButton::clicked, this, [=]() {
		this->mType = 0;
		this->execute();
		//QMessageBox::information(this, "降采样", "这是一个降采样弹框");
		});


}

//耗时方法
QPixmap PryUpAndDownWindow::handle() {
	qDebug() << "形态学开操作";
	Mat mats[2];
	option->showPyrUp(filePath.toStdString().c_str(), type, kSize, mats);
	qDebug() << "开操作";
	QPixmap src = QPixmap::fromImage(ImageUtils::matToQImage(mats[0]));
	imageTips[0]->setItemsContent("原图", src.scaled(QSize(400, 300), Qt::KeepAspectRatio));
	qDebug() << "结果";
	QPixmap src1 = QPixmap::fromImage(ImageUtils::matToQImage(mats[1]));
	imageTips[1]->setItemsContent("上采样/降采样", src1.scaled(QSize(400, 300), Qt::KeepAspectRatio));
	return NULL;
}

PryUpAndDownWindow::~PryUpAndDownWindow()
{

}
