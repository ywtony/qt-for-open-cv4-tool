#include "SobelAndScharrWindow.h"

SobelAndScharrWindow::SobelAndScharrWindow(QWidget* parent)
	: BaseSceneView(parent)
{
	this->setWindowTitle("Sobel/Scharr梯度图像");
	this->setFixedSize(1000, 480);
	this->setGraphicsViewSize(1000, 480);

	option = new BasePixelOption(this);

	//选择图片
	ChoiceImageWidget* choiceImageWidget = new ChoiceImageWidget(this);
	choiceImageWidget->setFixedWidth(200);
	choiceImageWidget->setCallback([=](QString filePath) {
		this->filePath = filePath;
		qDebug() << "选择图片完成开始打印路径：" << filePath;
		this->execute();
		});

	MinusPlusWidget* gaussKSizeBtn = new MinusPlusWidget(this);
	gaussKSizeBtn->setMinusAndPlus("高斯模糊卷积核", [=](int minusValue) {
		qDebug() << "-value:" << minusValue;
		this->gaussKSize -= 2;
		if (this->gaussKSize <= 1) {
			this->gaussKSize = 1;
		}
		this->execute();
		}, [=](int plusMinus) {
			qDebug() << "+value:" << plusMinus;
			this->gaussKSize += 2;
			this->execute();
			});

	MinusPlusWidget* sobelKSizeBtn = new MinusPlusWidget(this);
	sobelKSizeBtn->setMinusAndPlus("Sobel卷积核", [=](int minusValue) {
		qDebug() << "-value:" << minusValue;
		this->sobelKSize -= 2;
		if (this->sobelKSize <= 1) {
			this->sobelKSize = 1;
		}
		this->execute();
		}, [=](int plusMinus) {
			qDebug() << "+value:" << plusMinus;
			this->sobelKSize += 2;
			this->execute();
			});

	//添加显示图片
	imageTips[0] = new ImageTip(this);
	imageTips[0]->setItemsTitle("原图");
	imageTips[1] = new ImageTip(this);
	imageTips[1]->setItemsTitle("高斯模糊去噪声");
	imageTips[2] = new ImageTip(this);
	imageTips[2]->setItemsTitle("转灰度");
	imageTips[3] = new ImageTip(this);
	imageTips[3]->setItemsTitle("Sobel对x及y方向上的梯度图像做权重相加");
	imageTips[4] = new ImageTip(this);
	imageTips[4]->setItemsTitle("Scharr对x及y方向上的梯度图像做权重相加");

	QHBoxLayout* hLayout = new QHBoxLayout(this);
	QVBoxLayout* vLayout = new QVBoxLayout(this);


	vLayout->addWidget(choiceImageWidget);
	vLayout->addWidget(gaussKSizeBtn);
	vLayout->addWidget(sobelKSizeBtn);
	vLayout->addWidget(imageTips[0]);
	vLayout->setAlignment(Qt::AlignTop);

	QVBoxLayout* vLayout2 = new QVBoxLayout(this);
	QHBoxLayout* hTopLayout = new QHBoxLayout(this);
	QHBoxLayout* hTopLayout2 = new QHBoxLayout(this);
	hTopLayout->addWidget(imageTips[1]);
	hTopLayout->addWidget(imageTips[2]);

	hTopLayout2->addWidget(imageTips[3]);
	hTopLayout2->addWidget(imageTips[4]);
	vLayout2->addLayout(hTopLayout);
	vLayout2->addLayout(hTopLayout2);


	hLayout->addLayout(vLayout, 1);
	hLayout->addLayout(vLayout2, 3);

}

//耗时方法(高斯不同演示)
QPixmap SobelAndScharrWindow::handle() {
	Mat mats[5];
	option->showSobelAndScharr(filePath.toStdString().c_str(), gaussKSize, sobelKSize, mats);
	for (int i = 0;i <= 4;i++) {
		if (i <= 1) {
			QPixmap pixmap = ImageUtils::getPixmap(mats[i], 200, 200);
			imageTips[i]->setItemsPixmap(pixmap);
		}
		else {
			QPixmap pixmap = ImageUtils::getPixmap8(mats[i], 200, 200);
			imageTips[i]->setItemsPixmap(pixmap);
		}

	}

	return NULL;
}

SobelAndScharrWindow::~SobelAndScharrWindow()
{
}
