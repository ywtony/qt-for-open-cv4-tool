#include "ImageFuseWindow.h"

ImageFuseWindow::ImageFuseWindow(QWidget* parent)
	: BaseSceneView(parent)
{
	this->setWindowTitle("图像融合");
	this->setFixedSize(QSize(320, 480));

	QVBoxLayout* vLayout = new QVBoxLayout(this);

	//选择第一张图片
	QHBoxLayout* hLayout = new QHBoxLayout(this);
	EditText* et = new EditText(this);
	et->setEnabled(false);
	et->setFixedHeight(30);
	Button* btnChoiceBtn = new Button(this);
	btnChoiceBtn->setText("第一张图片");


	hLayout->addWidget(et);
	hLayout->addWidget(btnChoiceBtn);
	hLayout->setAlignment(Qt::AlignTop);

	//选择第二张图片
	QHBoxLayout* hLayout2 = new QHBoxLayout(this);
	EditText* et2 = new EditText(this);
	et2->setEnabled(false);
	et2->setFixedHeight(30);
	Button* btnChoiceBtn2 = new Button(this);
	btnChoiceBtn2->setText("第二张图片");


	hLayout2->addWidget(et2);
	hLayout2->addWidget(btnChoiceBtn2);
	hLayout2->setAlignment(Qt::AlignTop);

	Button* btnStart = new Button(this);
	btnStart->setText("开始融合");

	QLabel* labelTips1 = new QLabel(this);
	//labelTips1->setStyleSheet("QLabel{color:white}");
	labelTips1->setText("第一张图片原图");
	labelTips1->setFixedHeight(15);
	imageViewStep1 = new QLabel(this);

	QLabel* labelTips2 = new QLabel(this);
	//labelTips2->setStyleSheet("QLabel{color:white}");
	labelTips2->setText("第二张图片原图");
	labelTips2->setFixedHeight(15);
	imageViewStep2 = new QLabel(this);


	QLabel* labelTips3 = new QLabel(this);
	//labelTips2->setStyleSheet("QLabel{color:white}");
	labelTips3->setText("融合后的图片--->使用seekbar进度条改变两张图片的透明度");
	labelTips3->setFixedHeight(15);
	imageViewResult = new QLabel(this);

	Seekbar* seekbar = new Seekbar(this);

	vLayout->addLayout(hLayout);
	vLayout->addLayout(hLayout2);
	vLayout->addWidget(btnStart);

	vLayout->addWidget(labelTips1);
	vLayout->addWidget(imageViewStep1);
	vLayout->addWidget(labelTips2);
	vLayout->addWidget(imageViewStep2);
	vLayout->addWidget(labelTips3);
	vLayout->addWidget(seekbar);
	vLayout->addWidget(imageViewResult);

	vLayout->setAlignment(Qt::AlignTop);
	this->setLayout(vLayout);

	option = new BasePixelOption(this);
	//选择第一张图片
	connect(btnChoiceBtn, &Button::clicked, this, [=]() {
		filePath = QFileDialog::getOpenFileName(this, tr("请选择图片"), "C:/Users/DBF-DEV-103/Downloads/", tr("Image Files(*.jpg *.png *.webp *.jpeg)"));
		et->setText(filePath);

		});
	//选择第二张图片
	connect(btnChoiceBtn2, &Button::clicked, this, [=]() {
		filePath2 = QFileDialog::getOpenFileName(this, tr("请选择图片"), "C:/Users/DBF-DEV-103/Downloads/", tr("Image Files(*.jpg *.png *.webp *.jpeg)"));
		et2->setText(filePath2);

		});
	//开始融合
	connect(btnStart, &Button::clicked, this, [=]() {
		this->execute();
		});

	connect(seekbar, &Seekbar::valueChanged, this, [=](int value) {
		mAlpha = value / 100.0;
		qDebug() << "value:" << mAlpha;
		this->execute();
		});
}

QPixmap ImageFuseWindow::handle() {
	Mat mats[3];
	option->imageFuse(filePath.toStdString().c_str(), filePath2.toStdString().c_str(), mAlpha, mats);
	qDebug() << "融合成功开始显示第一张图片";
	QPixmap src = QPixmap::fromImage(ImageUtils::matToQImage(mats[0]));
	imageViewStep1->setPixmap(src.scaled(QSize(this->width() - 20, this->height() - 20), Qt::KeepAspectRatio));
	qDebug() << "融合成功开始显示第二张图片";
	QPixmap src2 = QPixmap::fromImage(ImageUtils::matToQImage(mats[1]));
	imageViewStep2->setPixmap(src2.scaled(QSize(this->width() - 20, this->height() - 20), Qt::KeepAspectRatio));
	qDebug() << "融合成功开始显示第三张图片";
	QPixmap reuslt = QPixmap::fromImage(ImageUtils::matToQImage(mats[2]));
	imageViewResult->setPixmap(reuslt.scaled(QSize(this->width() - 20, this->height() - 20), Qt::KeepAspectRatio));
	return NULL;
}

ImageFuseWindow::~ImageFuseWindow()
{
}
