#pragma once

#include <QWidget>
#include "BasePixelOption.h"
#include "../../common/base/BaseSceneView.h"
#include "../../common/button/Button.h"
#include "../../common/edittext/EditText.h"
#include <QFileDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QVBoxLayout>

class ShowOriginalImageWindow : public BaseSceneView
{
	Q_OBJECT

public:
	ShowOriginalImageWindow(QWidget *parent = nullptr);
	~ShowOriginalImageWindow();
protected:
	QPixmap handle();

private:
	QString filePath;
	BasePixelOption* option;
	QLabel* imageView;

};
