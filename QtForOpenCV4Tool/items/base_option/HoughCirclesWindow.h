#pragma once

#include <QWidget>
#include "BasePixelOption.h"
#include "../../common/base/BaseSceneView.h"
#include "../../common/button/Button.h"
#include "../../common/edittext/EditText.h"
#include "../../common/seekbar/Seekbar.h"
#include "../../common/minus_plus/MinusPlusWidget.h"
#include "../../common/seekbar/SeekBarTipsWidget.h"
#include "../../common/choice_image/ChoiceImageWidget.h"
#include "../../common/image_tip/ImageTip.h"
#include "../../common/layout/VariableGridView.h"

#include <QFileDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QVBoxLayout>
#include <iostream>
#include <QGridLayout>
#include <QButtonGroup>
#include <QRadioButton>
#include <QMetaType> 
#include <QMessageBox>

//根据以下四个参数来调节参数，参数设置的正确识别率还是可以的
class HoughCirclesWindow : public BaseSceneView
{
	Q_OBJECT

public:
	HoughCirclesWindow(QWidget* parent = nullptr);
	~HoughCirclesWindow();

protected:
	QPixmap handle();

private:
	QString filePath;
	BasePixelOption* option;
	ImageTip* imageTips[5];
	int param1 = 30;//两个圆心之间的最小距离，如果小于这个距离被认为是同心圆
	int param2 = 10;//累加器的值，值越小检测到的圆就越多
	int minRadius = 10;//能检测到的最小圆的半径
	int maxRadius = 17;//能检测到的最大圆的半径
};
