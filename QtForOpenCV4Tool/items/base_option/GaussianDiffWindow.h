#pragma once

#include <QWidget>
#include "BasePixelOption.h"
#include "../../common/base/BaseSceneView.h"
#include "../../common/button/Button.h"
#include "../../common/edittext/EditText.h"
#include "../../common/seekbar/Seekbar.h"
#include "../../common/minus_plus/MinusPlusWidget.h"
#include "../../common/seekbar/SeekBarTipsWidget.h"
#include "../../common/choice_image/ChoiceImageWidget.h"
#include "../../common/image_tip/ImageTip.h"
#include "../../common/layout/VariableGridView.h"

#include <QFileDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QVBoxLayout>
#include <iostream>
#include <QGridLayout>
#include <QButtonGroup>
#include <QRadioButton>
#include <QMetaType> 
#include <QMessageBox>

class GaussianDiffWindow : public BaseSceneView
{
	Q_OBJECT

public:
	GaussianDiffWindow(QWidget* parent = nullptr);
	~GaussianDiffWindow();

protected:
	QPixmap handle();

private:
	QString filePath;
	BasePixelOption* option;
	ImageTip* imageTips[6];
	int firstKSize = 3;
	int secondKSize = 15;

};
