#include "BilateralFilterWindow.h"

BilateralFilterWindow::BilateralFilterWindow(QWidget* parent)
	: BaseSceneView(parent)
{
	this->setWindowTitle("双边滤波->保留边缘的同时抑制平坦区域的噪声");
	this->setFixedSize(QSize(320, 800));
	QVBoxLayout* vLayout = new QVBoxLayout(this);

	QHBoxLayout* hLayout = new QHBoxLayout(this);
	EditText* et = new EditText(this);
	et->setEnabled(false);
	et->setFixedHeight(30);
	Button* btnChoiceBtn = new Button(this);
	btnChoiceBtn->setText("请选择图片");


	hLayout->addWidget(et);
	hLayout->addWidget(btnChoiceBtn);
	hLayout->setAlignment(Qt::AlignTop);

	QLabel* labelTips1 = new QLabel(this);
	//labelTips1->setStyleSheet("QLabel{color:white}");
	labelTips1->setText("显示原图");
	labelTips1->setFixedHeight(15);
	imageViewSrc = new QLabel(this);

	QLabel* labelTips2 = new QLabel(this);
	//labelTips2->setStyleSheet("QLabel{color:white}");
	labelTips2->setText("显示双边滤波");
	labelTips2->setFixedHeight(15);

	QLabel* seekbarTips = new QLabel(this);
	seekbarTips->setText("卷积核直径");
	seekbarTips->setFixedHeight(15);
	Seekbar* seekbar = new Seekbar(this);
	QLabel* seekbar1Tips = new QLabel(this);
	seekbar1Tips->setText("颜色空间方差");
	seekbar1Tips->setFixedHeight(15);
	Seekbar* seekbar1 = new Seekbar(this);
	QLabel* seekbar2Tips = new QLabel(this);
	seekbar2Tips->setText("坐标空间方差");
	seekbar2Tips->setFixedHeight(15);
	Seekbar* seekbar2 = new Seekbar(this);

	imageViewResult = new QLabel(this);

	vLayout->addLayout(hLayout);
	vLayout->addWidget(labelTips1);
	vLayout->addWidget(imageViewSrc);
	vLayout->addWidget(labelTips2);
	vLayout->addWidget(seekbarTips);
	vLayout->addWidget(seekbar);
	vLayout->addWidget(seekbar1Tips);
	vLayout->addWidget(seekbar1);
	vLayout->addWidget(seekbar2Tips);
	vLayout->addWidget(seekbar2);
	vLayout->addWidget(imageViewResult);
	vLayout->setAlignment(Qt::AlignTop);
	this->setLayout(vLayout);

	option = new BasePixelOption(this);

	connect(btnChoiceBtn, &Button::clicked, this, [=]() {
		filePath = QFileDialog::getOpenFileName(this, tr("请选择图片"), "C:/Users/DBF-DEV-103/Downloads/", tr("Image Files(*.jpg *.png *.webp *.jpeg)"));
		et->setText(filePath);
		this->execute();//执行handle耗时方法

		});

	connect(seekbar, &Seekbar::valueChanged, this, [=](int value) {
		g_d = value;
		qDebug() << "value:" << value;
		this->execute();
		});

	connect(seekbar1, &Seekbar::valueChanged, this, [=](int value) {
		g_sigmaColor = value;
		qDebug() << "value:" << value;
		this->execute();
		});
	connect(seekbar2, &Seekbar::valueChanged, this, [=](int value) {
		g_sigmaSpace = value;
		qDebug() << "value:" << value;
		this->execute();
		});


}
//耗时方法
QPixmap BilateralFilterWindow::handle() {
	Mat mats[2];
	option->showBilateralFilter(filePath.toStdString().c_str(), g_d, g_sigmaColor, g_sigmaSpace, mats);//双边滤波做测试
	QPixmap src = QPixmap::fromImage(ImageUtils::matToQImage(mats[0]));
	imageViewSrc->setPixmap(src.scaled(QSize(this->width() - 20, this->height() - 20), Qt::KeepAspectRatio));
	qDebug() << "结果";
	QPixmap src2 = QPixmap::fromImage(ImageUtils::matToQImage(mats[1]));
	imageViewResult->setPixmap(src2.scaled(QSize(this->width() - 20, this->height() - 20), Qt::KeepAspectRatio));
	return NULL;
}

BilateralFilterWindow::~BilateralFilterWindow()
{
}
