#include "ExtractEnglishLettersWindow.h"

ExtractEnglishLettersWindow::ExtractEnglishLettersWindow(QWidget* parent)
	: BaseSceneView(parent)
{
	this->setWindowTitle("开闭操作提取图像中的英文字母把图像中的直线去掉");
	this->setFixedSize(QSize(1000, 480));
	this->setGraphicsViewSize(1000, 480);
	QHBoxLayout* container = new QHBoxLayout(this);
	container->setAlignment(Qt::AlignLeft);
	/**左边操作栏的内容*******************************/
	QVBoxLayout* vLayout = new QVBoxLayout(this);
	ChoiceImageWidget* choiceImageWidget = new ChoiceImageWidget(this);
	choiceImageWidget->setFixedWidth(200);
	choiceImageWidget->setCallback([=](QString filePath) {
		this->filePath = filePath;
		qDebug() << "选择图片完成开始打印路径：" << filePath;
		this->execute();
		qDebug() << "开始对此路径执行耗时任务：" << filePath;
		});


	//调整高斯核的大小
	MinusPlusWidget* minusPlusWidget = new MinusPlusWidget(this);
	minusPlusWidget->setMinusClick([=](int value) {
		qDebug() << "-value:" << value;
		this->kSize -= 2;
		if (this->kSize <= 1) {
			this->kSize = 1;
		}
		this->execute();
		});
	minusPlusWidget->setPlusClick([=](int value) {
		qDebug() << "+value:" << value;
		this->kSize += 2;
		this->execute();
		});
	minusPlusWidget->setTipsContent("高斯模糊卷积核");
	//调整开操作卷积核大小
	MinusPlusWidget* minusPlusWidgetDilate = new MinusPlusWidget(this);
	minusPlusWidgetDilate->setMinusClick([=](int value) {
		qDebug() << "-value:" << value;
		this->dilateKSize -= 2;
		if (this->dilateKSize <= 1) {
			this->dilateKSize = 1;
		}
		this->execute();
		});

	minusPlusWidgetDilate->setPlusClick([=](int value) {
		qDebug() << "+value:" << value;
		this->dilateKSize += 2;
		this->execute();
		});
	minusPlusWidgetDilate->setTipsContent("膨胀卷积核大小调整");
	//调整毕操作卷积核大小
	MinusPlusWidget* minusPlusWidgetEredo = new MinusPlusWidget(this);
	minusPlusWidgetEredo->setMinusClick([=](int value) {
		qDebug() << "-value:" << value;
		this->erodeKsize -= 2;
		if (this->erodeKsize <= 1) {
			this->erodeKsize = 1;
		}
		this->execute();
		});

	minusPlusWidgetEredo->setPlusClick([=](int value) {
		qDebug() << "+value:" << value;
		this->erodeKsize += 2;
		this->execute();
		});
	minusPlusWidgetEredo->setTipsContent("开操作卷积核大小调整");

	SeekBarTipsWidget* seekBarTips = new SeekBarTipsWidget(this);
	seekBarTips->setFixedWidth(200);
	seekBarTips->initContent(0, 255, 1, "调整阈值大小", [=](int value) {
		//此处一旦加载就会初始化
		if (filePath != NULL) {
			qDebug() << "回调的阈值：" << value;
			this->thresholdMin = value;
			this->execute();
		}

		});
	seekBarTips->setSeekBarValue(this->thresholdMin);

	vLayout->addWidget(choiceImageWidget);
	vLayout->addWidget(minusPlusWidget);
	vLayout->addWidget(minusPlusWidgetDilate);
	vLayout->addWidget(minusPlusWidgetEredo);
	vLayout->addWidget(seekBarTips);
	vLayout->setAlignment(Qt::AlignTop);

	/**右边图片显示步骤的内容操作栏的内容*******************************/
	QVBoxLayout* vLayout1 = new QVBoxLayout(this);
	QLabel* labelTips1 = new QLabel(this);
	labelTips1->setText("（1）原图");
	labelTips1->setFixedHeight(15);
	this->imageViews[0] = new QLabel(this);

	QLabel* labelTips2 = new QLabel(this);
	labelTips2->setText("（2）原图->高斯模糊");
	labelTips2->setFixedHeight(15);
	this->imageViews[1] = new QLabel(this);



	vLayout1->addWidget(labelTips1);
	vLayout1->addWidget(this->imageViews[0]);
	vLayout1->addWidget(labelTips2);
	vLayout1->addWidget(this->imageViews[1]);
	vLayout1->setAlignment(Qt::AlignTop);


	QVBoxLayout* vLayout2 = new QVBoxLayout(this);
	QLabel* labelTips3 = new QLabel(this);
	labelTips3->setText("（3）原图->高斯模糊->灰度");
	labelTips3->setFixedHeight(15);
	this->imageViews[2] = new QLabel(this);

	QLabel* labelTips4 = new QLabel(this);
	labelTips4->setText("（4）原图->高斯模糊->灰度->二值化");
	labelTips4->setFixedHeight(15);
	this->imageViews[3] = new QLabel(this);


	vLayout2->addWidget(labelTips3);
	vLayout2->addWidget(this->imageViews[2]);
	vLayout2->addWidget(labelTips4);
	vLayout2->addWidget(this->imageViews[3]);
	vLayout2->setAlignment(Qt::AlignTop);


	QVBoxLayout* vLayout3 = new QVBoxLayout(this);
	QLabel* labelTips5 = new QLabel(this);
	labelTips5->setText("（5）原图->高斯模糊->灰度->二值化->膨胀");
	labelTips5->setFixedHeight(15);
	this->imageViews[4] = new QLabel(this);

	QLabel* labelTips6 = new QLabel(this);
	labelTips6->setText("（6）原图->高斯模糊->灰度->二值化->膨胀->腐蚀");
	labelTips6->setFixedHeight(15);
	this->imageViews[5] = new QLabel(this);

	vLayout3->addWidget(labelTips5);
	vLayout3->addWidget(this->imageViews[4]);
	vLayout3->addWidget(labelTips6);
	vLayout3->addWidget(this->imageViews[5]);
	vLayout3->setAlignment(Qt::AlignTop);


	container->addLayout(vLayout);
	container->addLayout(vLayout1);
	container->addLayout(vLayout2);
	container->addLayout(vLayout3);
	this->setLayout(container);

	option = new BasePixelOption(this);

}
//耗时方法
QPixmap ExtractEnglishLettersWindow::handle() {
	qDebug() << "已经进入到耗时任务内部了";
	Mat mats[6];
	option->showCleanImage(filePath.toStdString().c_str(), kSize, thresholdMin, thresholdMax, dilateKSize, erodeKsize, mats);
	qDebug() << "提取图片中的英文字母完成";
	QPixmap src = QPixmap::fromImage(ImageUtils::matToQImage(mats[0]));
	imageViews[0]->setPixmap(src.scaled(QSize(imgWidth, imgHeight), Qt::KeepAspectRatio));
	qDebug() << "结果";
	QPixmap src1 = QPixmap::fromImage(ImageUtils::matToQImage(mats[1]));
	imageViews[1]->setPixmap(src1.scaled(QSize(imgWidth, imgHeight), Qt::KeepAspectRatio));

	QPixmap src2 = QPixmap::fromImage(ImageUtils::matToQImage8(mats[2]));
	imageViews[2]->setPixmap(src2.scaled(QSize(imgWidth, imgHeight), Qt::KeepAspectRatio));

	QPixmap src3 = QPixmap::fromImage(ImageUtils::matToQImage8(mats[3]));
	imageViews[3]->setPixmap(src3.scaled(QSize(imgWidth, imgHeight), Qt::KeepAspectRatio));

	QPixmap src4 = QPixmap::fromImage(ImageUtils::matToQImage8(mats[4]));
	imageViews[4]->setPixmap(src4.scaled(QSize(imgWidth, imgHeight), Qt::KeepAspectRatio));

	QPixmap src5 = QPixmap::fromImage(ImageUtils::matToQImage8(mats[5]));
	imageViews[5]->setPixmap(src5.scaled(QSize(imgWidth, imgHeight), Qt::KeepAspectRatio));
	return NULL;
}
ExtractEnglishLettersWindow::~ExtractEnglishLettersWindow()
{
}
