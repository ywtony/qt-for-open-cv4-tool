#include "MatchTemplateWindow.h"

MatchTemplateWindow::MatchTemplateWindow(QWidget *parent)
	: BaseSceneView(parent)
{

	this->setWindowTitle("模版匹配");
	this->setFixedSize(1000, 480);
	this->setGraphicsViewSize(1000, 480);

	option = new BasePixelOption(this);

	//选择图片
	ChoiceImageWidget* choiceImageWidget = new ChoiceImageWidget(this);
	choiceImageWidget->setFixedWidth(200);
	choiceImageWidget->setCallback([=](QString filePath) {
		this->filePath = filePath;
		qDebug() << "选择图片完成开始打印路径：" << filePath;
		//this->execute();
		});

	ChoiceImageWidget* matchChoiceImageWidget = new ChoiceImageWidget(this);
	matchChoiceImageWidget->setFixedWidth(200);
	matchChoiceImageWidget->setCallback([=](QString filePath) {
		this->filePath_Match = filePath;
		qDebug() << "选择图片完成开始打印路径：" << filePath;
		this->execute();
		});


	//添加显示图片
	imageTips[0] = new ImageTip(this);
	imageTips[0]->setItemsTitle("原图(1)");
	imageTips[1] = new ImageTip(this);
	imageTips[1]->setItemsTitle("蓝色通道(2)");
	imageTips[2] = new ImageTip(this);
	imageTips[2]->setItemsTitle("绿色通道(3)");

	QHBoxLayout* hLayout = new QHBoxLayout(this);
	QVBoxLayout* vLayout = new QVBoxLayout(this);


	vLayout->addWidget(choiceImageWidget);
	vLayout->addWidget(matchChoiceImageWidget);
	vLayout->addWidget(imageTips[0]);
	vLayout->setAlignment(Qt::AlignTop);

	QVBoxLayout* vLayout2 = new QVBoxLayout(this);
	QHBoxLayout* hTopLayout = new QHBoxLayout(this);
	QHBoxLayout* hTopLayout2 = new QHBoxLayout(this);
	hTopLayout->addWidget(imageTips[1]);
	hTopLayout->addWidget(imageTips[2]);
	vLayout2->addLayout(hTopLayout);
	vLayout2->addLayout(hTopLayout2);


	hLayout->addLayout(vLayout, 1);
	hLayout->addLayout(vLayout2, 3);

}

//耗时方法(高斯不同演示)
QPixmap MatchTemplateWindow::handle() {
	Mat mats[3];
	option->showMatchTemplate(filePath.toStdString().c_str(), filePath_Match.toStdString().c_str(),mats);
	imageTips[0]->setItemsPixmap(ImageUtils::getPixmap(mats[0], 200, 200));
	imageTips[1]->setItemsPixmap(ImageUtils::getPixmap(mats[1], 200, 200));
	imageTips[2]->setItemsPixmap(ImageUtils::getPixmap(mats[2], 200, 200));
	return NULL;
}

MatchTemplateWindow::~MatchTemplateWindow()
{
}
