#include "MakeBorderCopyWindow.h"

MakeBorderCopyWindow::MakeBorderCopyWindow(QWidget* parent)
	: BaseSceneView(parent)
{
	this->setWindowTitle("自定义算子x、y方向算子展示及合并");
	this->setFixedSize(1000, 480);
	this->setGraphicsViewSize(1000, 480);

	option = new BasePixelOption(this);

	//选择图片
	ChoiceImageWidget* choiceImageWidget = new ChoiceImageWidget(this);
	choiceImageWidget->setFixedWidth(200);
	choiceImageWidget->setCallback([=](QString filePath) {
		this->filePath = filePath;
		qDebug() << "选择图片完成开始打印路径：" << filePath;
		this->execute();
		});

	MinusPlusWidget* borderWidthBtn = new MinusPlusWidget(this);
	borderWidthBtn->setMinusAndPlus("边缘填充宽度", [=](int minusValue) {
		qDebug() << "-value:" << minusValue;
		this->borderWidth -= 2;
		if (this->borderWidth <= 1) {
			this->borderWidth = 1;
		}
		this->execute();
		}, [=](int plusMinus) {
			qDebug() << "+value:" << plusMinus;
			this->borderWidth += 2;
			this->execute();
			});

	//添加显示图片
	imageTips[0] = new ImageTip(this);
	imageTips[0]->setItemsTitle("原图");
	imageTips[1] = new ImageTip(this);
	imageTips[1]->setItemsTitle("系统默认填充方式");
	imageTips[2] = new ImageTip(this);
	imageTips[2]->setItemsTitle("填充边缘，用指定的像素值");
	imageTips[3] = new ImageTip(this);
	imageTips[3]->setItemsTitle("填充边缘像素用已知边缘像素值");
	imageTips[4] = new ImageTip(this);
	imageTips[4]->setItemsTitle("填充边缘像素用已知边缘像素值");

	QHBoxLayout* hLayout = new QHBoxLayout(this);
	QVBoxLayout* vLayout = new QVBoxLayout(this);


	vLayout->addWidget(choiceImageWidget);
	vLayout->addWidget(borderWidthBtn);
	vLayout->addWidget(imageTips[0]);
	vLayout->setAlignment(Qt::AlignTop);

	QVBoxLayout* vLayout2 = new QVBoxLayout(this);
	QHBoxLayout* hTopLayout = new QHBoxLayout(this);
	QHBoxLayout* hTopLayout2 = new QHBoxLayout(this);
	hTopLayout->addWidget(imageTips[1]);
	hTopLayout->addWidget(imageTips[2]);

	hTopLayout2->addWidget(imageTips[3]);
	hTopLayout2->addWidget(imageTips[4]);
	vLayout2->addLayout(hTopLayout);
	vLayout2->addLayout(hTopLayout2);


	hLayout->addLayout(vLayout, 1);
	hLayout->addLayout(vLayout2, 3);

}

//耗时方法(高斯不同演示)
QPixmap MakeBorderCopyWindow::handle() {
	Mat mats[5];
	option->showCopyMakeBorder(filePath.toStdString().c_str(), borderWidth, mats);
	for (int i = 0;i <= 4;i++) {
		QPixmap pixmap = ImageUtils::getPixmap(mats[i], 200, 200);
		imageTips[i]->setItemsPixmap(pixmap);
	}

	return NULL;
}

MakeBorderCopyWindow::~MakeBorderCopyWindow()
{
}
