#include "ExtractLineWindow.h"

ExtractLineWindow::ExtractLineWindow(QWidget* parent)
	: BaseSceneView(parent)
{
	qRegisterMetaType<cv::Mat>("cv::Mat");
	qRegisterMetaType<QList<cv::Mat*>>("QList<cv::Mat*>");
	this->setWindowTitle("提取图像中的直线");
	this->setFixedSize(800, 480);
	this->setGraphicsViewSize(800, 480);

	option = new BasePixelOption(this);

	//选择图片
	ChoiceImageWidget* choiceImageWidget = new ChoiceImageWidget(this);
	choiceImageWidget->setFixedWidth(200);
	choiceImageWidget->setCallback([=](QString filePath) {
		this->filePath = filePath;
		qDebug() << "选择图片完成开始打印路径：" << filePath;
		//this->execute();
		Mat src = imread(filePath.toStdString().c_str());
		Mat src2 = imread(filePath.toStdString().c_str());
		Mat mats[1];
		mats[0] = src;
		//mats[1] = src2;
		QList<ImageTip*> mViews = QList<ImageTip*>();
		ImageTip* tip = new ImageTip(this);
		QPixmap pixmap = QPixmap::fromImage(ImageUtils::matToQImage(mats[0]));
		tip->setItemsContent("原图", pixmap.scaled(QSize(this->width() / 3 * 2, this->height()), Qt::KeepAspectRatio));
		/*ImageTip* tip2 = new ImageTip(this);
		QPixmap pixmap2 = QPixmap::fromImage(ImageUtils::matToQImage(mats[1]));
		tip2->setItemsContent("原图", pixmap2.scaled(QSize(this->width() / 3 * 2, this->height()), Qt::KeepAspectRatio));*/
		mViews.append(tip);
		//mViews.append(tip2);
		//vgv->setAdapter(mViews, QSize(this->width() / 3 * 2, this->height()));
		});
	//形态学梯度卷积核大小调整
	MinusPlusWidget* minusPlusWidgetDilate = new MinusPlusWidget(this);
	minusPlusWidgetDilate->setMinusClick([=](int value) {
		qDebug() << "-value:" << value;
		this->kSize -= 2;
		if (this->kSize <= 1) {
			this->kSize = 1;
		}
		this->execute();
		});

	minusPlusWidgetDilate->setPlusClick([=](int value) {
		qDebug() << "+value:" << value;
		this->kSize += 2;
		this->execute();
		});
	minusPlusWidgetDilate->setTipsContent("从图像中提取直线");

	//添加显示图片
	imageTips[0] = new ImageTip(this);
	imageTips[1] = new ImageTip(this);

	QHBoxLayout* hLayout = new QHBoxLayout(this);
	QVBoxLayout* vLayout = new QVBoxLayout(this);
	vLayout->addWidget(choiceImageWidget);
	vLayout->addWidget(minusPlusWidgetDilate);
	vLayout->setAlignment(Qt::AlignTop);

	QVBoxLayout* vLayout2 = new QVBoxLayout(this);
	vgv = new VariableGridView(this);
	vLayout2->addWidget(vgv);

	hLayout->addLayout(vLayout, 1);
	hLayout->addLayout(vLayout2, 3);

	//connect(mThread, &HandlerThread::done, this, [=](cv::Mat* mats) {
	//	qDebug() << "接收到QThread信号";
	//	QList<ImageTip*> mViews = QList<ImageTip*>();
	//	ImageTip* tip = new ImageTip(this);
	//	QPixmap src = QPixmap::fromImage(ImageUtils::matToQImage(mats[0]));
	//	tip->setItemsContent("原图", src.scaled(QSize(this->width() / 3 * 2, this->height()), Qt::KeepAspectRatio));
	//	mViews.append(tip);
	//	vgv->setAdapter(mViews, QSize(this->width() / 3 * 2, this->height()));
	//	});
	
}

//耗时方法
QPixmap ExtractLineWindow::handle() {
	qDebug() << "形态学开操作";
	Mat mats[1];
	option->showLines(filePath.toStdString().c_str(), mats);
	emit mThread->done(mats);
	/*option->showMorphologicalGradient(filePath.toStdString().c_str(), kSize, mType, mats);
	qDebug() << "开操作";
	QPixmap src = QPixmap::fromImage(ImageUtils::matToQImage(mats[0]));
	imageTips[0]->setItemsContent("原图", src.scaled(QSize(400, 300), Qt::KeepAspectRatio));
	qDebug() << "结果";
	QPixmap src1 = QPixmap::fromImage(ImageUtils::matToQImage8(mats[1]));
	imageTips[1]->setItemsContent("梯度图像", src1.scaled(QSize(400, 300), Qt::KeepAspectRatio));*/
	return NULL;
}

//void ExtractLineWindow::showGrid(Mat* mats) {

//}

ExtractLineWindow::~ExtractLineWindow()
{

}
