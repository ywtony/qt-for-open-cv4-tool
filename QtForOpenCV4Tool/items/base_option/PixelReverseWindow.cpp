#include "PixelReverseWindow.h"

PixelReverseWindow::PixelReverseWindow(QWidget *parent)
	: BaseSceneView(parent)
{
	this->setWindowTitle("像素取反");
	//this->setStyleSheet("QWidget{background-color:#000000}");

	QVBoxLayout* vLayout = new QVBoxLayout(this);

	QHBoxLayout* hLayout = new QHBoxLayout(this);
	EditText* et = new EditText(this);
	et->setEnabled(false);
	et->setFixedHeight(30);
	Button* btnChoiceBtn = new Button(this);
	btnChoiceBtn->setText("请选择图片");


	hLayout->addWidget(et);
	hLayout->addWidget(btnChoiceBtn);
	hLayout->setAlignment(Qt::AlignTop);

	QLabel* labelTips1 = new QLabel(this);
	//labelTips1->setStyleSheet("QLabel{color:white}");
	labelTips1->setText("显示原图");
	labelTips1->setFixedHeight(15);
	imageViewSrc = new QLabel(this);

	QLabel* labelTips2 = new QLabel(this);
	//labelTips2->setStyleSheet("QLabel{color:white}");
	labelTips2->setText("像素取反");
	labelTips2->setFixedHeight(15);
	imageViewResult = new QLabel(this);

	vLayout->addLayout(hLayout);
	vLayout->addWidget(labelTips1);
	vLayout->addWidget(imageViewSrc);
	vLayout->addWidget(labelTips2);
	vLayout->addWidget(imageViewResult);
	vLayout->setAlignment(Qt::AlignTop);
	this->setLayout(vLayout);

	option = new BasePixelOption(this);

	connect(btnChoiceBtn, &Button::clicked, this, [=]() {
		filePath = QFileDialog::getOpenFileName(this, tr("请选择图片"), "C:/Users/DBF-DEV-103/Downloads/", tr("Image Files(*.jpg *.png *.webp *.jpeg)"));
		et->setText(filePath);
		this->execute();//执行handle耗时方法

		});
}

void PixelReverseWindow::paintEvent(QPaintEvent* event) {

}
//执行耗时任务
QPixmap PixelReverseWindow::handle() {
	list<QPixmap> pixmaps = option->pixleReverse(filePath.toStdString().c_str());
	//list<QPixmap>::iterator it = pixmaps.begin();
	QPixmap src = pixmaps.front();
	imageViewSrc->setPixmap(src.scaled(QSize(this->width()-20,this->height()-20), Qt::KeepAspectRatio));
	QPixmap result = pixmaps.back();
	imageViewResult->setPixmap(result.scaled(QSize(this->width() - 20, this->height()-20), Qt::KeepAspectRatio));
	/*while (it != pixmaps.end()) {
		imageViewSrc->setPixmap(it->scaled(imageViewSrc->size(), Qt::KeepAspectRatio));
		it++;
	}*/

	
	return NULL;
}
PixelReverseWindow::~PixelReverseWindow()
{
}
