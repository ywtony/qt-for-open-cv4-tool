#include "Filter2DMergeXYCustomKernelWindow.h"

Filter2DMergeXYCustomKernelWindow::Filter2DMergeXYCustomKernelWindow(QWidget *parent)
	: BaseSceneView(parent)
{
	this->setWindowTitle("自定义算子x、y方向算子展示及合并");
	this->setFixedSize(1000, 480);
	this->setGraphicsViewSize(1000, 480);

	option = new BasePixelOption(this);

	//选择图片
	ChoiceImageWidget* choiceImageWidget = new ChoiceImageWidget(this);
	choiceImageWidget->setFixedWidth(200);
	choiceImageWidget->setCallback([=](QString filePath) {
		this->filePath = filePath;
		qDebug() << "选择图片完成开始打印路径：" << filePath;
		this->execute();
		});

	//添加显示图片
	imageTips[0] = new ImageTip(this);
	imageTips[0]->setItemsTitle("原图");
	imageTips[1] = new ImageTip(this);
	imageTips[1]->setItemsTitle("(3,3)robert x方向算子");
	imageTips[2] = new ImageTip(this);
	imageTips[2]->setItemsTitle("(3,3)robert y方向上算子");
	imageTips[3] = new ImageTip(this);
	imageTips[3]->setItemsTitle("(3,3)robert x y算子合并");
	imageTips[4] = new ImageTip(this);
	imageTips[4]->setItemsTitle("(3,3)sobel x方向上算子");
	imageTips[5] = new ImageTip(this);
	imageTips[5]->setItemsTitle("(3,3)sobel y方向上算子");
	imageTips[6] = new ImageTip(this);
	imageTips[6]->setItemsTitle("(3,3)sobel x y方向算子合并");

	QHBoxLayout* hLayout = new QHBoxLayout(this);
	QVBoxLayout* vLayout = new QVBoxLayout(this);


	vLayout->addWidget(choiceImageWidget);
	vLayout->addWidget(imageTips[0]);
	vLayout->setAlignment(Qt::AlignTop);

	QVBoxLayout* vLayout2 = new QVBoxLayout(this);
	QHBoxLayout* hTopLayout = new QHBoxLayout(this);
	QHBoxLayout* hTopLayout2 = new QHBoxLayout(this);
	hTopLayout->addWidget(imageTips[1]);
	hTopLayout->addWidget(imageTips[2]);
	hTopLayout->addWidget(imageTips[3]);

	hTopLayout2->addWidget(imageTips[4]);
	hTopLayout2->addWidget(imageTips[5]);
	hTopLayout2->addWidget(imageTips[6]);
	vLayout2->addLayout(hTopLayout);
	vLayout2->addLayout(hTopLayout2);


	hLayout->addLayout(vLayout, 1);
	hLayout->addLayout(vLayout2, 3);

}

//耗时方法(高斯不同演示)
QPixmap Filter2DMergeXYCustomKernelWindow::handle() {
	Mat mats[7];
	option->showCustomKernelFilter2DMergeXY(filePath.toStdString().c_str(), mats);
	for (int i = 0;i <= 6;i++) {
		QPixmap pixmap = ImageUtils::getPixmap(mats[i], 200, 200);
		imageTips[i]->setItemsPixmap(pixmap);
	}

	return NULL;
}

Filter2DMergeXYCustomKernelWindow::~Filter2DMergeXYCustomKernelWindow()
{
}
