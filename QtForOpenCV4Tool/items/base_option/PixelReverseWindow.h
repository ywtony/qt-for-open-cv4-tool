#pragma once

#include <QWidget>
#include "BasePixelOption.h"
#include "../../common/base/BaseSceneView.h"
#include "../../common/button/Button.h"
#include "../../common/edittext/EditText.h"
#include <QFileDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QVBoxLayout>
#include <iostream>
#include <QSize>

//像素取反窗口
class PixelReverseWindow : public BaseSceneView
{
	Q_OBJECT

public:
	PixelReverseWindow(QWidget* parent = nullptr);
	~PixelReverseWindow();

protected:
	QPixmap handle();
	void paintEvent(QPaintEvent* event);

private:
	QString filePath;
	BasePixelOption* option;
	QLabel* imageViewSrc;
	QLabel* imageViewResult;

};
