#pragma once

#include <QWidget>
#include "BasePixelOption.h"
#include "../../common/base/BaseSceneView.h"
#include "../../common/button/Button.h"
#include "../../common/edittext/EditText.h"
#include <QFileDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QVBoxLayout>
#include <iostream>
#include <QSize>

class DrawShapeWindow : public BaseSceneView
{
	Q_OBJECT

public:
	DrawShapeWindow(QWidget *parent = nullptr);
	~DrawShapeWindow();

protected:
	QPixmap handle();

private:
	QString filePath;
	BasePixelOption* option;
	QLabel* imageViewSrc;
	QLabel* imageViewResult;

};
