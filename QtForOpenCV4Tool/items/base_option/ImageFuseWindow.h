#pragma once

#include <QWidget>
#include "BasePixelOption.h"
#include "../../common/base/BaseSceneView.h"
#include "../../common/button/Button.h"
#include "../../common/edittext/EditText.h"
#include "../../common/seekbar/Seekbar.h"
#include <QFileDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QVBoxLayout>
#include <iostream>
#include <QSize>


//图像融合
class ImageFuseWindow : public BaseSceneView
{
	Q_OBJECT

public:
	ImageFuseWindow(QWidget *parent = nullptr);
	~ImageFuseWindow();

protected:
	QPixmap handle();

private:
	QString filePath;
	QString filePath2;
	BasePixelOption* option;
	QLabel* imageViewStep1;
	QLabel* imageViewStep2;
	QLabel* imageViewResult;
	float mAlpha = 0.5;
};
