#pragma once

#include <QWidget>
#include "BasePixelOption.h"
#include "../../common/base/BaseSceneView.h"
#include "../../common/button/Button.h"
#include "../../common/edittext/EditText.h"
#include <QFileDialog>
#include <QHBoxLayout>

class BasicPixelOptionWindow : public BaseSceneView
{
	Q_OBJECT

public:
	BasicPixelOptionWindow(QWidget* parent = nullptr);
	~BasicPixelOptionWindow();

protected:
	QPixmap handle();

private:
	BasePixelOption* option;

};
