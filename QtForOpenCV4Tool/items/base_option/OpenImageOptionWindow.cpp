#include "OpenImageOptionWindow.h"

OpenImageOptionWindow::OpenImageOptionWindow(QWidget *parent)
	: BaseSceneView(parent)
{
	this->setWindowTitle("形态学开操作");
	this->setFixedSize(QSize(1000, 480));
	this->setGraphicsViewSize(1000, 480);
	QHBoxLayout* container = new QHBoxLayout(this);
	container->setAlignment(Qt::AlignLeft);
	/**左边操作栏的内容*******************************/
	QVBoxLayout* vLayout = new QVBoxLayout(this);
	ChoiceImageWidget* choiceImageWidget = new ChoiceImageWidget(this);
	choiceImageWidget->setFixedWidth(200);
	choiceImageWidget->setCallback([=](QString filePath) {
		this->filePath = filePath;
		qDebug() << "选择图片完成开始打印路径：" << filePath;
		this->execute();
		});


	
	//调整膨胀卷积核大小
	MinusPlusWidget* minusPlusWidgetDilate = new MinusPlusWidget(this);
	minusPlusWidgetDilate->setMinusClick([=](int value) {
		qDebug() << "-value:" << value;
		this->dilateKSize -= 2;
		if (this->dilateKSize <= 1) {
			this->dilateKSize = 1;
		}
		this->execute();
		});

	minusPlusWidgetDilate->setPlusClick([=](int value) {
		qDebug() << "+value:" << value;
		this->dilateKSize += 2;
		this->execute();
		});
	minusPlusWidgetDilate->setTipsContent("膨胀卷积核大小调整");
	//调整腐蚀卷积核大小
	MinusPlusWidget* minusPlusWidgetEredo = new MinusPlusWidget(this);
	minusPlusWidgetEredo->setMinusClick([=](int value) {
		qDebug() << "-value:" << value;
		this->erodeKsize -= 2;
		if (this->erodeKsize <= 1) {
			this->erodeKsize = 1;
		}
		this->execute();
		});

	minusPlusWidgetEredo->setPlusClick([=](int value) {
		qDebug() << "+value:" << value;
		this->erodeKsize += 2;
		this->execute();
		});
	minusPlusWidgetEredo->setTipsContent("腐蚀卷积核大小调整");


	vLayout->addWidget(choiceImageWidget);
	vLayout->addWidget(minusPlusWidgetDilate);
	vLayout->addWidget(minusPlusWidgetEredo);
	vLayout->setAlignment(Qt::AlignTop);

	/**右边图片显示步骤的内容操作栏的内容*******************************/
	QVBoxLayout* vLayout1 = new QVBoxLayout(this);
	QLabel* labelTips1 = new QLabel(this);
	labelTips1->setText("（1）原图");
	labelTips1->setFixedHeight(15);
	this->imageViews[0] = new QLabel(this);
	vLayout1->addWidget(labelTips1);
	vLayout1->addWidget(this->imageViews[0]);
	vLayout1->setAlignment(Qt::AlignTop);


	QVBoxLayout* vLayout2 = new QVBoxLayout(this);
	QLabel* labelTips3 = new QLabel(this);
	labelTips3->setText("（2）原图->腐蚀");
	labelTips3->setFixedHeight(15);
	this->imageViews[1] = new QLabel(this);

	QLabel* labelTips4 = new QLabel(this);
	labelTips4->setText("（3）原图->膨胀");
	labelTips4->setFixedHeight(15);
	this->imageViews[2] = new QLabel(this);


	vLayout2->addWidget(labelTips3);
	vLayout2->addWidget(this->imageViews[1]);
	vLayout2->addWidget(labelTips4);
	vLayout2->addWidget(this->imageViews[2]);
	vLayout2->setAlignment(Qt::AlignTop);




	container->addLayout(vLayout);
	container->addLayout(vLayout1);
	container->addLayout(vLayout2);
	this->setLayout(container);

	option = new BasePixelOption(this);

}
//耗时方法
QPixmap OpenImageOptionWindow::handle() {
	qDebug() << "形态学开操作";
	Mat mats[3];
	option->showImageOpen(filePath.toStdString().c_str(), erodeKsize, dilateKSize, mats);
	qDebug() << "开操作";
	QPixmap src = QPixmap::fromImage(ImageUtils::matToQImage(mats[0]));
	imageViews[0]->setPixmap(src.scaled(QSize(imgWidth, imgHeight), Qt::KeepAspectRatio));
	qDebug() << "结果";
	QPixmap src1 = QPixmap::fromImage(ImageUtils::matToQImage8(mats[1]));
	imageViews[1]->setPixmap(src1.scaled(QSize(imgWidth, imgHeight), Qt::KeepAspectRatio));

	QPixmap src2 = QPixmap::fromImage(ImageUtils::matToQImage8(mats[2]));
	imageViews[2]->setPixmap(src2.scaled(QSize(imgWidth, imgHeight), Qt::KeepAspectRatio));
	return NULL;
}

OpenImageOptionWindow::~OpenImageOptionWindow()
{
}
