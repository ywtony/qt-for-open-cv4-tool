#include "EqualizeHistColorImageWindow.h"

EqualizeHistColorImageWindow::EqualizeHistColorImageWindow(QWidget *parent)
	: BaseSceneView(parent)
{
	this->setWindowTitle("直方图均衡话彩色图片");
	this->setFixedSize(1000, 480);
	this->setGraphicsViewSize(1000, 480);

	option = new BasePixelOption(this);

	//选择图片
	ChoiceImageWidget* choiceImageWidget = new ChoiceImageWidget(this);
	choiceImageWidget->setFixedWidth(200);
	choiceImageWidget->setCallback([=](QString filePath) {
		this->filePath = filePath;
		qDebug() << "选择图片完成开始打印路径：" << filePath;
		this->execute();
		});


	
	//添加显示图片
	imageTips[0] = new ImageTip(this);
	imageTips[0]->setItemsTitle("原图(1)");
	imageTips[1] = new ImageTip(this);
	imageTips[1]->setItemsTitle("过度图片");
	imageTips[2] = new ImageTip(this);
	imageTips[2]->setItemsTitle("过度图片");
	imageTips[3] = new ImageTip(this);
	imageTips[3]->setItemsTitle("过度图片");
	imageTips[4] = new ImageTip(this);
	imageTips[4]->setItemsTitle("过度图片");
	imageTips[5] = new ImageTip(this);
	imageTips[5]->setItemsTitle("过度图片");
	imageTips[6] = new ImageTip(this);
	imageTips[6]->setItemsTitle("过度图片");
	imageTips[7] = new ImageTip(this);
	imageTips[7]->setItemsTitle("最终图");

	QHBoxLayout* hLayout = new QHBoxLayout(this);
	QVBoxLayout* vLayout = new QVBoxLayout(this);


	vLayout->addWidget(choiceImageWidget);
	vLayout->addWidget(imageTips[0]);
	vLayout->addWidget(imageTips[1]);
	vLayout->setAlignment(Qt::AlignTop);

	QVBoxLayout* vLayout2 = new QVBoxLayout(this);
	QHBoxLayout* hTopLayout = new QHBoxLayout(this);
	QHBoxLayout* hTopLayout2 = new QHBoxLayout(this);
	hTopLayout->addWidget(imageTips[2]);
	hTopLayout->addWidget(imageTips[3]);
	hTopLayout->addWidget(imageTips[4]);
	hTopLayout2->addWidget(imageTips[5]);
	hTopLayout2->addWidget(imageTips[6]);
	hTopLayout2->addWidget(imageTips[7]);
	vLayout2->addLayout(hTopLayout);
	vLayout2->addLayout(hTopLayout2);


	hLayout->addLayout(vLayout, 1);
	hLayout->addLayout(vLayout2, 3);

}

//耗时方法(高斯不同演示)
QPixmap EqualizeHistColorImageWindow::handle() {
	Mat mats[8];
	option->showEqualizeHistColorImage(filePath.toStdString().c_str(),mats);
	imageTips[0]->setItemsPixmap(ImageUtils::getPixmap(mats[0], 200, 200));
	imageTips[1]->setItemsPixmap(ImageUtils::getPixmap8(mats[1], 200, 200));
	imageTips[2]->setItemsPixmap(ImageUtils::getPixmap8(mats[2], 200, 200));
	imageTips[3]->setItemsPixmap(ImageUtils::getPixmap8(mats[3], 200, 200));
	imageTips[4]->setItemsPixmap(ImageUtils::getPixmap8(mats[4], 200, 200));
	imageTips[5]->setItemsPixmap(ImageUtils::getPixmap8(mats[5], 200, 200));
	imageTips[6]->setItemsPixmap(ImageUtils::getPixmap8(mats[6], 200, 200));
	imageTips[7]->setItemsPixmap(ImageUtils::getPixmap(mats[7], 200, 200));
	return NULL;
}

EqualizeHistColorImageWindow::~EqualizeHistColorImageWindow()
{
}
