#pragma once

#include <QWidget>
#include "BasePixelOption.h"
#include "../../common/base/BaseSceneView.h"
#include "../../common/button/Button.h"
#include "../../common/edittext/EditText.h"
#include "../../common/seekbar/Seekbar.h"
#include "../../common/minus_plus/MinusPlusWidget.h"
#include "../../common/seekbar/SeekBarTipsWidget.h"
#include "../../common/choice_image/ChoiceImageWidget.h"
#include "../../common/image_tip/ImageTip.h"

#include <QFileDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QVBoxLayout>
#include <iostream>
#include <QGridLayout>
#include <QButtonGroup>
#include <QRadioButton>

//形态学梯度
class MorphologicalGradientWindow : public BaseSceneView
{
	Q_OBJECT

public:
	MorphologicalGradientWindow(QWidget *parent = nullptr);
	~MorphologicalGradientWindow();

protected:
	QPixmap handle();

private:
	QString filePath;
	BasePixelOption* option;
	ImageTip *imageTips[3];
	int kSize = 3;
	int mType = 3;

};
