#include "HoughLineWindow.h"

HoughLineWindow::HoughLineWindow(QWidget *parent)
	: BaseSceneView(parent)
{
	this->setWindowTitle("Canny边缘检测");
	this->setFixedSize(1000, 480);
	this->setGraphicsViewSize(1000, 480);

	option = new BasePixelOption(this);

	//选择图片
	ChoiceImageWidget* choiceImageWidget = new ChoiceImageWidget(this);
	choiceImageWidget->setFixedWidth(200);
	choiceImageWidget->setCallback([=](QString filePath) {
		this->filePath = filePath;
		qDebug() << "选择图片完成开始打印路径：" << filePath;
		this->execute();
		});


	MinusPlusWidget* cannySobelKSizeBtn = new MinusPlusWidget(this);
	cannySobelKSizeBtn->setMinusAndPlus("霍夫直线检测", [=](int minusValue) {
		qDebug() << "-value:" << minusValue;
		this->cannySobelKSize -= 2;
		if (this->cannySobelKSize <= 1) {
			this->cannySobelKSize = 1;
		}
		this->execute();
		}, [=](int plusMinus) {
			qDebug() << "+value:" << plusMinus;
			this->cannySobelKSize += 2;
			this->execute();
			});


	//添加滑动条
	SeekBarTipsWidget* seekbar1 = new SeekBarTipsWidget(this);
	seekbar1->initContent(0, 255, 1, "最小阈值调整", [=](int value) {
		if (filePath != NULL) {
			this->threshold_min = value;
			this->execute();
		}

		});
	seekbar1->setSeekBarValue(this->threshold_min);

	SeekBarTipsWidget* seekbar2 = new SeekBarTipsWidget(this);

	seekbar2->initContent(0, 255, 2, "最小阈值调整", [=](int value) {
		if (filePath != NULL) {
			this->threshold_max = value;
			this->execute();
		}
		});
	seekbar2->setSeekBarValue(this->threshold_max);

	MinusPlusWidget* rhoBtn = new MinusPlusWidget(this);
	rhoBtn->setMinusAndPlus("搜索步长", [=](int minusValue) {
		qDebug() << "-value:" << minusValue;
		this->rho -= 1;
		if (this->rho <= 1) {
			this->rho = 1;
		}
		this->execute();
		}, [=](int plusMinus) {
			qDebug() << "+value:" << plusMinus;
			this->rho += 1;
			this->execute();
			});

	MinusPlusWidget* threshold_pBtn = new MinusPlusWidget(this);
	threshold_pBtn->setMinusAndPlus("大于此值会被检测到", [=](int minusValue) {
		qDebug() << "-value:" << minusValue;
		this->threshold_p -= 1;
		if (this->threshold_p <= 1) {
			this->threshold_p = 1;
		}
		this->execute();
		}, [=](int plusMinus) {
			qDebug() << "+value:" << plusMinus;
			this->threshold_p += 1;
			this->execute();
			});

	MinusPlusWidget* minLineLengthBtn = new MinusPlusWidget(this);
	threshold_pBtn->setMinusAndPlus("线段不能比这个小", [=](int minusValue) {
		qDebug() << "-value:" << minusValue;
		this->minLineLength -= 2;
		if (this->minLineLength <= 1) {
			this->minLineLength = 1;
		}
		this->execute();
		}, [=](int plusMinus) {
			qDebug() << "+value:" << plusMinus;
			this->minLineLength += 2;
			this->execute();
			});

	MinusPlusWidget* maxLineGapBtn = new MinusPlusWidget(this);
	maxLineGapBtn->setMinusAndPlus("允许点到点连接最大距离", [=](int minusValue) {
		qDebug() << "-value:" << minusValue;
		this->maxLineGap -= 2;
		if (this->maxLineGap <= 1) {
			this->maxLineGap = 1;
		}
		this->execute();
		}, [=](int plusMinus) {
			qDebug() << "+value:" << plusMinus;
			this->maxLineGap += 2;
			this->execute();
			});

	//添加显示图片
	imageTips[0] = new ImageTip(this);
	imageTips[0]->setItemsTitle("原图");
	imageTips[1] = new ImageTip(this);
	imageTips[1]->setItemsTitle("转灰度图像");
	imageTips[2] = new ImageTip(this);
	imageTips[2]->setItemsTitle("边缘检测结果");
	imageTips[3] = new ImageTip(this);
	imageTips[3]->setItemsTitle("最后绘制结果");
	//imageTips[4] = new ImageTip(this);
	//imageTips[4]->setItemsTitle("边缘检测结果");
	//imageTips[5] = new ImageTip(this);
	//imageTips[5]->setItemsTitle("最后的结果");

	QHBoxLayout* hLayout = new QHBoxLayout(this);
	QVBoxLayout* vLayout = new QVBoxLayout(this);


	vLayout->addWidget(choiceImageWidget);
	vLayout->addWidget(cannySobelKSizeBtn);
	//vLayout->addWidget(sobelKSizeBtn);
	vLayout->addWidget(seekbar1);
	vLayout->addWidget(seekbar2);
	vLayout->addWidget(rhoBtn);
	vLayout->addWidget(threshold_pBtn);
	vLayout->addWidget(minLineLengthBtn);
	vLayout->addWidget(maxLineGapBtn);
	vLayout->setAlignment(Qt::AlignTop);

	QVBoxLayout* vLayout2 = new QVBoxLayout(this);
	QHBoxLayout* hTopLayout = new QHBoxLayout(this);
	QHBoxLayout* hTopLayout2 = new QHBoxLayout(this);
	hTopLayout->addWidget(imageTips[0]);
	hTopLayout->addWidget(imageTips[1]);
	hTopLayout->addWidget(imageTips[2]);
	hTopLayout2->addWidget(imageTips[3]);
	//hTopLayout2->addWidget(imageTips[4]);
	//hTopLayout2->addWidget(imageTips[5]);
	vLayout2->addLayout(hTopLayout);
	vLayout2->addLayout(hTopLayout2);


	hLayout->addLayout(vLayout, 1);
	hLayout->addLayout(vLayout2, 3);

}

//耗时方法(高斯不同演示)
QPixmap HoughLineWindow::handle() {
	Mat mats[5];
	option->showHoughLine(filePath.toStdString().c_str(), 
		threshold_min, threshold_max, cannySobelKSize,rho, threshold_p, minLineLength, maxLineGap,mats);
	imageTips[0]->setItemsPixmap(ImageUtils::getPixmap(mats[0], 200, 200));
	imageTips[1]->setItemsPixmap(ImageUtils::getPixmap8(mats[1], 200, 200));
	imageTips[2]->setItemsPixmap(ImageUtils::getPixmap8(mats[2], 200, 200));
	imageTips[3]->setItemsPixmap(ImageUtils::getPixmap(mats[3], 200, 200));
	return NULL;
}

HoughLineWindow::~HoughLineWindow()
{
}
