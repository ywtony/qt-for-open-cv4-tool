#pragma once

#include <QWidget>
#include "BasePixelOption.h"
#include "../../common/base/BaseSceneView.h"
#include "../../common/button/Button.h"
#include "../../common/edittext/EditText.h"
#include "../../common/seekbar/Seekbar.h"
#include <QFileDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QVBoxLayout>
#include <iostream>
#include <QSize>

class BlurWindow : public BaseSceneView
{
	Q_OBJECT

public:
	BlurWindow(QWidget *parent = nullptr);
	~BlurWindow();

protected:
	QPixmap handle();

private:
	QString filePath;
	BasePixelOption* option;
	QLabel* imageViewSrc;
	QLabel* imageViewResult;
	int mKsize = 3;//必须是奇数，3,5,7,9,11,13,15
};
