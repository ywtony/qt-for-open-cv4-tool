#pragma once

#include <QWidget>
#include "BasePixelOption.h"
#include "../../common/base/BaseSceneView.h"
#include "../../common/button/Button.h"
#include "../../common/edittext/EditText.h"
#include "../../common/seekbar/Seekbar.h"
#include "../../common/minus_plus/MinusPlusWidget.h"
#include "../../common/seekbar/SeekBarTipsWidget.h"
#include "../../common/choice_image/ChoiceImageWidget.h"
#include <QFileDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QVBoxLayout>
#include <iostream>
//提取图像中的英文字母，其实这是一个降噪的过程
class ExtractEnglishLettersWindow : public BaseSceneView
{
	Q_OBJECT

public:
	ExtractEnglishLettersWindow(QWidget *parent = nullptr);
	~ExtractEnglishLettersWindow();
	

protected:
	QPixmap handle();

private:
	QString filePath;
	BasePixelOption* option;
	QLabel *imageViews[6];
	int kSize=5;
	int thresholdMin=160;
	int thresholdMax=255;
	int dilateKSize=3;
	int erodeKsize=5;
	int imgWidth = 300;
	int imgHeight = 200;

};
