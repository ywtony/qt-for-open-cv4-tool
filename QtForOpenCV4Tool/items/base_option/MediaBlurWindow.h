#pragma once

#include <QWidget>
#include "BasePixelOption.h"
#include "../../common/base/BaseSceneView.h"
#include "../../common/button/Button.h"
#include "../../common/edittext/EditText.h"
#include "../../common/seekbar/Seekbar.h"
#include <QFileDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QVBoxLayout>
#include <iostream>

//中值滤波抑制椒盐噪声（小点点），效果是贼拉明显。（运行起来随便搜索一个椒盐噪声图片做测试）
class MediaBlurWindow : public BaseSceneView
{
	Q_OBJECT

public:
	MediaBlurWindow(QWidget *parent = nullptr);
	~MediaBlurWindow();

protected:
	QPixmap handle();

private:
	QString filePath;
	BasePixelOption* option;
	QLabel* imageViewSrc;
	QLabel* imageViewResult;
	int mKsize = 3;//高斯核，3,5,7,9,11,13,15
};
