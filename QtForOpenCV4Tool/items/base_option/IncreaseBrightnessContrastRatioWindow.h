#pragma once

#include <QWidget>
#include "BasePixelOption.h"
#include "../../common/base/BaseSceneView.h"
#include "../../common/button/Button.h"
#include "../../common/edittext/EditText.h"
#include "../../common/seekbar/Seekbar.h"
#include <QFileDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QVBoxLayout>
#include <iostream>
#include <QSize>

class IncreaseBrightnessContrastRatioWindow : public BaseSceneView
{
	Q_OBJECT

public:
	IncreaseBrightnessContrastRatioWindow(QWidget *parent = nullptr);
	~IncreaseBrightnessContrastRatioWindow();

protected:
	QPixmap handle();

private:
	QString filePath;
	BasePixelOption* option;
	QLabel* imageViewStep1;
	QLabel* imageViewStep2;
	double mAlpha = 1.2;//对比度调节1.0~2.0
	double mBeta = 50;//亮度调节范围0~255
};
