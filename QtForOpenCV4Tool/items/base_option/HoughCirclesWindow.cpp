#include "HoughCirclesWindow.h"

HoughCirclesWindow::HoughCirclesWindow(QWidget* parent)
	: BaseSceneView(parent)
{
	this->setWindowTitle("霍夫圆检测");
	this->setFixedSize(1000, 480);
	this->setGraphicsViewSize(1000, 480);

	option = new BasePixelOption(this);

	//选择图片
	ChoiceImageWidget* choiceImageWidget = new ChoiceImageWidget(this);
	choiceImageWidget->setFixedWidth(200);
	choiceImageWidget->setCallback([=](QString filePath) {
		this->filePath = filePath;
		qDebug() << "选择图片完成开始打印路径：" << filePath;
		this->execute();
		});


	SeekBarTipsWidget* seekbarTipParam1 = new SeekBarTipsWidget(this);
	seekbarTipParam1->initContent(0, 100, 1, "两个圆心最小距离", [=](int value) {
		if (filePath != NULL) {
			this->param1 = value;
			this->execute();
		}
		});
	seekbarTipParam1->setSeekBarValue(this->param1);

	SeekBarTipsWidget* seekbarTipParam2 = new SeekBarTipsWidget(this);
	seekbarTipParam2->initContent(0, 100, 1, "累加器的值", [=](int value) {
		if (filePath != NULL) {
			this->param2 = value;
			this->execute();
		}
		});
	seekbarTipParam2->setSeekBarValue(this->param2);

	SeekBarTipsWidget* seekbarTipMinRadius = new SeekBarTipsWidget(this);
	seekbarTipMinRadius->initContent(0, 100, 1, "最小半径", [=](int value) {
		if (filePath != NULL) {
			this->minRadius = value;
			this->execute();
		}
		});
	seekbarTipMinRadius->setSeekBarValue(this->minRadius);


	SeekBarTipsWidget* seekbarTipMaxRadius = new SeekBarTipsWidget(this);
	seekbarTipMaxRadius->initContent(0, 100, 1, "最大半径", [=](int value) {
		if (filePath != NULL) {
			this->maxRadius = value;
			this->execute();
		}
		});
	seekbarTipMaxRadius->setSeekBarValue(this->maxRadius);



	//添加显示图片
	imageTips[0] = new ImageTip(this);
	imageTips[0]->setItemsTitle("原图");
	imageTips[1] = new ImageTip(this);
	imageTips[1]->setItemsTitle("灰度图");
	imageTips[2] = new ImageTip(this);
	imageTips[2]->setItemsTitle("中值滤波");
	imageTips[3] = new ImageTip(this);
	imageTips[3]->setItemsTitle("边缘检测结果");
	imageTips[4] = new ImageTip(this);
	imageTips[4]->setItemsTitle("最终结果");

	QHBoxLayout* hLayout = new QHBoxLayout(this);
	QVBoxLayout* vLayout = new QVBoxLayout(this);


	vLayout->addWidget(choiceImageWidget);
	vLayout->addWidget(seekbarTipParam1);
	vLayout->addWidget(seekbarTipParam2);
	vLayout->addWidget(seekbarTipMinRadius);
	vLayout->addWidget(seekbarTipMaxRadius);
	vLayout->setAlignment(Qt::AlignTop);

	QVBoxLayout* vLayout2 = new QVBoxLayout(this);
	QHBoxLayout* hTopLayout = new QHBoxLayout(this);
	QHBoxLayout* hTopLayout2 = new QHBoxLayout(this);
	hTopLayout->addWidget(imageTips[0]);
	hTopLayout->addWidget(imageTips[1]);
	hTopLayout->addWidget(imageTips[2]);
	hTopLayout2->addWidget(imageTips[3]);
	hTopLayout2->addWidget(imageTips[4]);
	//hTopLayout2->addWidget(imageTips[5]);
	vLayout2->addLayout(hTopLayout);
	vLayout2->addLayout(hTopLayout2);


	hLayout->addLayout(vLayout, 1);
	hLayout->addLayout(vLayout2, 3);

}

//耗时方法(高斯不同演示)
QPixmap HoughCirclesWindow::handle() {
	Mat mats[5];
	option->showHoughCircles(filePath.toStdString().c_str(), param1, param2, minRadius, maxRadius, mats);
	imageTips[0]->setItemsPixmap(ImageUtils::getPixmap(mats[0], 200, 200));
	imageTips[1]->setItemsPixmap(ImageUtils::getPixmap8(mats[1], 200, 200));
	imageTips[2]->setItemsPixmap(ImageUtils::getPixmap8(mats[2], 200, 200));
	imageTips[3]->setItemsPixmap(ImageUtils::getPixmap8(mats[3], 200, 200));
	imageTips[4]->setItemsPixmap(ImageUtils::getPixmap(mats[4], 200, 200));
	return NULL;
}

HoughCirclesWindow::~HoughCirclesWindow()
{
}
