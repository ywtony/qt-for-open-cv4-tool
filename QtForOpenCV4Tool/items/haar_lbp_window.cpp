#include "haar_lbp_window.h"

Haar_LBP_Window::Haar_LBP_Window(QWidget* parent)
	: QWidget(parent)
{

	this->setFixedSize(QSize(320, 480));
	createListView();
}

void Haar_LBP_Window::createListView() {

	listView = new CommonListView(this);
	connect(listView, &CommonListView::itemClicked, [=](QListWidgetItem* item) {
		CommonListViewItem* item2 = dynamic_cast<CommonListViewItem*>(item);
		qDebug() << "itemPos:" << item2->mPos;
		switch (item2->mPos) {
		case 0:
			imageFaceDistinguish.show();
			break;
		case 1:
			videoFaceDistinguish.show();
			break;
		case 2:
			catFaceDistinguish.show();
			break;
		case 3:
			break;
		}
		});
	new CommonListViewItem(QString::fromLocal8Bit("级联分类器识别图片中的人脸"), QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 0, listView);
	new CommonListViewItem(QString::fromLocal8Bit("级联分类器识别视频中的人脸"), QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 1, listView);
	new CommonListViewItem(QString::fromLocal8Bit("使用级联分类器检测猫脸"), QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 2, listView);


}
