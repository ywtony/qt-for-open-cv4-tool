#ifndef FEATURES_DETECTION_WINDOW_H
#define FEATURES_DETECTION_WINDOW_H
#include <QWidget>
#include <QPixmap>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QMimeData>
#include <QFileInfo>
#include <QMessageBox>
#include <QResizeEvent>
#include <QModelIndex>
#include <QIcon>
#include "../common/utils/ImageUtils.h"
#include "../common/CommonListView.h"
#include "../common/CommonListViewItem.h"
#include "feature_detection/CornerHarris.h"
#include "feature_detection/CornerHarrisWindow.h"
#include "feature_detection/goodfeaturestotrack.h"
#include "feature_detection/surf_window.h"
#include "feature_detection/HogFeatureTestingWindow.h"
#include "feature_detection/IntegralGraphicsWindow.h"
#include "feature_detection/ORBImageMatch.h"
#include "feature_detection/PlaneDetectionWindow.h"


class Features_Detection_Window : public QWidget
{
	Q_OBJECT
public:
	explicit Features_Detection_Window(QWidget* parent = nullptr);
	void createListView();//创建一个ListView

private:
	CommonListView* listView;
	CornerHarrisWindow cornerHarrisWindow;
	GoodFeaturesToTrack goodFeaturesToTrack;
	surf_window surfWindow;
	//hog特征检测，检测行人
	HogFeatureTestingWindow hogFeatureTestingWindow;
	//积分图
	IntegralGraphicsWindow integralGraphicsWindow;
	ORBImageMatch oRBImageMatchWindow;
	//平面检测
	PlaneDetectionWindow planeDetectionWindow;

signals:

};

#endif // FEATURES_DETECTION_WINDOW_H
