#pragma once

#include <QWidget>
#include <QPixmap>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QMimeData>
#include <QFileInfo>
#include <QMessageBox>
#include <QResizeEvent>
#include <QModelIndex>
#include <QIcon>
#include "../common/utils/ImageUtils.h"
#include "../common/CommonListView.h"
#include "../common/CommonListViewItem.h"
#include "base_option/BasePixelOption.h"
#include "base_option/ShowOriginalImageWindow.h"
#include "base_option/PixelReverseWindow.h"
#include "base_option/ImageFuseWindow.h"
#include "base_option/IncreaseBrightnessContrastRatioWindow.h"
#include "base_option/DrawShapeWindow.h"
#include "base_option/BlurWindow.h"
#include "base_option/GaussianBlurWindow.h"
#include "base_option/MediaBlurWindow.h"
#include "base_option/BilateralFilterWindow.h"
#include "base_option/ExtractEnglishLettersWindow.h"
#include "base_option/OpenImageOptionWindow.h"
#include "base_option/MorphologicalGradientWindow.h"
#include "base_option/ExtractLineWindow.h"
#include "base_option/PryUpAndDownWindow.h"
#include "base_option/GaussianDiffWindow.h"
#include "base_option/Filter2DCustomKernelWindow.h"
#include "base_option/Filter2DMergeXYCustomKernelWindow.h"
#include "base_option/MakeBorderCopyWindow.h"
#include "base_option/SobelAndScharrWindow.h"
#include "base_option/LaplacianWindow.h"
#include "base_option/CannyWindow.h"
#include "base_option/HoughLineWindow.h"
#include "base_option/HoughCirclesWindow.h"
#include "base_option/RemapWindow.h"
#include "base_option/EqualizeHistWindow.h"
#include "base_option/EqualizeHistColorImageWindow.h"
#include "base_option/DrawCalcHistWindow.h"
#include "base_option/CalcHistBackProjectWindow.h"
#include "base_option/MatchTemplateWindow.h"
#include "base_option/FindContoursWindow.h"
#include "base_option/ContoursRectAndCircleWindow.h"

class BaseOptionWindow : public QWidget
{
	Q_OBJECT

public:
	BaseOptionWindow(QWidget* parent = nullptr);
	~BaseOptionWindow();
	void createListView();//创建一个ListView

private:
	CommonListView* listView;
	BasePixelOption* option;
	//显示原图
	ShowOriginalImageWindow showOriginalImageWindow;
	//像素取反
	PixelReverseWindow pixelReverseWindow;
	//图像融合
	ImageFuseWindow imageFuseWindow;
	//调整图像亮度
	IncreaseBrightnessContrastRatioWindow increaseBrightnessContrastRatioWindow;
	//opencv绘制
	DrawShapeWindow drawShapeWindow;
	//均值铝箔
	BlurWindow blurWindow;
	//高斯滤波
	GaussianBlurWindow gaussianBlurWindow;
	//中值滤波
	MediaBlurWindow mediaBlurWindow;
	//双边滤波
	BilateralFilterWindow bilateralFilterWindow;
	//使用开闭操作，提取图像中的英文字母，把多余的噪声去掉
	ExtractEnglishLettersWindow extractEnglishLettersWindow;
	//开操作（开操作是由腐蚀和膨胀组合而成的，先腐蚀后膨胀就是开操作）
	OpenImageOptionWindow openImageOptionWindow;
	//形态学梯度
	MorphologicalGradientWindow morphologicalGradientWindow;
	//从图像中提取直线
	ExtractLineWindow extractLineWindow;
	//上采样和降采样
	PryUpAndDownWindow pryUpAndDownWindow;
	//高斯不同
	GaussianDiffWindow gaussianDiffWindow;
	//filter2d自定义卷积核
	Filter2DCustomKernelWindow filter2DCustomKernelWindow;
	//filter2d自定义rebort和sobol算子x，y单独显示，及xy合并
	Filter2DMergeXYCustomKernelWindow filter2DMergeXYCustomKernelWindow;
	//边缘填充
	MakeBorderCopyWindow makeBorderCopyWindow;
	//Sobel和Scharr梯度图像
	SobelAndScharrWindow sobelAndScharrWindow;
	//拉普拉斯梯度图像
	LaplacianWindow laplacianWindow;
	//边缘检测
	CannyWindow cannyWindow;
	//霍夫直线检测
	HoughLineWindow houghLineWindow;
	//霍夫圆检测
	HoughCirclesWindow houghCirclesWindow;
	//重复映射
	RemapWindow remapWindow;
	//直方图均衡化
	EqualizeHistWindow equalizeHistWindow;
	//直方图均衡话彩色图片
	EqualizeHistColorImageWindow equalizeHistColorImageWindow;
	//绘制直方图
	DrawCalcHistWindow drawCalcHistWindow;
	//直方图反射投影
	CalcHistBackProjectWindow calcHistBackProjectWindow;
	//模版匹配
	MatchTemplateWindow matchTemplateWindow;
	//轮廓发现
	FindContoursWindow findContoursWindow;
	//轮廓发现及绘制
	ContoursRectAndCircleWindow contoursRectAndCircleWindow;
};
