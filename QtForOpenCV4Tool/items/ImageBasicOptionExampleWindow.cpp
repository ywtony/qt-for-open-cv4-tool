#include "ImageBasicOptionExampleWindow.h"

ImageBasicOptionExampleWindow::ImageBasicOptionExampleWindow(QWidget *parent)
	: QWidget(parent)
{
	this->setWindowTitle("图像基础操作案例");
	this->resize(320, 480);
	createListView();

}
void ImageBasicOptionExampleWindow::createListView() {
	listView = new CommonListView(this);
	connect(listView, &CommonListView::itemClicked, [=](QListWidgetItem* item) {
		CommonListViewItem* item2 = dynamic_cast<CommonListViewItem*>(item);
		qDebug() << "itemPos:" << item2->mPos;
		switch (item2->mPos) {
		case 1://检测硬币个数
			coinCountExampleWindow.show();
			break;
		case 2://图像切边

			break;
		case 3://提取考试券中的直线

			break;
		case 4://从图片中提取对象

			break;
		case 5://图像矫正

			break;
		case 6://计算卫星云图的周长及面积

			break;
		
		}
		});
	new CommonListViewItem("检测硬币个数", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 1, listView);
	new CommonListViewItem("图像切边", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 2, listView);
	new CommonListViewItem("提取考试券中的直线", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 3, listView);
	new CommonListViewItem("从图片中提取对象", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 4, listView);
	new CommonListViewItem("图像矫正", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 5, listView);
	new CommonListViewItem("计算卫星云图的周长及面积", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 6, listView);
}

ImageBasicOptionExampleWindow::~ImageBasicOptionExampleWindow()
{
}
