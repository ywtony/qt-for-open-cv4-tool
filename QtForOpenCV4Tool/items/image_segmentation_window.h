#ifndef IMAGE_SEGMENTATION_WINDOW_H
#define IMAGE_SEGMENTATION_WINDOW_H

#include <QWidget>
#include <QPixmap>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QMimeData>
#include <QFileInfo>
#include <QMessageBox>
#include <QResizeEvent>
#include <QModelIndex>
#include <QIcon>
#include "../common/utils/ImageUtils.h"
#include "../common/CommonListView.h"
#include "../common/CommonListViewItem.h"
#include "image_segmentation/colors_classification.h"
#include "image_segmentation/gmm_image_division.h"
#include "image_segmentation/watershed_division.h"
#include "image_segmentation/watershed_image_division.h"
#include "image_segmentation/crabcut_matting.h"
#include "image_segmentation/id_photo_background_replacement.h"
#include "image_segmentation/vide_greencurtain_background_replacement.h"

class Image_Segmentation_Window : public QWidget
{
    Q_OBJECT
public:
    explicit Image_Segmentation_Window(QWidget *parent = nullptr);
    void createListView();//����һ��ListView

private:
    CommonListView * listView;
    Colors_ClassIfication colorsClassification;
    GMM_Image_Division gmmImageDivision;
    Watershed_Division watershedDivision;
    Watershed_Image_Division watershedImageDivision;
    CrabCut_Matting grabCutMatting;
    Id_Photo_Background_Replacement idPhotoBackgroundReplacement;
    Vide_GreenCurtain_Background_Replacement videoGreenCurtainBackgroundReplacement;

signals:

};

#endif // IMAGE_SEGMENTATION_WINDOW_H
