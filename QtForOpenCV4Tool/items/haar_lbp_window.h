#ifndef HAAR_LBP_WINDOW_H
#define HAAR_LBP_WINDOW_H

#include <QWidget>
#include <QPixmap>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QMimeData>
#include <QFileInfo>
#include <QMessageBox>
#include <QResizeEvent>
#include <QModelIndex>
#include <QIcon>
#include "../common/utils/ImageUtils.h"
#include "../common/CommonListView.h"
#include "../common/CommonListViewItem.h"
#include "harr_lbp/image_face_distinguish.h"
#include "harr_lbp/video_face_distinguish.h"
#include "harr_lbp/cat_face_distinguish.h"

class Haar_LBP_Window : public QWidget
{
    Q_OBJECT
public:
    explicit Haar_LBP_Window(QWidget *parent = nullptr);
    void createListView();//����һ��ListView

private:
    CommonListView * listView;
    Image_Face_Distinguish imageFaceDistinguish;
    Video_Face_Distinguish videoFaceDistinguish;
    Cat_Face_Distinguish catFaceDistinguish;

signals:

};

#endif // HAAR_LBP_WINDOW_H
