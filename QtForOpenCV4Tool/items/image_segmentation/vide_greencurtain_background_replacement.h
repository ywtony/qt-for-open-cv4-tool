#ifndef VIDE_GREENCURTAIN_BACKGROUND_REPLACEMENT_H
#define VIDE_GREENCURTAIN_BACKGROUND_REPLACEMENT_H

#include <QWidget>
#include "../../common/CommonGraphicsView.h"

/**
 * 视频绿幕背景替换
 * @brief The Vide_GreenCurtain_Background_Replacement class
 */
class Vide_GreenCurtain_Background_Replacement : public CommonGraphicsView
{
    Q_OBJECT
public:
    explicit Vide_GreenCurtain_Background_Replacement(QWidget *parent = nullptr);
    void showVideoGreenCurtainBackgroundReplacement(const char* filePath);
    void resizeImage(Mat &target);
    void showResult(Mat &frame,Mat mask);
protected:
    void dropEvent(QDropEvent *event) override;
private:
    Mat background1;
    Mat background2;

signals:

};

#endif // VIDE_GREENCURTAIN_BACKGROUND_REPLACEMENT_H
