#ifndef ID_PHOTO_BACKGROUND_REPLACEMENT_H
#define ID_PHOTO_BACKGROUND_REPLACEMENT_H

#include <QWidget>
#include "../../common/CommonGraphicsView.h"

/**
 * 使用kmeans实现证件照背景替换小案例
 * @brief The Id_Photo_Background_Replacement class
 */
class Id_Photo_Background_Replacement : public CommonGraphicsView
{
    Q_OBJECT
public:
    explicit Id_Photo_Background_Replacement(QWidget *parent = nullptr);
    void showIdPhotoBackgroundReplacement(const char* filePath);
protected:
    void dropEvent(QDropEvent *event) override;


signals:

};

#endif // ID_PHOTO_BACKGROUND_REPLACEMENT_H
