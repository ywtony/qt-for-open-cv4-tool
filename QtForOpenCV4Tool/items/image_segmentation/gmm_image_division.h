#ifndef GMM_IMAGE_DIVISION_H
#define GMM_IMAGE_DIVISION_H

#include <QWidget>
#include "../../common/CommonGraphicsView.h"
using namespace cv::ml;
/**
 * GMM图像分割
 * @brief The GMM_Image_Division class
 */

class GMM_Image_Division : public CommonGraphicsView
{
    Q_OBJECT
public:
    explicit GMM_Image_Division(QWidget *parent = nullptr);
    void showGMMImageDivision(const char *filePath);
protected:
    void dropEvent(QDropEvent *event) override;

signals:

};

#endif // GMM_IMAGE_DIVISION_H
