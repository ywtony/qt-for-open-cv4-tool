#ifndef WATERSHED_DIVISION_H
#define WATERSHED_DIVISION_H

#include <QWidget>
#include "../../common/CommonGraphicsView.h"

/**
 * 使用分水岭变换实现图像对象计数:
 * 分水岭变换最终要的一步是寻找marker-->通过findContours+drawContours把marker对象画出来，然后再交给watershed生成最终的
 * @brief The Watershed_Division class
 */
class Watershed_Division : public CommonGraphicsView
{
    Q_OBJECT
public:
    explicit Watershed_Division(QWidget *parent = nullptr);
    void showImageObjectCounts(const char * filePath);
protected:
    void dropEvent(QDropEvent *event) override;

signals:

};

#endif // WATERSHED_DIVISION_H
