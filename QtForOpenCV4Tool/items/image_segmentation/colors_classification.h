#ifndef COLORS_CLASSIFICATION_H
#define COLORS_CLASSIFICATION_H

#include <QWidget>
#include "../../common/CommonGraphicsView.h"

/**
 * 颜色分类
 * @brief The Colors_ClassIfication class
 */
class Colors_ClassIfication : public CommonGraphicsView
{
    Q_OBJECT
public:
    explicit Colors_ClassIfication(QWidget *parent = nullptr);
    void showColorsClassification(const char *filePath);

protected:
    void dropEvent(QDropEvent *event) override;//鼠标拖拽事件
signals:

};

#endif // COLORS_CLASSIFICATION_H
