#ifndef WATERSHED_IMAGE_DIVISION_H
#define WATERSHED_IMAGE_DIVISION_H

#include <QWidget>
#include "../../common/CommonGraphicsView.h"

/**
 * 使用分水岭算法实现图像分割
 * @brief The Watershed_Image_Division class
 */
class Watershed_Image_Division : public CommonGraphicsView
{
    Q_OBJECT
public:
    explicit Watershed_Image_Division(QWidget *parent = nullptr);
    void showImageDivision(const char * filePath);
protected:
    void dropEvent(QDropEvent* event) override;

signals:

};

#endif // WATERSHED_IMAGE_DIVISION_H
