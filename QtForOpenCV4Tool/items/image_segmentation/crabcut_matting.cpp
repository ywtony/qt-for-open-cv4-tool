#include "crabcut_matting.h"

CrabCut_Matting::CrabCut_Matting(QWidget *parent)
    : CommonGraphicsView{parent}
{
    this->setWindowTitle("crabCut抠图");
    this->setMouseTracking(true);//设置鼠标事件可用
    init = false;
    numRun = false;
}


void CrabCut_Matting::dropEvent(QDropEvent*event){
    QString filePath = event->mimeData()->urls().at(0).toLocalFile();
    showCrabCutMatting(filePath.toStdString().c_str());
}

void CrabCut_Matting::showCrabCutMatting(const char* filePath){
    src = imread(filePath);
    if(src.empty()){
        qDebug()<<"输入图像为空";
        return;
    }

    //创建一个背景遮罩
    mMask = Mat::zeros(src.size(),CV_8UC1);
    mMask.setTo(Scalar::all(GC_BGD));


    convert2Sence(src);
}
void CrabCut_Matting::mouseMoveEvent(QMouseEvent *event){
    //    if(event->button()==Qt::LeftButton){//鼠标左键
    rect = Rect(Point(rect.x, rect.y), Point(event->pos().x(), event->pos().y()));
    qDebug()<<"mouseMoveEvent:"<<rect.width<<"|"<<rect.height;
    showImage();
    //    }
}

void CrabCut_Matting::mousePressEvent(QMouseEvent *event){
    grabMouse();
    if(event->button()==Qt::LeftButton){//鼠标左键
        rect.x = event->pos().x();
        rect.y = event->pos().y();
        rect.width = 1;
        rect.height = 1;
        init = false;
        numRun = 0;
        qDebug()<<"mousePressEvent:"<<event->pos().x()<<"|"<<event->pos().y();
    }

}

void CrabCut_Matting::mouseReleaseEvent(QMouseEvent *event){
    releaseMouse();
    if(event->button()==Qt::LeftButton){//鼠标左键
        if (rect.width > 1 && rect.height > 1) {
            setROIMask();
            qDebug()<<"mouseReleaseEvent:"<<rect.width<<"|"<<rect.height;
            //执行grabcut的代码
            runGrabCut();
            numRun++;
            showImage();
        }

    }
}
/**
 * 将选中的区域设置为前景
 * @brief CrabCut_Matting::setROIMask
 */
void CrabCut_Matting::setROIMask(){
    // GC_FGD = 1
    // GC_BGD =0;
    // GC_PR_FGD = 3
    // GC_PR_BGD = 2
    mMask.setTo(GC_BGD);
    rect.x = max(0, rect.x);
    rect.y = max(0, rect.y);
    rect.width = min(rect.width, src.cols - rect.x);
    rect.height = min(rect.height, src.rows - rect.y);
    mMask(rect).setTo(Scalar(GC_PR_FGD));//将选中的区域设置为
}

void CrabCut_Matting::showImage(){
    Mat result, binMask;
    binMask.create(mMask.size(), CV_8UC1);
    binMask = mMask & 1;
    if (init) {
        src.copyTo(result, binMask);
    } else {
        src.copyTo(result);
    }
    rectangle(result, rect, Scalar(0, 0, 255), 2, 8);
    convert2Sence(result);

}


void CrabCut_Matting::runGrabCut(){
    if (rect.width < 2 || rect.height < 2) {
        return;
    }

    if (init) {
        grabCut(src, mMask, rect, bgModel, fgModel, 1);
    } {
        grabCut(src, mMask, rect, bgModel, fgModel, 1, GC_INIT_WITH_RECT);
        init = true;
    }
}


void CrabCut_Matting::convert2Sence(Mat target){
    scene.clear();
    QImage image = ImageUtils::matToQImage(target);
    QPixmap pixmap = QPixmap::fromImage(image);
    QGraphicsPixmapItem *item = new QGraphicsPixmapItem(pixmap.scaled(this->size(),Qt::KeepAspectRatio,Qt::SmoothTransformation));
    scene.addItem(item);
}
