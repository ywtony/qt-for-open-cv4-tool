#ifndef CRABCUT_MATTING_H
#define CRABCUT_MATTING_H

#include <QWidget>
#include <QEnterEvent>>
#include "../../common/CommonGraphicsView.h"
/**
 * 使用crabcut实现，由用户输入的抠图
 * @brief The CrabCut_Matting class
 */

class CrabCut_Matting : public CommonGraphicsView
{
    Q_OBJECT
public:
    explicit CrabCut_Matting(QWidget *parent = nullptr);
    void showCrabCutMatting(const char* filePath);
    void showImage();
    void setROIMask();
    void runGrabCut();
    void convert2Sence(Mat image);
protected:
    /**
     * 拖拽图片到qt事件
     * @brief dropEvent
     * @param event
     */
    void dropEvent(QDropEvent *event) override;
    /**
     * 鼠标移动事件
     * @brief mouseMoveEvent
     * @param event
     */
    void mouseMoveEvent(QMouseEvent *event) override;
    /**
     * 鼠标按下事件
     * @brief mousePressEvent
     * @param ev
     */
    void mousePressEvent(QMouseEvent *event)override;
    /**
     * 鼠标释放事件
     * @brief mouseReleaseEvent
     * @param ev
     */
    void mouseReleaseEvent(QMouseEvent *event)override;
//    void enterEvent(QEnterEvent *event) override;

private:
    int numRun;
    Rect rect;
    bool init;
    Mat mMask, bgModel, fgModel;
    Mat src;
    String winTitle;


signals:

};

#endif // CRABCUT_MATTING_H
