#include "vide_greencurtain_background_replacement.h"

Vide_GreenCurtain_Background_Replacement::Vide_GreenCurtain_Background_Replacement(QWidget *parent)
    : CommonGraphicsView{parent}
{
    this->setWindowTitle("视频绿幕背景替换");
}

void Vide_GreenCurtain_Background_Replacement::dropEvent(QDropEvent *event){
    const char *filePath= "/Users/yangwei/Documents/tony/opencv/课程配套代码与图片/代码与图片/01.mp4";
    showVideoGreenCurtainBackgroundReplacement(filePath);
}

void Vide_GreenCurtain_Background_Replacement::showVideoGreenCurtainBackgroundReplacement(const char* filePath){
    background1 = imread("/Users/yangwei/Downloads/5bd38a8bd51c7f866b7a5b397b8c1807.jpeg");//海底世界
    background2 = imread("/Users/yangwei/Downloads/3e6d749dfbec37b624c387767a04f34e.jpeg");//m78星云
    VideoCapture videoCapture;
    videoCapture.open(filePath);
    if(!videoCapture.isOpened()){//视频是否打开了
        qDebug()<<"视频打开失败";
        return;
    }
    Mat frame,hsv;
    Mat mask;
    while(videoCapture.read(frame)){
        cvtColor(frame,hsv,COLOR_BGR2HSV);//将图像转为hsv色彩空间
        inRange(hsv,Scalar(35, 43, 46), Scalar(155, 255, 255),mask);//使用inRange过滤像素并生成遮罩
        //使用形态学闭操作去除图像上的干扰白点
        Mat kernel = getStructuringElement(MORPH_RECT,Size(3,3),Point(-1,-1));
        morphologyEx(mask,mask,MORPH_CLOSE,kernel,Point(-1,-1));
        //使用形态学腐蚀操作对mask边缘进行腐蚀（去掉边缘白色）
        erode(mask,mask,kernel);
        //使用高斯模糊平滑前景与背景区域的过度（此处指的是黑白过度处）
        GaussianBlur(mask,mask,Size(3,3),0,0);
        resizeImage(frame);
        showResult(frame,mask);
        waitKey(1);
    }
}

/**
 * 将图像调整到指定的大小
 * @brief Vide_GreenCurtain_Background_Replacement::resizeImage
 * @param target
 */
void Vide_GreenCurtain_Background_Replacement::resizeImage(Mat &frame){
    qDebug()<<"width:"<<frame.cols<<"---->height:"<<frame.rows;

    cv::resize(background1,background1,frame.size());
    qDebug()<<"width:"<<background1.cols<<"---->height:"<<background1.rows;
}

/**
 * 填充像素输出指定的图像
 * @brief Vide_GreenCurtain_Background_Replacement::showResult
 * @param result
 */
void Vide_GreenCurtain_Background_Replacement::showResult(Mat &frame,Mat mask){
    Mat result = Mat::zeros(frame.size(),CV_8UC3);
    int width = frame.cols;
    int height = frame.rows;
    int dims = frame.channels();
    int m = 0;
    double wt = 0;

    int r = 0, g = 0, b = 0;
    int r1 = 0, g1 = 0, b1 = 0;
    int r2 = 0, g2 = 0, b2 = 0;
    for(int row=0;row<height;row++){
        uchar *currentImage = frame.ptr<uchar>(row);//原始帧图像的一列像素
        uchar *bgImage = background1.ptr<uchar>(row);//背景图像的一列像素
        uchar *maskImage = mask.ptr<uchar>(row);//遮罩的一列像素
        uchar *resultImage = result.ptr<uchar>(row);//最终输出结果的一列像素
        for(int col=0;col<width;col++){
            m = *maskImage++;//取出像素
            if(m==255){//背景
                *resultImage++ = *bgImage++;
                *resultImage++ = *bgImage++;
                *resultImage++ = *bgImage++;
                currentImage+=3;
            }else if(m==0){//前景
                *resultImage++ = *currentImage++;
                *resultImage++ = *currentImage++;
                *resultImage++ = *currentImage++;
                bgImage+=3;
            }else{//过度部分像素
                b1 = *resultImage++;
                g1 = *resultImage++;
                r1 = *resultImage++;

                b2 = *currentImage++;
                g2 = *currentImage++;
                r2 = *currentImage++;

                // 权重
                wt = m / 255.0;

                // 缓和权重
                b = b1*wt + b2*(1.0 - wt);
                g = g1*wt + g2*(1.0 - wt);
                r = r1*wt + r2*(1.0 - wt);

                *resultImage++ = b;
                *resultImage++ = g;
                *resultImage++ = r;
            }

        }
    }
    imshow("result",result);
}
