#include "GLTriangleWindow.h"

GLTriangleWindow::GLTriangleWindow(QWidget* parent)
	: QOpenGLWidget(parent)
{
	this->resize(QSize(480, 480));
	this->setWindowTitle("三角形");
	this->setWindowIcon(QIcon("images/opencv.png"));
	QHBoxLayout* hLayout = new QHBoxLayout(this);
	QPushButton* btnRed = new QPushButton(this);
	btnRed->setText("红色");
	QPushButton* btnGreen = new QPushButton(this);
	btnGreen->setText("绿色");
	QPushButton* btnBlue = new QPushButton(this);
	btnBlue->setText("蓝色");
	hLayout->addWidget(btnRed);
	hLayout->addWidget(btnGreen);
	hLayout->addWidget(btnBlue);
	hLayout->setAlignment(Qt::AlignTop);

	connect(btnRed, &QPushButton::clicked, [=]() {
		changeTriangleColor(RED);
		});

	connect(btnGreen, &QPushButton::clicked, [=]() {
		changeTriangleColor(GREEN);
		});

	connect(btnBlue, &QPushButton::clicked, [=]() {
		changeTriangleColor(BLUE);
		});

}
void GLTriangleWindow::initializeGL() {
	initializeOpenGLFunctions();

	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);//设置清屏颜色为红色
	glClear(GL_COLOR_BUFFER_BIT);

	//第一个
	shaderProgram.create();
	shaderProgram.addShaderFromSourceFile(QOpenGLShader::Vertex, ":/QtForOpenCV4Tool/shader/triangle.vert");
	shaderProgram.addShaderFromSourceFile(QOpenGLShader::Fragment, ":/QtForOpenCV4Tool/shader/triangle.frag");
	shaderProgram.link();

	//第二个
	shaderProgram2.create();
	shaderProgram2.addShaderFromSourceFile(QOpenGLShader::Vertex, ":/QtForOpenCV4Tool/shader/triangle.vert");
	shaderProgram2.addShaderFromSourceFile(QOpenGLShader::Fragment, ":/QtForOpenCV4Tool/shader/triangle.frag");
	shaderProgram2.link();

	//开启着色器属性pos属性
	shaderProgram.enableAttributeArray("pos");
	shaderProgram2.enableAttributeArray("pos");
}


void GLTriangleWindow::resizeGL(int w, int h) {
	glViewport(0, 0, w, h);
}
void GLTriangleWindow::paintGL() {
	shaderProgram.bind();
	//设置三角形的顶点
	GLfloat vertices[] = {
		0.0f, 0.5f, 0.0f,
		0.5f, -0.5f, 0.0f,
		-0.5f, -0.5f, 0.0f,
	};
	//设置顶点数据的来源
	shaderProgram.setAttributeArray("pos", vertices, 3);
	glDrawArrays(GL_TRIANGLES, 0, 3);

	//绘制第二个三角形
	shaderProgram2.bind();
	//设置三角形的顶点
	GLfloat vertices2[] = {
		0.5f, -0.5f, 0.0f,
		0.5f, -1.0f, 0.0f,
		1.0f, -0.5f, 0.0f,
	};
	//设置顶点数据的来源
	shaderProgram2.setAttributeArray("pos", vertices2, 3);
	glDrawArrays(GL_TRIANGLES, 0, 3);

}
void GLTriangleWindow::changeTriangleColor(TriColor mColor) {
	//makeCurrent();
	switch (mColor) {
	case RED:
		shaderProgram.bind();
		shaderProgram.setUniformValue("ourColor", 1.0f, 0.0f, 0.0f, 1.0f);
		shaderProgram2.bind();
		shaderProgram2.setUniformValue("ourColor", 1.0f, 0.0f, 0.0f, 1.0f);
		break;
	case GREEN:
		shaderProgram.bind();
		shaderProgram.setUniformValue("ourColor", 0.0f, 1.0f, 0.0f, 1.0f);
		shaderProgram2.bind();
		shaderProgram2.setUniformValue("ourColor", 0.0f, 1.0f, 0.0f, 1.0f);
		break;
	case BLUE:
		shaderProgram.bind();
		shaderProgram.setUniformValue("ourColor", 0.0f, 0.0f, 1.0f, 1.0f);
		shaderProgram2.bind();
		shaderProgram2.setUniformValue("ourColor", 0.0f, 0.0f, 1.0f, 1.0f);
		break;
	}
	glDrawArrays(GL_TRIANGLES, 0, 3);
	update();//opengl的渲染流程是一个巨大的状态机。这里调用update，会更新状态机
	//doneCurrent();
}
GLTriangleWindow::~GLTriangleWindow()
{
}
