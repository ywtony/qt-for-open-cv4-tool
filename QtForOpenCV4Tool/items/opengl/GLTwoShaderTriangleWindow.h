#pragma once

#include <QWidget>
#include <QIcon>
#include <QOpenGLFunctions>
#include <QOpenGLWidget>
#include <QOpenGLShaderProgram>
#include <QOpenGLFunctions_3_3_Core>
#include <QPushButton>
#include <QHBoxLayout>
#include <Qt>
#include <QColor>
#include <QTextStream>
#include <QString>
#include <QFile>
#include <QDebug>
#include <QRadioButton>
#include <QVBoxLayout>

class GLTwoShaderTriangleWindow : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core
{
	Q_OBJECT

public:
	GLTwoShaderTriangleWindow(QWidget *parent = nullptr);
	~GLTwoShaderTriangleWindow();
public:
	GLuint getShaderId(GLenum shaderType, QString resPath);
	void buildShader();
	void getLinkProgramErrorInfo(GLuint programId);
	void getCompileShaderErrorInfo(GLuint shaderId);
protected:
	virtual void initializeGL();//初始化OpenGL
	virtual void resizeGL(int w, int h);//ResizeWindow大小
	virtual void paintGL();//绘制OpenGL
private:
	GLuint programId1,programId2;
	unsigned int VAOs[2], VBOs[2];//创建两个VAO管理两个VBO
};
