#pragma once

#include <QWidget>
#include <QIcon>
#include <QOpenGLFunctions>
#include <QOpenGLWidget>
#include <QOpenGLShaderProgram>
#include <QOpenGLFunctions_3_3_Core>
#include <QPushButton>
#include <QHBoxLayout>
#include <Qt>
#include <QColor>
#include <QTextStream>
#include <QString>
#include <QFile>
#include <QDebug>
#include <QRadioButton>
#include <QVBoxLayout>

class GLTwoTriangleWindow : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core
{
	Q_OBJECT

public:
	GLTwoTriangleWindow(QWidget* parent = nullptr);
	~GLTwoTriangleWindow();
protected:
	virtual void initializeGL();//初始化OpenGL
	virtual void resizeGL(int w, int h);//ResizeWindow大小
	virtual void paintGL();//绘制OpenGL
public:
	GLuint getShaderId(GLenum shaderType, QString resPath);
	void createAndLinkProgram(GLenum vertexShaderType, QString vertexResPath, GLenum fragmentShaderType, QString fragmentResPath);
private:
	unsigned int VAO, VBO;
//	GLfloat vertices[9] = {
//0.5f, -0.5f, 0.0f,
//0.5f, -1.0f, 0.0f,
//1.0f, -0.5f, 0.0f,
//	};
	float vertices[18] = {
		// first triangle
		-0.9f, -0.5f, 0.0f,  // left 
		-0.0f, -0.5f, 0.0f,  // right
		-0.45f, 0.5f, 0.0f,  // top 
		// second triangle
		 0.0f, -0.5f, 0.0f,  // left
		 0.9f, -0.5f, 0.0f,  // right
		 0.45f, 0.5f, 0.0f   // top 
	};
	GLuint programId;
	const char* vertexShaderSource = "#version 330 core\n"
		"layout (location = 0) in vec3 aPos;\n"
		"void main()\n"
		"{\n"
		"   gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);\n"
		"}\0";
	const char* fragmentShaderSource = "#version 330 core\n"
		"out vec4 FragColor;\n"
		"void main()\n"
		"{\n"
		"   FragColor = vec4(1.0f, 0.5f, 0.2f, 1.0f);\n"
		"}\n\0";
};
