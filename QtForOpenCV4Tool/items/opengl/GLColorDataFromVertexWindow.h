#pragma once

#include "BaseOpenGLWindow.h"

class GLColorDataFromVertexWindow : public BaseOpenGLWindow
{
	Q_OBJECT

public:
	GLColorDataFromVertexWindow(QWidget* parent = nullptr);
	~GLColorDataFromVertexWindow();
protected:
	virtual void initializeGL();//初始化OpenGL
	virtual void resizeGL(int w, int h);//ResizeWindow大小
	virtual void paintGL();//绘制OpenGL
public:
	void changeTriStatus(int type);
private:
	unsigned int VAO, VBO;
	GLuint programId;
	GLfloat op = 1.0f;
	float mY = 1.0f;//y轴颠倒
	float mX = 1.0f;//x轴颠倒
	float mXT = 0;//x轴向左移
	int type = 1;//
};
