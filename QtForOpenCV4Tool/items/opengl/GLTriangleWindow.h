#pragma once

#include <QWidget>
#include <QIcon>
#include <QOpenGLFunctions>
#include <QOpenGLWidget>
#include <QOpenGLShaderProgram>
#include <QPushButton>
#include <QHBoxLayout>
#include <Qt>
#include <QColor>

enum TriColor
{
	RED=0,
	GREEN,
	BLUE
};

class GLTriangleWindow : public QOpenGLWidget,protected QOpenGLFunctions
{
	Q_OBJECT

public:
	GLTriangleWindow(QWidget* parent = nullptr);
	~GLTriangleWindow();
	
protected:
	virtual void initializeGL();//初始化OpenGL
	virtual void resizeGL(int w, int h);//ResizeWindow大小
	virtual void paintGL();//绘制OpenGL
public:
	void changeTriangleColor(TriColor mColor);

private:
	QOpenGLShaderProgram shaderProgram;
	QOpenGLShaderProgram shaderProgram2;
	TriColor mColor = RED;
};
