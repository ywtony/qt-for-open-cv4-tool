#pragma once

#include "BaseOpenGLWindow.h"
#ifndef STB_IMAGE_H
#define STB_IMAGE_H
#include "../../stb_image.h"
#endif // STB_IMAGE_H
#include <qmatrix4x4.h>
#include <qvector3d.h>
#include <QRectF>

class GLTextureMixWindow : public BaseOpenGLWindow
{
	Q_OBJECT

public:
	GLTextureMixWindow(QWidget *parent = nullptr);
	~GLTextureMixWindow();

protected:
	virtual void initializeGL();//初始化OpenGL
	virtual void resizeGL(int w, int h);//ResizeWindow大小
	virtual void paintGL();//绘制OpenGL
public:
	GLuint buildTexture(QString textureFileName, GLint imageColorType);
	void setMixScale(float mix);
	void flipToX();
private:
	unsigned int VAO, VBO, EBO;
	unsigned int texture, texture1, texture2;
	GLuint programId;
	float mixScale = 0.5f;
	float flip_x = 1.0f;
};
