#pragma once

#include "BaseOpenGLWindow.h"
#ifndef STB_IMAGE_H
#define STB_IMAGE_H
#include "../../stb_image.h"
#endif // STB_IMAGE_H
#include <QQuaternion>
#include <Qt>
#include <QTimer>


class GLTextureMvpTestWindow : public BaseOpenGLWindow
{
	Q_OBJECT

public:
	GLTextureMvpTestWindow(QWidget* parent = nullptr);
	~GLTextureMvpTestWindow();

protected:
	virtual void initializeGL();//初始化OpenGL
	virtual void resizeGL(int w, int h);//ResizeWindow大小
	virtual void paintGL();//绘制OpenGL
public:
	GLuint buildTexture(QString textureFileName, GLint imageColorType);
	void setOrthoMProjection(float width, float height);
private:
	unsigned int VAO, VBO, EBO;
	unsigned int texture;
	GLuint programId;
	//以下三个默认是4x4单位矩阵
	QMatrix4x4 uModelMatrix;
	QMatrix4x4 uViewMatrix;
	QMatrix4x4 uProjectionMatrix;
	int mWidth, mHeight, mChannels;
};
