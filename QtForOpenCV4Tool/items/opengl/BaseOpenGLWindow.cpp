#include "BaseOpenGLWindow.h"

BaseOpenGLWindow::BaseOpenGLWindow(QWidget* parent)
	: QOpenGLWidget(parent)
{
	this->resize(QSize(480, 480));
	this->setWindowIcon(QIcon("images/opencv.png"));
}
GLuint BaseOpenGLWindow::buildAttachShaderAndReturnProgramId(QString vertexResPath, QString fragmentResPath) {
	//编译顶点着色器和片元着色器
	GLuint vertexShader = getShaderId(GL_VERTEX_SHADER, vertexResPath);
	GLuint fragmentShader = getShaderId(GL_FRAGMENT_SHADER, fragmentResPath);
	GLuint programId = glCreateProgram();
	glAttachShader(programId, vertexShader);
	glAttachShader(programId, fragmentShader);
	glLinkProgram(programId);
	getLinkProgramErrorInfo(programId);

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
	return programId;
}
void BaseOpenGLWindow::getLinkProgramErrorInfo(GLuint programId) {
	int success;
	char infoLog[512];
	glGetProgramiv(programId, GL_LINK_STATUS, &success);
	if (!success) {
		glGetProgramInfoLog(programId, 512, NULL, infoLog);
		qDebug() << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog;
	}
}
GLuint BaseOpenGLWindow::getShaderId(GLenum shaderType, QString resPath) {
	//创建顶点着色器
	unsigned int shaderId = glCreateShader(shaderType);
	QFile vertexShaderFile(resPath);
	if (!vertexShaderFile.open(QIODevice::ReadOnly)) {
		qDebug() << "Cannot open vertex shader file for reading";
	}
	QString verQStr = vertexShaderFile.readAll();
	std::string verStdStr = verQStr.toStdString();
	const char* vertexStr = verStdStr.c_str();
	qDebug() << "vertexStr-------------" << vertexStr;
	vertexShaderFile.flush();
	vertexShaderFile.close();
	glShaderSource(shaderId, 1, &vertexStr, NULL);
	glCompileShader(shaderId);
	getCompileShaderErrorInfo(shaderId);
	return shaderId;
}
void BaseOpenGLWindow::getCompileShaderErrorInfo(GLuint shaderId) {
	int success;
	char infoLog[512];
	glGetShaderiv(shaderId, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(shaderId, 512, NULL, infoLog);
		qDebug() << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog;
	}
}
void BaseOpenGLWindow::initializeGL() {
	//this->initializeOpenGLFunctions();
	//glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	//glClear(GL_COLOR_BUFFER_BIT);
}
void BaseOpenGLWindow::resizeGL(int w, int h) {
	//glViewport(0, 0, w, h);
}
QString BaseOpenGLWindow::getTexturePath(QString textureImageName) {
	return textureBasePath.append(textureImageName);
}
void BaseOpenGLWindow::paintGL() {

}
BaseOpenGLWindow::~BaseOpenGLWindow()
{
}
