#pragma once

#include <QWidget>
#include <QIcon>
#include <QOpenGLFunctions>
#include <QOpenGLWidget>
#include <QOpenGLShaderProgram>
#include <QOpenGLFunctions_3_3_Core>
#include <QPushButton>
#include <QHBoxLayout>
#include <Qt>
#include <QColor>
#include <QTextStream>
#include <QString>
#include <QFile>
#include <QDebug>
#include <QRadioButton>
#include <QVBoxLayout>

class BaseOpenGLWindow : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core
{
	Q_OBJECT

public:
	BaseOpenGLWindow(QWidget* parent = nullptr);
	~BaseOpenGLWindow();
protected:
	virtual void initializeGL();//初始化OpenGL
	virtual void resizeGL(int w, int h);//ResizeWindow大小
	virtual void paintGL();//绘制OpenGL
public:
	GLuint getShaderId(GLenum shaderType, QString resPath);
	GLuint buildAttachShaderAndReturnProgramId(QString vertexResPath, QString fragmentResPath);
	void getLinkProgramErrorInfo(GLuint programId);
	void getCompileShaderErrorInfo(GLuint shaderId);
	QString getTexturePath(QString textureImageName);
public:
	QString textureBasePath = "E:/tony/demo/visualstudio_workspace/QtForOpenCV4Tool/QtForOpenCV4Tool/bin/images/";
private:

};
