#pragma once

#include "BaseOpenGLWindow.h"
#ifndef STB_IMAGE_H
#define STB_IMAGE_H
#include "../../stb_image.h"
#endif // STB_IMAGE_H
#include <QQuaternion>
#include <Qt>
#include <QTimer>
#include <QLabel>
#include <QRadioButton>
#include "../../common/edittext/EditText.h"
#include "../../common/minus_plus/MinusPlusWidget.h"
#include "../../common/seekbar/SeekBarTipsWidget.h"


class GLTextureMVP3DWindow : public BaseOpenGLWindow
{
	Q_OBJECT

public:
	GLTextureMVP3DWindow(QWidget* parent = nullptr);
	~GLTextureMVP3DWindow();

protected:
	virtual void initializeGL();//初始化OpenGL
	virtual void resizeGL(int w, int h);//ResizeWindow大小
	virtual void paintGL();//绘制OpenGL
public:
	GLuint buildTexture(QString textureFileName, GLint imageColorType);
private:
	unsigned int VAO, VBO, EBO;
	unsigned int texture;
	GLuint programId;
	//以下三个默认是4x4单位矩阵
	QMatrix4x4 uModelMatrix;
	QMatrix4x4 uViewMatrix;
	QMatrix4x4 uProjectionMatrix;
	//旋转角度参数
	float mRotateAngle = -55.0f;
	//旋转轴
	float mRoateX = 0.5f, mRoateY = 1.0f, mRoateZ = 0.0f;

	//在X、Y、Z三个分量上做平移
	float mTranslateX = 0.0f, mTranslateY = 0.0f, mTranslateZ = -0.3f;
	int mTranslateType = 0;//0:x 1:y 2:z

	//透视投影参数
	float mPerspectiveAngle = 45.0f;

	QVector3D cubePositions[5] =
	{
		QVector3D(0.0f, 0.0f, -3.0f),
		QVector3D(2.0f,  5.0f, -1.0f),
		QVector3D(-1.5f, -2.2f, -2.5f),
		QVector3D(-3.8f, -2.0f, -1.3f),
		QVector3D(2.4f, -0.4f, -3.0f)
	};

};
