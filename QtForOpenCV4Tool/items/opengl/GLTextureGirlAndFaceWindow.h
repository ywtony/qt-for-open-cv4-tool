#pragma once

#include "BaseOpenGLWindow.h"
#ifndef STB_IMAGE_H
#define STB_IMAGE_H
#include "../../stb_image.h"
#endif // STB_IMAGE_H

class GLTextureGirlAndFaceWindow : public BaseOpenGLWindow
{
	Q_OBJECT

public:
	GLTextureGirlAndFaceWindow(QWidget *parent = nullptr);
	~GLTextureGirlAndFaceWindow();
protected:
	virtual void initializeGL();//初始化OpenGL
	virtual void resizeGL(int w, int h);//ResizeWindow大小
	virtual void paintGL();//绘制OpenGL
private:
	unsigned int VAO, VBO,EBO;
	unsigned int texture, texture1, texture2;
	GLuint programId;
};
