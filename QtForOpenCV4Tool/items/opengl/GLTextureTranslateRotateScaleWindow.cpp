#include "GLTextureTranslateRotateScaleWindow.h"

GLTextureTranslateRotateScaleWindow::GLTextureTranslateRotateScaleWindow(QWidget* parent)
	: BaseOpenGLWindow(parent)
{
	this->setWindowTitle("纹理的平移、缩放、旋转");
	QHBoxLayout* hLayout = new QHBoxLayout(this);
	QPushButton* btnRed = new QPushButton(this);
	btnRed->setText("平移");
	QPushButton* btnGreen = new QPushButton(this);
	btnGreen->setText("缩放");
	QPushButton* btnFlipX = new QPushButton(this);
	btnFlipX->setText("旋转");
	hLayout->addWidget(btnRed);
	hLayout->addWidget(btnGreen);
	hLayout->addWidget(btnFlipX);
	hLayout->setAlignment(Qt::AlignTop);

	QTimer* timer = new QTimer(this);
	connect(btnRed, &QPushButton::clicked, [this]() {
		if (mTranslateIndex > 3) {
			mTranslateIndex = 0;
		}
		setTextureTranslate();
		mTranslateIndex++;
		});

	connect(btnGreen, &QPushButton::clicked, [=]() {
		if (mScaleIndex > 3) {
			mScaleIndex = 0;
		}
		setTextureScale();
		mScaleIndex++;
		});

	connect(btnFlipX, &QPushButton::clicked, [=]() {
		timer->start(50);
		});

	connect(timer, &QTimer::timeout, [this] {
		if (mRoate > 360) {
			mRoate = 0;
		}
		setTextureRoate();
		mRoate += 10;
		});

}
void GLTextureTranslateRotateScaleWindow::initializeGL() {
	this->initializeOpenGLFunctions();
	//清屏颜色
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);


	float vertices[] = {
		//     ---- 位置 ----          - 纹理坐标 -
			 0.5f,  0.5f, 0.0f,   1.0f, 1.0f,   // 右上
			 0.5f, -0.5f, 0.0f,   1.0f, 0.0f,   // 右下
			-0.5f, -0.5f, 0.0f,   0.0f, 0.0f,   // 左下
			-0.5f,  0.5f, 0.0f,    0.0f, 1.0f    // 左上
	};
	unsigned int indices[] = {
	   0, 1, 3, // first triangle
	   1, 2, 3  // second triangle
	};
	//创建VAO
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);
	glBindVertexArray(VAO);

	//创建VBO
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	//创建EBO
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	//告知显卡如何解析顶点数据
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);

	//解绑VBO
	//glBindBuffer(GL_ARRAY_BUFFER, 0);

	stbi_set_flip_vertically_on_load(true);
	texture = buildTexture("E:/tony/demo/visualstudio_workspace/QtForOpenCV4Tool/QtForOpenCV4Tool/bin/images/similing_face.png",
		GL_RGBA);
	texture1 = buildTexture("E:/tony/demo/visualstudio_workspace/QtForOpenCV4Tool/QtForOpenCV4Tool/bin/images/similing_face.png",
		GL_RGBA);

	//小程序
	programId = buildAttachShaderAndReturnProgramId(":/QtForOpenCV4Tool/shader/texture_t_r_s.vert",
		":/QtForOpenCV4Tool/shader/texture_t_r_s.frag");

	uMatrix.setToIdentity();
	uMatrix.translate(QVector3D(0.0f, 0.0f, 0.0f));

	uMatrix2.setToIdentity();
	uMatrix2.translate(QVector3D(0.5f, 0.5f, 0.0f));

}

void GLTextureTranslateRotateScaleWindow::resizeGL(int w, int h) {
	glViewport(0, 0, w, h);
}
void GLTextureTranslateRotateScaleWindow::paintGL() {
	glBindVertexArray(VAO);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, texture);
	glUseProgram(programId);
	//绘制第一个笑脸
	GLint ourTextureLocation = glGetUniformLocation(programId, "ourTexture");
	glUniform1i(ourTextureLocation, 0);
	GLint uMatrixLocation = glGetUniformLocation(programId, "uMatrix");
	glUniformMatrix4fv(uMatrixLocation, 1, false, uMatrix.data());
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

	//绘制第二个笑脸
	GLint ourTextureLocation2 = glGetUniformLocation(programId, "ourTexture");
	glUniform1i(ourTextureLocation2, 1);
	GLint uMatrixLocation2 = glGetUniformLocation(programId, "uMatrix");
	glUniformMatrix4fv(uMatrixLocation2, 1, false, uMatrix2.data());
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}


GLuint GLTextureTranslateRotateScaleWindow::buildTexture(QString textureFileName,
	GLint imageColorType) {
	GLuint mTexture;
	////创建纹理
	glGenTextures(1, &mTexture);
	glBindTexture(GL_TEXTURE_2D, mTexture);
	//设置纹理环绕方式
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	//设置纹理过滤方式
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	//生成纹理
	int width, height, mChannels;
	//QString path = getTexturePath(textureFileName);
	//qDebug() << path.toStdString().c_str();
	unsigned char* data = stbi_load(textureFileName.toStdString().c_str(), &width, &height, &mChannels, 0);
	if (data) {
		qDebug() << "图片加载成功,width=" << width << ",height=" << height;
		glTexImage2D(GL_TEXTURE_2D, 0, imageColorType, width, height, 0, imageColorType, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else {
		qDebug() << "图像数据加载失败";
	}
	stbi_image_free(data);
	return mTexture;
}
void GLTextureTranslateRotateScaleWindow::setTextureTranslate() {
	GLint uMatrixLocation = glGetUniformLocation(programId, "uMatrix");
	uMatrix.setToIdentity();
	uMatrix.translate(QVector3D(mTranslates[mTranslateIndex][0], mTranslates[mTranslateIndex][1], 0.0f));
	glUniformMatrix4fv(uMatrixLocation, 1, false, uMatrix.data());
	update();
}
void GLTextureTranslateRotateScaleWindow::setTextureRoate() {
	GLint uMatrixLocation = glGetUniformLocation(programId, "uMatrix");
	uMatrix.setToIdentity();
	uMatrix.rotate(mRoate, QVector3D(0.0f, 0.0f, 1.0f));
	glUniformMatrix4fv(uMatrixLocation, 1, false, uMatrix.data());
	update();
}
void GLTextureTranslateRotateScaleWindow::setTextureScale() {
	GLint uMatrixLocation = glGetUniformLocation(programId, "uMatrix");
	uMatrix.setToIdentity();
	uMatrix.scale(mScales[mScaleIndex]);
	qDebug() << "缩放倍数：" << mScales[mScaleIndex];
	glUniformMatrix4fv(uMatrixLocation, 1, false, uMatrix.data());
	update();
}
GLTextureTranslateRotateScaleWindow::~GLTextureTranslateRotateScaleWindow()
{
}
