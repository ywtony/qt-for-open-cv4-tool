#pragma once

#include <QWidget>
#include <QIcon>
#include <QOpenGLFunctions>
#include <QOpenGLWidget>
#include <QOpenGLShaderProgram>
#include <QOpenGLFunctions_3_3_Core>
#include <QPushButton>
#include <QHBoxLayout>
#include <Qt>
#include <QColor>
#include <QTextStream>
#include <QString>
#include <QFile>
#include <QDebug>
#include <QRadioButton>
#include <QVBoxLayout>
#ifndef STB_IMAGE_H
#define STB_IMAGE_H
#include "../../stb_image.h"
#endif // STB_IMAGE_H
class GLTextureWindow : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core
{
	Q_OBJECT

public:
	GLTextureWindow(QWidget* parent = nullptr);
	~GLTextureWindow();
protected:
	virtual void initializeGL();//初始化OpenGL
	virtual void resizeGL(int w, int h);//ResizeWindow大小
	virtual void paintGL();//绘制OpenGL
private:
	float vertices[32] = {
		//     ---- 位置 ----       ---- 颜色 ----     - 纹理坐标 -
			 1.0f,  1.0f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f,   // 右上
			 1.0f, -1.0f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f,   // 右下
			-1.0f, -1.0f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f,   // 左下
			-1.0f,  1.0f, 0.0f,   1.0f, 1.0f, 0.0f,   0.0f, 1.0f    // 左上
	};
	float vertices2[32] = {
		//     ---- 位置 ----       ---- 颜色 ----     - 纹理坐标 -
			 0.5f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f,   // 右上
			 0.5f, -0.5f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f,   // 右下
			-0.5f, -0.5f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f,   // 左下
			-0.5f,  0.5f, 0.0f,   1.0f, 1.0f, 0.0f,   0.0f, 1.0f    // 左上
	};

	unsigned int indices[6] = {
		0, 1, 3, // 第一个三角形
		1, 2, 3  // 第二个三角形
	};
	//创建VAO：顶点数组对象，VBO：顶点缓冲，EBO：顶点索引缓冲
	unsigned int VAO, VBO, EBO;
	//创建纹理对象
	unsigned int texture, texture1;
	//创建 GPU programid
	GLuint programId;
};
