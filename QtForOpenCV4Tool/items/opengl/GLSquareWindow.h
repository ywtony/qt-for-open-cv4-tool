#pragma once

#include <QWidget>
#include <QIcon>
#include <QOpenGLFunctions>
#include <QOpenGLWidget>
#include <QOpenGLShaderProgram>
#include <QOpenGLFunctions_3_3_Core>
#include <QPushButton>
#include <QHBoxLayout>
#include <Qt>
#include <QColor>
#include <QTextStream>
#include <QString>
#include <QFile>
#include <QDebug>
#include <QRadioButton>
#include <QVBoxLayout>

//这篇介绍VAO、VBO、EBO的博客是相当牛逼的(它们三个都是用来操作顶点数据的)
//https://blog.csdn.net/u012861978/article/details/130953012

enum SquareColor
{
	RED2=0,
	GREEN2,
	BLUE2
};
class GLSquareWindow : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core
{
	Q_OBJECT

public:
	GLSquareWindow(QWidget *parent = nullptr);
	~GLSquareWindow();

protected:
	virtual void initializeGL();//初始化OpenGL
	virtual void resizeGL(int w, int h);//ResizeWindow大小
	virtual void paintGL();//绘制OpenGL
public:
	void drawSquare();
	void changeSquareColor(SquareColor mColor);
	void setPolygonMode(bool isFullMode);
private:
	SquareColor mColor = SquareColor::RED2;
	bool isFullMode = true;
	GLuint programShaderNative;//原生gpu programeid
	//创建VBO和VAO对象
	//VAO：管理VBO和EBO。VBO：顶点缓冲区。EBO：索引缓冲区，用于决定顶点绘制顺序
	unsigned int VAO, VBO, EBO;
	//创建索引（给EBO使用），绘制一个正方形
	unsigned int indices[6] =
	{
		0, 1, 3,//第一个三角形
		1, 2, 3 // 第二个三角形
	};
};
