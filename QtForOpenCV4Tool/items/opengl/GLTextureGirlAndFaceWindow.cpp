#include "GLTextureGirlAndFaceWindow.h"
GLTextureGirlAndFaceWindow::GLTextureGirlAndFaceWindow(QWidget* parent)
	: BaseOpenGLWindow(parent)
{
	this->setWindowTitle("纹理贴图->美女和笑脸");
}
void GLTextureGirlAndFaceWindow::initializeGL() {
	this->initializeOpenGLFunctions();
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	float vertices[] = {
		//     ---- 位置 ----            - 纹理坐标 -
			 1.0f,  1.0f, 0.0f,    1.0f, 1.0f,   // 右上
			 1.0f, -1.0f, 0.0f,   1.0f, 0.0f,   // 右下
			-1.0f, -1.0f, 0.0f,   0.0f, 0.0f,   // 左下
			-1.0f,  1.0f, 0.0f,   0.0f, 1.0f    // 左上
	};
	//float vertices[] = {
	//	//     ---- 位置 ----            - 纹理坐标 -
	//		 0.5f,  0.5f, 0.0f,    0.5f, 0.5f,   // 右上
	//		 0.5f, -0.5f, 0.0f,   0.5f, 0.0f,   // 右下
	//		-0.5f, -0.5f, 0.0f,   0.0f, 0.0f,   // 左下
	//		-0.5f,  0.5f, 0.0f,   0.0f, 0.5f    // 左上
	//};
	unsigned int indices[] = {
	   0, 1, 3, // first triangle
	   1, 2, 3  // second triangle
	};
	//创建VAO、VBO、EBO
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);
	glBindVertexArray(VAO);

	////顶点数据存入缓冲区
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	////EBO
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	////告知显卡顶点数据如何解析
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	////
	////告知显卡纹理坐标如何解析
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);



	////创建纹理
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	//设置纹理环绕方式
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	//设置纹理过滤方式
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	stbi_set_flip_vertically_on_load(true);
	//生成纹理
	int width, height, mChannels;
	QString path = getTexturePath("similing_face.png");
	unsigned char* data = stbi_load(path.toStdString().c_str(), &width, &height, &mChannels, 0);
	if (data) {
		qDebug() << "图片加载成功,width=" << width << ",height=" << height;
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else {
		qDebug() << "图像数据加载失败";
	}
	stbi_image_free(data);

	//小程序
	programId = buildAttachShaderAndReturnProgramId(":/QtForOpenCV4Tool/shader/girl_face.vert",
		":/QtForOpenCV4Tool/shader/girl_face.frag");
}
void GLTextureGirlAndFaceWindow::resizeGL(int w, int h) {
	glViewport(0, 0, w, h);
}
void GLTextureGirlAndFaceWindow::paintGL() {

	glBindVertexArray(VAO);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);
	glUseProgram(programId);
	//glUniform1i(glGetUniformLocation(programId, "ourTexture"), 0);

	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}
GLTextureGirlAndFaceWindow::~GLTextureGirlAndFaceWindow()
{
}
