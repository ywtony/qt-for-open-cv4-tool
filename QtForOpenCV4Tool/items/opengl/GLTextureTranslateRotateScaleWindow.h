#pragma once

#include "BaseOpenGLWindow.h"
#ifndef STB_IMAGE_H
#define STB_IMAGE_H
#include "../../stb_image.h"
#endif // STB_IMAGE_H
#include <QQuaternion>
#include <Qt>
#include <QTimer>
#include <QList>
#include <cmath>

class GLTextureTranslateRotateScaleWindow : public BaseOpenGLWindow
{
	Q_OBJECT

public:
	GLTextureTranslateRotateScaleWindow(QWidget* parent = nullptr);
	~GLTextureTranslateRotateScaleWindow();

protected:
	virtual void initializeGL();//初始化OpenGL
	virtual void resizeGL(int w, int h);//ResizeWindow大小
	virtual void paintGL();//绘制OpenGL
public:
	GLuint buildTexture(QString textureFileName, GLint imageColorType);
	void setTextureTranslate();
	void setTextureRoate();
	void setTextureScale();
private:
	unsigned int VAO, VBO, EBO;
	unsigned int texture,texture1;
	GLuint programId;
	QMatrix4x4 uMatrix;
	QMatrix4x4 uMatrix2;
	int mScaleIndex = 0;
	float mScales[4] = {
			0.5f,//放大
			1.0f,//缩小
			1.5f,//缩小
			2.0f//放大
	};

	int mTranslateIndex = 0;
	float mTranslates[4][4] = {
		{ -0.5f,0.5f},//左上角
		{ -0.5f,-0.5f},//左下角
		{0.5f,0.5f},//右上角
		{0.5f,-0.5f}//右下角
	};

	int mRoateIndex = 0;
	float mRoate = 0.0f;
	float mRoates[8] = {
		60.0f,
		120.0f,
		180.0f,
		240.0f,
		360.0f,
		0.0f
	};
};
