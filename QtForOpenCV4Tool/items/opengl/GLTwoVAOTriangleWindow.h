#pragma once

#include <QWidget>
#include <QIcon>
#include <QOpenGLFunctions>
#include <QOpenGLWidget>
#include <QOpenGLShaderProgram>
#include <QOpenGLFunctions_3_3_Core>
#include <QPushButton>
#include <QHBoxLayout>
#include <Qt>
#include <QColor>
#include <QTextStream>
#include <QString>
#include <QFile>
#include <QDebug>
#include <QRadioButton>
#include <QVBoxLayout>

class GLTwoVAOTriangleWindow : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core
{
	Q_OBJECT

public:
	GLTwoVAOTriangleWindow(QWidget *parent = nullptr);
	~GLTwoVAOTriangleWindow();
public:
	GLuint getShaderId(GLenum shaderType, QString resPath);
	void createAndLinkProgram(GLenum vertexShaderType, QString vertexResPath, GLenum fragmentShaderType, QString fragmentResPath);
protected:
	virtual void initializeGL();//初始化OpenGL
	virtual void resizeGL(int w, int h);//ResizeWindow大小
	virtual void paintGL();//绘制OpenGL
private:
	unsigned int VBOs[2], VAOs[2];
	GLuint programId;
};
