#pragma once

#include <QWidget>
#include <QPixmap>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QMimeData>
#include <QFileInfo>
#include <QMessageBox>
#include <QResizeEvent>
#include <QModelIndex>
#include <QIcon>
#include "../common/utils/ImageUtils.h"
#include "../common/CommonListView.h"
#include "../common/CommonListViewItem.h"
#include "base_option_example/CoinCountExampleWindow.h"

class ImageBasicOptionExampleWindow : public QWidget
{
	Q_OBJECT

public:
	ImageBasicOptionExampleWindow(QWidget *parent = nullptr);
	~ImageBasicOptionExampleWindow();

	void createListView();//创建一个ListView

private:
	CommonListView* listView;
	//检测硬币个数
	CoinCountExampleWindow coinCountExampleWindow;

};
