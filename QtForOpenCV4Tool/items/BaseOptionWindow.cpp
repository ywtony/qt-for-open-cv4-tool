#include "BaseOptionWindow.h"

BaseOptionWindow::BaseOptionWindow(QWidget* parent)
	: QWidget(parent)
{
	this->setWindowTitle("OpenCV基础操作");
	this->resize(320, 480);
	option = new BasePixelOption(this);
	createListView();

}
void BaseOptionWindow::createListView() {
	listView = new CommonListView(this);
	connect(listView, &CommonListView::itemClicked, [=](QListWidgetItem* item) {
		CommonListViewItem* item2 = dynamic_cast<CommonListViewItem*>(item);
		qDebug() << "itemPos:" << item2->mPos;
		switch (item2->mPos) {
		case 1://显示原图
			showOriginalImageWindow.show();
			break;
		case 2://像素取反
			pixelReverseWindow.show();
			break;
		case 3://图像融合
			imageFuseWindow.show();
			break;
		case 4://调整图像亮度及对比度
			increaseBrightnessContrastRatioWindow.show();
			break;
		case 5://绘制线、矩形、椭圆、圆、多边形、文本
			drawShapeWindow.show();
			break;
		case 6://均值模糊：用于图像的降噪(对椒盐噪声有比较好的一直效果)
			blurWindow.show();
			break;
		case 7://高斯模糊：用于图像的降噪，其对自然界的噪声有很好的抑制作用
			gaussianBlurWindow.show();
			break;
		case 8://中值滤波：终止滤波用于图像的降噪，其对椒盐噪声有很好的抑制作用（黑白点）
			mediaBlurWindow.show();
			break;
		case 9://双边滤波：其可以很好的保留边缘的同时对平坦区域进行降噪
			bilateralFilterWindow.show();
			break;
		case 10://提起图像中的英文字母
			extractEnglishLettersWindow.show();
			break;
		case 11://形态学开操作(先腐蚀后膨胀)
			openImageOptionWindow.show();
			break;
		case 12:// 形态学闭操作
			openImageOptionWindow.show();
			break;
		case 13://形态学梯度（基本梯度）：膨胀减去腐蚀
			morphologicalGradientWindow.show();
			break;
		case 14://顶帽操作：相当于原图像与开操作之间的差值图像
			morphologicalGradientWindow.show();
			break;
		case 15://黑帽操作：相当于原图像与闭操作之间的差值图像
			morphologicalGradientWindow.show();
			break;
		case 16://小案例：提取提取项目中的字母或者直线
			extractLineWindow.show();
			break;
		case 17://上采样:利用拉普拉斯金字塔进行图像重建
			pryUpAndDownWindow.show();
			break;
		case 18://降采样：利用高斯金字塔进行降采样
			pryUpAndDownWindow.show();
			break;
		case 19://高斯不同：把同一张图片再不同的参数下做高斯模糊之后的结果相减，得到的输出图像称为高斯不同
			gaussianDiffWindow.show();
			break;
		case 20://使用自定义卷积核filter2D
			filter2DCustomKernelWindow.show();
			break;
		case 21://1.合并rebort和sobel的x方向梯度和y方向的梯度
			filter2DMergeXYCustomKernelWindow.show();
			break;
		case 22://填充图像边缘
			makeBorderCopyWindow.show();
			break;
		case 23://使用Sobel和Scharr计算图形梯度
			sobelAndScharrWindow.show();
			break;
		case 24://使用拉普拉斯算子显示梯度图像
			laplacianWindow.show();
			break;
		case 25://边缘检测
			cannyWindow.show();
			break;
		case 26://霍夫直线检测
			houghLineWindow.show();
			break;
		case 27://霍夫圆检测
			houghCirclesWindow.show();
			break;
		case 28://像素映射(重复映射)
			remapWindow.show();
			break;
		case 29://直方图均衡化
			equalizeHistWindow.show();
			break;
		case 30://直方图均衡化彩色图片
			equalizeHistColorImageWindow.show();
			break;
		case 31://绘制直方图
			drawCalcHistWindow.show();
			break;
		case 32://直方图反向投影
			calcHistBackProjectWindow.show();
			break;
		case 33://模板匹配
			matchTemplateWindow.show();
			break;
		case 34://轮廓发现
			findContoursWindow.show();
			break;
		case 35://绘制最小的外接矩形及椭圆
			contoursRectAndCircleWindow.show();
			break;
		case 36://分水岭算法演示
			//option->showWaterShedImage(fileRealPath);
			break;
		}
		});
	new CommonListViewItem("显示原图", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 1, listView);
	new CommonListViewItem("像素取反", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 2, listView);
	new CommonListViewItem("图像融合", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 3, listView);
	new CommonListViewItem("调整图像亮度及对比度", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 4, listView);
	new CommonListViewItem("绘制线、矩形、椭圆、圆、多边形、文本", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 5, listView);
	new CommonListViewItem("均值模糊", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 6, listView);
	new CommonListViewItem("高斯模糊", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 7, listView);
	new CommonListViewItem("中值滤波", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 8, listView);
	new CommonListViewItem("双边滤波", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 9, listView);
	new CommonListViewItem("提取图像中的英文字母", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 10, listView);
	new CommonListViewItem("形态学开操作", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 11, listView);
	new CommonListViewItem("形态学闭操作", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 12, listView);
	new CommonListViewItem("形态学梯度（基本梯度）", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 13, listView);
	new CommonListViewItem("顶帽操作", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 14, listView);
	new CommonListViewItem("黑帽操作", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 15, listView);
	new CommonListViewItem("小案例：提取提取项目中的字母或者直线", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 16, listView);
	new CommonListViewItem("上采样", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 17, listView);
	new CommonListViewItem("降采样", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 18, listView);
	new CommonListViewItem("高斯不同", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 19, listView);
	new CommonListViewItem("使用自定义卷积核filter2D", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 20, listView);
	new CommonListViewItem("合并rebort及Sobel的x方向梯度和y方向的梯度", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 21, listView);
	new CommonListViewItem("填充图像边缘", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 22, listView);
	new CommonListViewItem("使用Sobel和Scharr计算图形梯度", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 23, listView);
	new CommonListViewItem("使用拉普拉斯算子显示梯度图像", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 24, listView);
	new CommonListViewItem("边缘检测", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 25, listView);
	new CommonListViewItem("霍夫直线检测", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 26, listView);
	new CommonListViewItem("霍夫圆检测", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 27, listView);
	new CommonListViewItem("像素映射", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 28, listView);
	new CommonListViewItem("直方图均衡化", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 29, listView);
	new CommonListViewItem("直方图均衡化彩色图片", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 30, listView);
	new CommonListViewItem("绘制直方图", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 31, listView);
	new CommonListViewItem("直方图反向投影", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 32, listView);
	new CommonListViewItem("模板匹配", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 33, listView);
	new CommonListViewItem("轮廓发现", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 34, listView);
	new CommonListViewItem("绘制轮廓的外接矩形及椭圆", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 35, listView);
	new CommonListViewItem("分水岭算法演示", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 36, listView);
}

BaseOptionWindow::~BaseOptionWindow()
{

}
