#ifndef AI_WINDOW_H
#define AI_WINDOW_H

#include <QWidget>
#include <QPixmap>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QMimeData>
#include <QFileInfo>
#include <QMessageBox>
#include <QResizeEvent>
#include <QModelIndex>
#include "../common/utils/ImageUtils.h"
#include "../common/CommonListView.h"
#include "../common/CommonListViewItem.h"
#include "ai/ai_google_net_image_division.h"
#include "ai/ai_image_distinguish.h"
////导入深度学习dnn模块
//#include <opencv2/dnn.hpp>
//using namespace cv::dnn;



/**
 * OpenCV中的人工智能算法：
 * 1.图像分类
 * 2.对象检测
 * 3.实时对象检测
 * 4.图像分割
 * 5.预测
 * 6.视频对象跟踪
 * @brief The Video_Analysis_And_Object_Track_Window class
 */
class AI_Window : public QWidget
{
    Q_OBJECT
public:
    explicit AI_Window(QWidget *parent = nullptr);
    void createListView();//创建一个ListView

private:
    CommonListView * listView;
    AI_Google_Net_Image_Division aiGoogleNetImageDivision;
    AI_Image_Distinguish aiImageDistinguish;


signals:

};

#endif // AI_WINDOW_H
