#include "ai_window.h"


AI_Window::AI_Window(QWidget* parent)
	: QWidget{ parent }
{
	this->setWindowTitle("OpenCV中的人工智能算法");
	this->setFixedSize(QSize(320, 480));
	createListView();
}

void AI_Window::createListView() {
	listView = new CommonListView(this);
	connect(listView, &CommonListView::itemClicked, [=](QListWidgetItem* item) {
		CommonListViewItem* item2 = dynamic_cast<CommonListViewItem*>(item);
		qDebug() << "itemPos:" << item2->mPos;
		switch (item2->mPos) {
		case 0:
			aiGoogleNetImageDivision.show();
			break;
		case 1:
			aiImageDistinguish.show();
			break;
		}
		});
	new CommonListViewItem("使用GoogleNet实现图像分割", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 0, listView);
	new CommonListViewItem("使用SSD实现图像识别", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 1, listView);
}
