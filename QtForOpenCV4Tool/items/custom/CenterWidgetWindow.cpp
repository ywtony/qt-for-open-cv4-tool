#include "CenterWidgetWindow.h"

CenterWidgetWindow::CenterWidgetWindow(QWidget* parent)
	: QMainWindow(parent)
{
	this->setWindowTitle("中心部件");
	this->setFixedSize(400, 400);

	QPushButton* btn = new QPushButton(this);
	btn->resize(60, 20);
	btn->setText("中心部件的按钮");

	// 设置中心部件  在这里我们来显示文本控件
	QTextEdit* textEdit = new QTextEdit("德玛西亚万岁", this);
	this->setCentralWidget(textEdit);
	//this->setCentralWidget(btn);
}

CenterWidgetWindow::~CenterWidgetWindow()
{

}
