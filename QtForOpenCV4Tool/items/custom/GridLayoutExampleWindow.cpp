#include "GridLayoutExampleWindow.h"

GridLayoutExampleWindow::GridLayoutExampleWindow(QWidget* parent)
	: QWidget(parent)
{
	this->setWindowTitle("网格布局");
	this->setFixedSize(800, 480);

	gridLayout = new QGridLayout(this);
	//gridLayout->setHorizontalSpacing(50);//设置水平间距
	//gridLayout->setVerticalSpacing(50);//设置竖直间距
	gridLayout->setSpacing(5);//设置两个方向的间距
	//gridLayout->setRowStretch(1, 2);//设置第一行的弹簧系数
	//gridLayout->setColumnStretch(1, 3);//设置指定列的弹簧系数
	gridLayout->setMargin(5);//设置周边填充
	int itemWidth = this->width() / 3;
	int itemHeight = this->height() / 3;
	cv::Mat src = imread("images/flowers.jpeg");
	//cv::Mat src = imread("images/face_shibie.png");
	QPixmap pixmap = ImageUtils::getPixmap(src);
	label = new QLabel(this);
	label->resize(itemWidth, itemHeight);
	label->setScaledContents(true);
	label->setPixmap(pixmap);
	label2 = new QLabel(this);
	label2->resize(itemWidth, itemHeight);
	label2->setScaledContents(true);
	label2->setPixmap(pixmap);
	label3 = new QLabel(this);
	label3->resize(itemWidth, itemHeight);
	label3->setPixmap(pixmap);
	label3->setScaledContents(true);
	label4 = new QLabel(this);
	label4->resize(itemWidth, itemHeight);
	label4->setScaledContents(true);
	label4->setPixmap(pixmap);
	label5 = new QLabel(this);
	label5->resize(itemWidth, itemHeight);
	label5->setScaledContents(true);
	label5->setPixmap(pixmap);
	label6 = new QLabel(this);
	label6->resize(itemWidth, itemHeight);
	label6->setScaledContents(true);
	label6->setPixmap(pixmap);
	label7 = new QLabel(this);
	label7->resize(itemWidth, itemHeight);
	label7->setScaledContents(true);
	label7->setPixmap(pixmap);
	label8 = new QLabel(this);
	label8->resize(itemWidth, itemHeight);
	label8->setScaledContents(true);
	label8->setPixmap(pixmap);
	label9 = new QLabel(this);
	label9->resize(itemWidth, itemHeight);
	label9->setScaledContents(true);
	label9->setPixmap(pixmap);

	QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding);
	label->setSizePolicy(sizePolicy);
	QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Expanding);
	label2->setSizePolicy(sizePolicy2);
	QSizePolicy sizePolicy3(QSizePolicy::Minimum, QSizePolicy::Expanding);
	label3->setSizePolicy(sizePolicy3);
	QSizePolicy sizePolicy4(QSizePolicy::Minimum, QSizePolicy::Expanding);
	label4->setSizePolicy(sizePolicy4);
	QSizePolicy sizePolicy5(QSizePolicy::Minimum, QSizePolicy::Expanding);
	label5->setSizePolicy(sizePolicy5);
	QSizePolicy sizePolicy6(QSizePolicy::Minimum, QSizePolicy::Expanding);
	label6->setSizePolicy(sizePolicy6);
	QSizePolicy sizePolicy7(QSizePolicy::Minimum, QSizePolicy::Expanding);
	label7->setSizePolicy(sizePolicy7);
	QSizePolicy sizePolicy8(QSizePolicy::Minimum, QSizePolicy::Expanding);
	label8->setSizePolicy(sizePolicy8);
	QSizePolicy sizePolicy9(QSizePolicy::Minimum, QSizePolicy::Expanding);
	label9->setSizePolicy(sizePolicy9);

	gridLayout->addWidget(label, 1, 1);
	gridLayout->addWidget(label2, 1, 2);
	gridLayout->addWidget(label3, 1, 3);
	gridLayout->addWidget(label4, 2, 1);
	gridLayout->addWidget(label5, 2, 2);
	gridLayout->addWidget(label6, 2, 3);
	gridLayout->addWidget(label7, 3, 1);
	gridLayout->addWidget(label8, 3, 2);
	gridLayout->addWidget(label9, 3, 3);

	/*gridLayout->setRowStretch(0, 1);
	gridLayout->setRowStretch(1, 1);
	gridLayout->setRowStretch(2, 1);
	gridLayout->setColumnStretch(0, 1);
	gridLayout->setColumnStretch(1, 1);
	gridLayout->setColumnStretch(2, 1);*/
}

void GridLayoutExampleWindow::resizeEvent(QResizeEvent* event) {

}

GridLayoutExampleWindow::~GridLayoutExampleWindow()
{
}
