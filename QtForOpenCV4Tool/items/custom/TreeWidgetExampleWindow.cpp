#include "TreeWidgetExampleWindow.h"

TreeWidgetExampleWindow::TreeWidgetExampleWindow(QWidget* parent)
	: QWidget(parent)
{
	this->setWindowTitle("TreeWidget组件");
	QVBoxLayout* vLayout = new QVBoxLayout;

	//创建TreeWidget
	QTreeWidget* treeWidget = new QTreeWidget;
	treeWidget->setColumnCount(3);// 设置列数为 3
	treeWidget->setHeaderLabels({ "LOL", "英雄","战力"}); // 设置列标签
	treeWidget->header()->setSectionResizeMode(QHeaderView::ResizeToContents); // 自适应列宽

	// 创建根节点
	QTreeWidgetItem* root = new QTreeWidgetItem(treeWidget);
	root->setText(0, "英雄联盟");
	root->setData(2, Qt::UserRole, "root_data_info");
	treeWidget->addTopLevelItem(root);

	// 创建子节点 1
	QTreeWidgetItem* child1 = new QTreeWidgetItem(root);//说明child1是root的子节点
	child1->setText(0, "德玛西亚");
	QTreeWidgetItem* child1_sub = new QTreeWidgetItem(child1);//说明child1_sub是child1的子节点
	child1_sub->setText(0, "德玛西亚之力");
	child1->addChild(child1_sub);//添加子节点
	//三级节点一级一级靠右边
	child1_sub->setText(0, "名称");
	child1_sub->setText(1, "德玛西亚");
	child1_sub->setText(2, "战力2");
	root->addChild(child1);

	// 创建子节点 2
	QTreeWidgetItem* child2 = new QTreeWidgetItem(root);//说明child2是root的子节点
	child2->setText(0, "诺克萨斯");
	QTreeWidgetItem* child2_sub = new QTreeWidgetItem(child2);//说明child2_sub是child2的子节点
	child2_sub->setText(0, "诺克萨斯之手");
	child2->addChild(child2_sub);
	child2_sub->setText(0, "101");
	root->addChild(child2);
	

	// 读取节点数据
	QTreeWidgetItem* item = treeWidget->topLevelItem(0);
	if (item) {
		qDebug() << "Root name: " << item->text(0);
		qDebug() << "Root value: " << item->text(1);
		qDebug() << "Root data: " << item->data(2, Qt::UserRole).toString();
	}

	vLayout->addWidget(treeWidget);
	this->setLayout(vLayout);


}

TreeWidgetExampleWindow::~TreeWidgetExampleWindow()
{

}
