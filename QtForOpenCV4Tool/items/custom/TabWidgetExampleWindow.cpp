#include "TabWidgetExampleWindow.h"

TabWidgetExampleWindow::TabWidgetExampleWindow(QWidget *parent)
	: QWidget(parent)
{
	this->setWindowTitle("TabLayout布局");

	QVBoxLayout* vLayout = new QVBoxLayout;
	QComboBox* cb = new QComboBox;//设置页面名字的位置 North, South, West, East
	cb->setFixedWidth(150);
	cb->setFixedHeight(30);
	cb->addItem("North");
	cb->addItem("South");
	cb->addItem("West");
	cb->addItem("East");
	vLayout->addWidget(cb);

	QTabWidget* tab = new QTabWidget;
	//给Tab添加内容
	Button* btn1 = new Button;
	btn1->resize(400, 400);
	btn1->setText("我的剑就是你的剑");
	QLabel* btn2 = new QLabel;
	btn2->setFixedSize(400, 400);
	btn2->setScaledContents(true);
	cv::Mat src = imread("images/flowers.jpeg");
	btn2->setPixmap(ImageUtils::getPixmap(src));
	Button* btn3 = new Button;
	btn3->setText("提莫队长正在待命");
	Button* btn4 = new Button;
	btn4->setText("恕瑞玛，你的皇帝回来了");
	tab->addTab(btn1, "Tab1");
	tab->addTab(btn2, "Tab2");
	tab->addTab(btn3, "Tab3");
	tab->addTab(btn4, "Tab4");
	//设置页面的名字
	tab->setTabText(0, "德玛西亚");
	tab->setTabText(1, "艾欧尼亚");
	tab->setTabText(2, "恕瑞玛");
	tab->setTabText(3, "诺克萨斯");

	//设置页面的提示信息
	tab->setTabToolTip(0, "德玛西亚之力");
	tab->setTabToolTip(1, "我忘记这是啥玩意了");
	tab->setTabToolTip(2, "恕瑞玛你的皇帝回来了");
	tab->setTabToolTip(3, "诺克萨斯永不退缩");

	//设置页面是否被激活.
	tab->setTabEnabled(0, true);
	tab->setTabEnabled(1, true);
	tab->setTabEnabled(2, true);
	tab->setTabEnabled(3, true);
	//设置页面名字的位置 North, South, West, East
	tab->setTabPosition(QTabWidget::North);

	//设置页面关闭按钮。
	tab->setTabsClosable(true);

	vLayout->addWidget(tab);
	this->setLayout(vLayout);

	//QComboBox事件
	connect(cb, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),this, [=](int index) {
		switch (index) {
		case 0:
			tab->setTabPosition(QTabWidget::North);
			break;
		case 1:
			tab->setTabPosition(QTabWidget::South);
			break;
		case 2:
			tab->setTabPosition(QTabWidget::West);
			break;
		case 3:
			tab->setTabPosition(QTabWidget::East);
			break;
		}
		
		});

}

TabWidgetExampleWindow::~TabWidgetExampleWindow()
{

}
