#include "StatusBarWindow.h"

StatusBarWindow::StatusBarWindow(QWidget *parent)
	: QMainWindow(parent)
{
	this->setWindowTitle("状态栏案例");
	this->setFixedSize(320, 480);

	QPushButton* btn = new QPushButton(this);
	btn->setText("按钮");
	QPushButton* btn2 = new QPushButton(this);
	btn2->setText("按钮2");
	QPushButton* btn3 = new QPushButton(this);
	btn3->setText("按钮3");

	QLabel* label = new QLabel(this);
	label->setText("状态");

	this->statusBar()->addWidget(btn,1);
	this->statusBar()->addWidget(btn2, 2);
	this->statusBar()->addWidget(btn3,3);
	this->statusBar()->addWidget(label, 4);
}

StatusBarWindow::~StatusBarWindow()
{

}
