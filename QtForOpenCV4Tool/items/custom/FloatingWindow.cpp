#include "FloatingWindow.h"

FloatingWindow::FloatingWindow(QWidget* parent)
	: QMainWindow(parent)
{
	this->setWindowTitle("铆接部件-->浮动窗口");
	this->setFixedSize(1000, 480);
	//创建一个铆接部件
	QDockWidget* dock = new QDockWidget("铆接部件->浮动窗口", this);
	QPushButton* btn = new QPushButton(dock);
	btn->move(20, 20);
	connect(btn, &QPushButton::clicked, [=]() {
		qDebug() << "浮动窗口的点击按钮";
		});
	btn->setText("浮动窗口的按钮");
	//添加铆接部件到窗口
	this->addDockWidget(Qt::LeftDockWidgetArea, dock);
	//设置铆接部件的范围
	dock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea |
		Qt::TopDockWidgetArea);
}

FloatingWindow::~FloatingWindow()
{

}
