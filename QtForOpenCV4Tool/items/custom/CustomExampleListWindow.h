#pragma once

#include <QWidget>
#include "../../common/CommonListView.h"
#include "../../common/CustomWidgetWindow.h"
#include "GridViewExampleWindow.h"
#include "FloatingWindow.h"
#include "MenuBarWindow.h"
#include "ToolBarWindow.h"
#include "StatusBarWindow.h"
#include "CenterWidgetWindow.h"
#include "DialogWindow.h"
#include "GridLayoutExampleWindow.h"
#include "FormLayoutExampleWindow.h"
#include "VHLayoutExampleWindow.h"
#include "StackedLayoutExampleWindow.h"
#include "TabWidgetExampleWindow.h"
#include "TreeWidgetExampleWindow.h"
#include "EventExampleWindow.h"

#include <QSize>
#include <QIcon>

class CustomExampleListWindow : public QWidget
{
	Q_OBJECT

public:
	CustomExampleListWindow(QWidget *parent = nullptr);
	~CustomExampleListWindow();

public:
    void createListView();//创建一个ListView

private:
    CommonListView* listView;
    //自定义View
    CustomWidgetWindow customWidgetWindow;
    //GridView测试
    GridViewExampleWindow gridViewExampleWindow;
    //浮动窗口
    FloatingWindow floatingWindow;
    //菜单
    MenuBarWindow menuBarWindow;
    //工具栏
    ToolBarWindow toolBarWindow;
    //状态栏
    StatusBarWindow statusBarWindow;
    //中心部件
    CenterWidgetWindow centerWidgetWindow;
    //Dialog
    DialogWindow dialogWindow;
    //GridLayout
    GridLayoutExampleWindow gridLayoutExampleWindow;
    //FormLayout表单
    FormLayoutExampleWindow formLayoutExampleWindow;
    //横向、竖直布局
    VHLayoutExampleWindow vHLayoutExampleWindow;
    //栈布局
    StackedLayoutExampleWindow stackedLayoutExampleWindow;
    //TabWidget
    TabWidgetExampleWindow tabWidgetExampleWindow;
    //TreeWidget
    TreeWidgetExampleWindow treeWidgetExampleWindow;
    //Qt事件
    EventExampleWindow eventExampleWindow;
};
