#include "MenuBarWindow.h"

MenuBarWindow::MenuBarWindow(QWidget* parent)
	: QWidget(parent)
{
	this->setWindowTitle("菜单测试");
	this->setFixedSize(320, 480);

	//创建菜单
	QMenu* menu = new QMenu("德玛西亚");
	QAction* action1 = new QAction("盖伦");
	QMenu* subMenu = new QMenu(menu);
	subMenu->addAction("人在塔在");
	subMenu->addAction("我的剑就是你的剑");
	subMenu->addAction("我从不退缩");
	action1->setMenu(subMenu);
	QAction* action2 = new QAction("嘉文四十");
	menu->addAction(action1);
	menu->addAction(action2);

	QMenu* menu2 = new QMenu("艾欧尼亚");
	QAction* action3 = new QAction("九尾妖狐阿狸");
	QAction* action4 = new QAction("盲僧李青");
	QAction* action5 = new QAction("无极剑圣易");
	menu2->addAction(action3);
	menu2->addAction(action4);
	menu2->addAction(action4);


	QMenuBar* menuBar = new QMenuBar(this);
	menuBar->addMenu(menu);
	menuBar->addMenu(menu2);

	connect(action2, &QAction::triggered, [=](bool flag) {
		qDebug() << "点击了菜单";
		});

}

MenuBarWindow::~MenuBarWindow()
{
}
