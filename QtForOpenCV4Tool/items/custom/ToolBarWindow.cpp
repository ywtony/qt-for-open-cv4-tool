#include "ToolBarWindow.h"

ToolBarWindow::ToolBarWindow(QWidget *parent)
	: QMainWindow(parent)
{
	this->setWindowTitle("工具栏测试");
	this->setFixedSize(320, 480);
	//创建工具栏
	QToolBar* toolBar = new QToolBar(this);
	//设置工具栏不可移动（默认是可移动的）
	toolBar->setMovable(false);
	//将工具栏添加到window
	this->addToolBar(toolBar);//需继承QMainWindow

	//给工具栏添加item
	toolBar->addAction("保存");
	toolBar->addAction("复制");
	toolBar->addAction("粘贴");
	QAction* action = new QAction(this);
	action->setText("转发");
	action->setIcon(QIcon("images/face_shibie.png"));
	toolBar->addAction(action);

	QPushButton* btn = new QPushButton(this);
	btn->setText("按钮");
	toolBar->addWidget(btn);//给工具栏添加Widget
}

ToolBarWindow::~ToolBarWindow()
{
}
