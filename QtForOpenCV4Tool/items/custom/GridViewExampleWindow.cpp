#include "GridViewExampleWindow.h"

GridViewExampleWindow::GridViewExampleWindow(QWidget* parent)
	: BaseSceneView(parent)
{
	this->setWindowTitle("自定义GridView");
	// 获取主屏幕  
	QScreen* screen = QApplication::primaryScreen();
	if (screen) {
		// 获取屏幕的尺寸  
		QRect screenSize = screen->geometry();
		int width = screenSize.width();
		int height = screenSize.height();
		this->setFixedSize(1000, 480);
		this->setGraphicsViewSize(1000, 480);
		/*this->setFixedSize(width, height);
		this->setGraphicsViewSize(width, height);*/
	}

	//选择图片
	ChoiceImageWidget* choiceImageWidget = new ChoiceImageWidget(this);
	choiceImageWidget->setFixedWidth(this->width() / 5 * 1);
	choiceImageWidget->setCallback([=](QString filePath) {
		this->filePath = filePath;
		qDebug() << "选择图片完成开始打印路径：" << filePath;
		this->execute();
		});

	//MinusPlusWidget* minusPlusWidget = new MinusPlusWidget(this);

	QHBoxLayout* hLayout = new QHBoxLayout(this);
	QVBoxLayout* vLayout = new QVBoxLayout(this);


	vLayout->addWidget(choiceImageWidget);
	//vLayout->addWidget(minusPlusWidget);
	vLayout->setAlignment(Qt::AlignTop);

	QVBoxLayout* vLayout2 = new QVBoxLayout(this);
	gridView = new VariableGridView(this);
	gridView->setStyleSheet("background-color:#000000");
	gridView->resize(QSize(this->width() / 5 * 4, this->height()));
	gridView->setViews(9,1);

	vLayout2->addWidget(gridView);
	hLayout->addLayout(vLayout, 1);
	hLayout->addLayout(vLayout2, 3);

}

QPixmap GridViewExampleWindow::handle() {
	qDebug() << "耗时任务";
	int gvWidth = gridView->width() / 3;
	int gvHeight = gridView->height() / 3;
	Mat src = imread(filePath.toStdString().c_str());
	//cv::resize(src, src, cv::Size(gvWidth, gvHeight));
	vector<QPixmap> pixmaps;
	for (int i = 0;i < 9;i++) {//, gvWidth / 5 * 4, gvHeight / 5 * 4
		pixmaps.push_back(ImageUtils::getPixmap(src.clone()));
	}
	gridView->setAdapter(pixmaps);
	return NULL;
}

GridViewExampleWindow::~GridViewExampleWindow()
{

}
