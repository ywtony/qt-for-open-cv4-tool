#pragma once

#include <QWidget>
#include <QGridLayout>
#include <QPushButton>
#include <QSizePolicy>
#include <QLabel>
#include <opencv2/opencv.hpp>
#include "../../common/utils/ImageUtils.h"

class GridLayoutExampleWindow : public QWidget
{
	Q_OBJECT

public:
	GridLayoutExampleWindow(QWidget *parent = nullptr);
	~GridLayoutExampleWindow();
protected:
	void resizeEvent(QResizeEvent* event);
private:
	QGridLayout* gridLayout;
	QLabel* label;
	QLabel* label2;
	QLabel* label3;
	QLabel* label4;
	QLabel* label5;
	QLabel* label6;
	QLabel* label7;
	QLabel* label8;
	QLabel* label9;

};
