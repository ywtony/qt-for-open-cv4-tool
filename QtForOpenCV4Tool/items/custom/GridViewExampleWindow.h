#pragma once

#include <QWidget>
#include "../../common/layout/VariableGridView.h"
#include <QPixmap>
#include "../../common/image_tip/ImageTip.h"
#include "../../common/base/BaseSceneView.h"
#include "../../common/button/Button.h"
#include "../../common/edittext/EditText.h"
#include "../../common/seekbar/Seekbar.h"
#include "../../common/minus_plus/MinusPlusWidget.h"
#include "../../common/seekbar/SeekBarTipsWidget.h"
#include "../../common/choice_image/ChoiceImageWidget.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <iostream>
#include <list>
#include <vector>
#include <QScreen>
#include <QApplication>

using namespace std;

class GridViewExampleWindow : public BaseSceneView
{
	Q_OBJECT

public:
	GridViewExampleWindow(QWidget* parent = nullptr);
	~GridViewExampleWindow();

protected:
	QPixmap handle();

private:
	QString filePath;
	int mValue = 1;
	VariableGridView* gridView;

};
