#include "StackedLayoutExampleWindow.h"

StackedLayoutExampleWindow::StackedLayoutExampleWindow(QWidget *parent)
	: QWidget(parent)
{
	this->setWindowTitle("StackedLayout布局");
	
	//创建主容器
	QHBoxLayout* hLayout = new QHBoxLayout;
	//创建一个ListWidget（列表）
	QListWidget* listWidget = new QListWidget(this);
	listWidget->setMinimumWidth(150);
	listWidget->setFont(QFont("宋体", 14));
	listWidget->addItem("添加Button");
	listWidget->addItem("添加图片");

	QWidget* widget1 = new QWidget(this);//第一个栈容器的内容
	widget1->setMinimumSize(400, 400);
	Button* but1 = new Button(widget1);
	but1->setText("提莫队长正在待命");

	QWidget* widget2 = new QWidget(this);//第二个栈容器的内容
	widget2->setMinimumSize(400, 400);
	QLabel* image = new QLabel(widget2);
	image->setScaledContents(true);
	image->setFixedSize(400, 400);
	cv::Mat src = imread("images/flowers.jpeg");
	image->setPixmap(ImageUtils::getPixmap(src));

	QStackedLayout* stackedLayout = new QStackedLayout;//栈布局
	stackedLayout->addWidget(widget1);
	stackedLayout->addWidget(widget2);

	hLayout->addWidget(listWidget, 1);//左边容器布局占整个容器的1/5
	hLayout->addLayout(stackedLayout, 4);//右边容器布局，占整个容器的4/5

	//把布局添加到Widget中
	this->setLayout(hLayout);

	//给listWidget添加事件，监听QListWidget::currentRowChanged信号，&QStackedLayout::setCurrentIndex槽函数
	connect(listWidget, &QListWidget::currentRowChanged, stackedLayout, &QStackedLayout::setCurrentIndex);
}

StackedLayoutExampleWindow::~StackedLayoutExampleWindow()
{
}
