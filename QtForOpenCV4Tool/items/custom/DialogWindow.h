#pragma once

#include <QMainWindow>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QColorDialog>//选择颜色
#include <QFileDialog>//选择文件或目录
#include <QFontDialog>//选择字体
#include <QInputDialog>//允许用户输入一个值并返回
#include <QMessageBox>//模态对话框，用于显示信息、询问问题等
#include <QProgressDialog>//显示操作过程
#include <QDialog>
#include <QColor>
#include <QLineEdit>
#include <QDebug>
#include <QFont>
#include <QCoreApplication>
#include <QString>
#include <QTimer>
#include <thread>
#include <QThread>
#include <QApplication>

using namespace std;


//案例：https://zhuanlan.zhihu.com/p/640635081
class DialogWindow : public QWidget
{
	Q_OBJECT

public:
	DialogWindow(QWidget *parent = nullptr);
	~DialogWindow();

public:
	QProgressDialog* pd;
private:
	QLineEdit* edtColor;
	QTimer* timer;
	int steps = 0;
signals:
	void sendValue(int value);
public slots:
	void getValue(int value);
};
