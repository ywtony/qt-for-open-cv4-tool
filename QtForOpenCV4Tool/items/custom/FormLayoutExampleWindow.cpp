#include "FormLayoutExampleWindow.h"

FormLayoutExampleWindow::FormLayoutExampleWindow(QWidget* parent)
	: QWidget(parent)
{
	this->setWindowTitle("Form表单");

	//表单布局
	EditText* edtUserName = new EditText;
	edtUserName->setPlaceholderText("请输入用户名");
	edtUserName->setFixedSize(150, 30);
	EditText* edtPassword = new EditText;
	edtPassword->setPlaceholderText("请输入密码");
	edtPassword->setFixedSize(150, 30);
	edtPassword->setEchoMode(EditText::Password);
	Button* btnLogin = new Button;
	btnLogin->setFixedSize(100, 40);
	btnLogin->setText("登录");
	QFormLayout* formLayout = new QFormLayout(this);
	formLayout->addRow("用户名:", edtUserName);
	formLayout->addRow("密    码:", edtPassword);
	formLayout->addRow(btnLogin);
	formLayout->setSpacing(10);
	formLayout->setMargin(50);
	formLayout->setAlignment(btnLogin, Qt::AlignRight);

	this->setLayout(formLayout);
	formLayout->setFormAlignment(Qt::AlignCenter);
	formLayout->setAlignment(Qt::AlignCenter);

	connect(btnLogin, &Button::clicked, [=]() {
		QString userName = edtUserName->text();
		QString password = edtPassword->text();
		if (userName.isEmpty()) {
			QString dlgTitle = "温馨提示";
			QString strInfo = "用户名不能为空？";
			QMessageBox::StandardButton result = QMessageBox::information(this, dlgTitle, strInfo,
				QMessageBox::Ok);
			return;
		}
		else if (password.isEmpty()) {
			QString dlgTitle2 = "温馨提示";
			QString strInfo2 = "用户密码不能为空？";
			QMessageBox::StandardButton result2 = QMessageBox::information(this, dlgTitle2, strInfo2,
				QMessageBox::Ok);
			return;
		}
		QString dlgTitle3 = "温馨提示";
		QString strInfo3 = "登录成功";
		QMessageBox::StandardButton result2 = QMessageBox::information(this, dlgTitle3, strInfo3,
			QMessageBox::Ok);

		});

}

FormLayoutExampleWindow::~FormLayoutExampleWindow()
{
}
