#include "VHLayoutExampleWindow.h"

VHLayoutExampleWindow::VHLayoutExampleWindow(QWidget* parent)
	: QWidget(parent)
{
	this->setWindowTitle("横向/竖直布局");
	QHBoxLayout* vLayoutContiner = new QHBoxLayout;
	QHBoxLayout* hLayout = new QHBoxLayout;
	Button* btn1 = new Button;
	btn1->setText("德玛西亚");
	Button* btn2 = new Button;
	btn2->setText("艾欧尼亚");
	Button* btn3 = new Button;
	btn3->setText("艾泽拉斯");
	Button* btn4 = new Button;
	btn4->setText("诺克萨斯");
	hLayout->addWidget(btn1);
	hLayout->addWidget(btn2);
	hLayout->addWidget(btn3);
	hLayout->addWidget(btn4);
	QVBoxLayout* vLayout = new QVBoxLayout;
	Button* btn5 = new Button;
	btn5->setText("恕瑞玛");
	Button* btn6 = new Button;
	btn6->setText("哈马行星");
	Button* btn7 = new Button;
	btn7->setText("M78星云");
	Button* btn8 = new Button;
	btn8->setText("虚空领域");
	vLayout->addWidget(btn4);
	vLayout->addWidget(btn5);
	vLayout->addWidget(btn6);
	vLayout->addWidget(btn7);

	vLayoutContiner->addLayout(vLayout, 1);
	vLayoutContiner->setAlignment(vLayout,Qt::AlignTop);
	vLayoutContiner->addLayout(hLayout, 4);
	vLayoutContiner->setAlignment(hLayout,Qt::AlignTop);
	this->setLayout(vLayoutContiner);
}

VHLayoutExampleWindow::~VHLayoutExampleWindow()
{

}
