#include "DialogWindow.h"

void progressFun(DialogWindow* window) {
	int i = 0;
	while (i <= 100) {
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
		i++;
		window->pd->setValue(i);
	}
}
void DialogWindow::getValue(int value) {
	qDebug() << "进度条:" << value;
	pd->setValue(value);
}
DialogWindow::DialogWindow(QWidget* parent)
	: QWidget(parent)
{
	this->setWindowTitle("各种弹框");
	this->setFixedSize(320, 480);

	QVBoxLayout* vLayout = new  QVBoxLayout(this);
	QPushButton* btnBaseDialog = new QPushButton(this);
	btnBaseDialog->setFixedHeight(30);
	btnBaseDialog->setText("基础dialog");

	QHBoxLayout* hLayoutColor = new QHBoxLayout(this);
	QPushButton* btnColorDialog = new QPushButton(this);
	btnColorDialog->setFixedHeight(30);
	btnColorDialog->setText("选择颜色");
	edtColor = new QLineEdit(this);
	edtColor->resize(60, 30);
	hLayoutColor->addWidget(edtColor);
	hLayoutColor->addWidget(btnColorDialog);

	QHBoxLayout* hFileDialogLayout = new QHBoxLayout(this);
	QPushButton* btnFileDialog = new QPushButton(this);
	btnFileDialog->setFixedHeight(30);
	btnFileDialog->setText("选择文件和目录");
	QPushButton* btnFileDialogMulti = new QPushButton(this);
	btnFileDialogMulti->setFixedHeight(30);
	btnFileDialogMulti->setText("选择多个文件");
	hFileDialogLayout->addWidget(btnFileDialog);
	hFileDialogLayout->addWidget(btnFileDialogMulti);



	QPushButton* btnFontDialog = new QPushButton(this);
	btnFontDialog->setFixedHeight(30);
	btnFontDialog->setText("选择字体");
	QPushButton* btnInputDialog = new QPushButton(this);
	btnInputDialog->setFixedHeight(30);
	btnInputDialog->setText("允许用户输入一个值，并将其返回");

	QHBoxLayout* hLayout2 = new QHBoxLayout(this);
	QPushButton* btnInfoDialog = new QPushButton(this);//
	btnInfoDialog->setFixedHeight(30);
	btnInfoDialog->setText("info");
	QPushButton* btnWarnDialog = new QPushButton(this);//
	btnWarnDialog->setFixedHeight(30);
	btnWarnDialog->setText("Warn");
	QPushButton* btnCriticalDialog = new QPushButton(this);//
	btnCriticalDialog->setFixedHeight(30);
	btnCriticalDialog->setText("critical");
	QPushButton* btnAboutDialog = new QPushButton(this);//
	btnAboutDialog->setFixedHeight(30);
	btnAboutDialog->setText("about");
	hLayout2->addWidget(btnInfoDialog);
	hLayout2->addWidget(btnWarnDialog);
	hLayout2->addWidget(btnCriticalDialog);
	hLayout2->addWidget(btnAboutDialog);


	QPushButton* btnProgressDialog = new QPushButton(this);//显示操作过程
	btnProgressDialog->setFixedHeight(30);
	btnProgressDialog->setText("显示操作过程");

	vLayout->addWidget(btnBaseDialog);
	vLayout->addLayout(hLayoutColor);
	vLayout->addLayout(hFileDialogLayout);
	vLayout->addWidget(btnFontDialog);
	vLayout->addWidget(btnInputDialog);
	vLayout->addLayout(hLayout2);
	vLayout->addWidget(btnProgressDialog);

	//vLayout->setDirection(QBoxLayout::BottomToTop);
	vLayout->setSpacing(10);
	this->setLayout(vLayout);
	//基础dialog
	connect(btnBaseDialog, &QPushButton::clicked, [=]() {
		QDialog* dialog = new QDialog;
		dialog->setWindowTitle(tr("基础弹框"));
		dialog->show();
		});
	//选择颜色
	connect(btnColorDialog, &QPushButton::clicked, [=]() {
		QPalette pal = edtColor->palette(); //获取现有 palette
		QColor  iniColor = pal.color(QPalette::Text); //现有的文字颜色
		QColor color = QColorDialog::getColor(iniColor, this, "选择颜色");
		qDebug() << "获取颜色";
		if (color.isValid()) //选择有效
		{
			pal.setColor(QPalette::Text, color); //palette 设置选择的颜色
			edtColor->setPalette(pal); //设置 palette
			//edtColor->setText(color);
		}
		});
	//选择文件或者目录
	connect(btnFileDialog, &QPushButton::clicked, [=]() {
		QString curPath = QCoreApplication::applicationDirPath(); //获取应用程序的路径
		QString dlgTitle = "保存文件"; //对话框标题
		QString filter = "文本文件(*.txt);;h文件(*.h);;C++文件(.cpp);;所有文件(*.*)"; //文件过滤器
		QString aFileName = QFileDialog::getSaveFileName(this, dlgTitle, curPath, filter);
		if (!aFileName.isEmpty())
			edtColor->setText(aFileName);
		});

	//多选文件
	connect(btnFileDialogMulti, &QPushButton::clicked, [=]() {
		QStringList list = QFileDialog::getOpenFileNames(this, tr("请选择多个文件"), "C:/Users/DBF-DEV-103/Downloads/", tr("Images (*.jpg *.png)"));
		for (QString filePath : list) {
			qDebug() << "filePath:" << filePath;
		}
		});

	//选择字体
	connect(btnFontDialog, &QPushButton::clicked, [=]() {
		QFont iniFont = edtColor->font(); //获取文本框的字体
		bool   ok = false;
		QFont font = QFontDialog::getFont(&ok, iniFont); //选择字体
		if (ok) //选择有效
			edtColor->setFont(font);
		});
	//允许用户输入一个值，并将其值返回
	connect(btnInputDialog, &QPushButton::clicked, [=]() {
		//输入字符串
		QString dlgTitle = "输入文字对话框";
		QString txtLabel = "请输入文件名";
		QString defaultInput = "新建文件.txt";
		QLineEdit::EchoMode echoMode = QLineEdit::Normal;//正常文字输入
		//QLineEdit::EchoMode echoMode=QLineEdit::Password;//密码输入
		bool ok = false;
		QString text = QInputDialog::getText(this, dlgTitle, txtLabel, echoMode, defaultInput, &ok);
		if (ok && !text.isEmpty())
			edtColor->setText(text);
		});

	//模态对话框，用于显示信息、询问问题等
	connect(btnInfoDialog, &QPushButton::clicked, [=]() {
		QString dlgTitle = "删除";
		QString strInfo = "确定要删除吗？";
		QMessageBox::StandardButton result = QMessageBox::information(this, dlgTitle, strInfo,
			QMessageBox::Ok, QMessageBox::No);
		if (result == QMessageBox::Ok) {
			qDebug() << "确定";
		}
		else if (result == QMessageBox::No) {
			qDebug() << "取消";
		}

		});
	connect(btnWarnDialog, &QPushButton::clicked, [=]() {
		QString dlgTitle = "警告";
		QString strInfo = "刀锋来袭";
		QMessageBox::warning(this, dlgTitle, strInfo);
		});
	connect(btnCriticalDialog, &QPushButton::clicked, [=]() {
		QString dlgTitle = "恕瑞玛";
		QString strInfo = "恕瑞玛，你的皇帝回来了";
		QMessageBox::critical(this, dlgTitle, strInfo);
		});
	connect(btnAboutDialog, &QPushButton::clicked, [=]() {
		QString dlgTitle = "Tips";
		QString strInfo = "德玛西亚万岁";
		QMessageBox::about(this, dlgTitle, strInfo);
		});
	//显示操作过程
	connect(btnProgressDialog, &QPushButton::clicked, [=]() {
		pd = new QProgressDialog("当前进度...", "Cancel", 0, 100);
		pd->setWindowFlags(Qt::Dialog | Qt::CustomizeWindowHint | Qt::WindowTitleHint);		//不显示进度条上的“最小化”“最大化”“关闭”
		pd->setWindowTitle("任务保存");	//窗口标题
		pd->setAutoClose(true);		//进度达到最大值时不关闭，默认为true
		pd->setLabelText("任务保存中...");	//显示的文本
		pd->setRange(0, 100);				//设置进度条的极值，默认为[0,100]
		//pd->setCancelButton(NULL);			//不显示取消按钮
		pd->show();						//进度条显示

		//std::thread mThread(progressFun, this);
		timer->start(20);
		});
	timer = new QTimer(this);
	connect(timer, &QTimer::timeout, this, [=] {
		pd->setValue(steps);
		steps++;
		//终止条件
		if (steps > pd->maximum()) {
			timer->stop();
			steps = 0;
		}
		});
}

DialogWindow::~DialogWindow()
{

}
