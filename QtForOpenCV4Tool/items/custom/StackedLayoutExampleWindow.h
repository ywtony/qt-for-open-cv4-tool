#pragma once

#include <QWidget>
#include <QStackedLayout >
#include <QListWidget>
#include "../../common/button/Button.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include "../../common/utils/ImageUtils.h"

class StackedLayoutExampleWindow : public QWidget
{
	Q_OBJECT

public:
	StackedLayoutExampleWindow(QWidget *parent = nullptr);
	~StackedLayoutExampleWindow();

private:
};
