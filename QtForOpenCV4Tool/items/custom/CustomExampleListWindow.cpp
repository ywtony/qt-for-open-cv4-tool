#include "CustomExampleListWindow.h"

CustomExampleListWindow::CustomExampleListWindow(QWidget* parent)
	: QWidget(parent)
{
	this->resize(QSize(320, 480));
	this->setWindowTitle("OpenCV Tool");
	this->setWindowIcon(QIcon("images/opencv.png"));
	createListView();
}

void CustomExampleListWindow::createListView() {
	listView = new CommonListView(this);
	connect(listView, &CommonListView::itemClicked, [=](QListWidgetItem* item) {
		CommonListViewItem* item2 = dynamic_cast<CommonListViewItem*>(item);
		qDebug() << "itemPos:" << item2->mPos;
		switch (item2->mPos) {
		case 0://自定义按钮
			customWidgetWindow.show();
			break;
		case 1://GridView
			gridViewExampleWindow.show();
			break;
		case 2://铆接部件
			floatingWindow.show();
			break;
		case 3://菜单栏
			menuBarWindow.show();
			break;
		case 4://工具栏
			toolBarWindow.show();
			break;
		case 5://状态栏
			statusBarWindow.show();
			break;
		case 6://中心部件
			centerWidgetWindow.show();
			break;
		case 7://dialog
			dialogWindow.show();
			break;
		case 8:
			gridLayoutExampleWindow.show();
			break;
		case 9:
			formLayoutExampleWindow.show();
			break;
		case 10:
			vHLayoutExampleWindow.show();
			break;
		case 11:
			stackedLayoutExampleWindow.show();
			break;
		case 12:
			tabWidgetExampleWindow.show();
			break;
		case 13:
			treeWidgetExampleWindow.show();
			break;
		case 14:
			eventExampleWindow.show();
			break;
		}
		});
	new CommonListViewItem("自定义按钮", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 0, listView);
	new CommonListViewItem("GridView测试", QIcon("images/images_cli.png"), QIcon("images/images_cli.png"), 1, listView);
	new CommonListViewItem("铆接部件", QIcon("images/images_cli.png"), QIcon("images/images_cli.png"), 2, listView);
	new CommonListViewItem("菜单栏", QIcon("images/images_cli.png"), QIcon("images/images_cli.png"), 3, listView);
	new CommonListViewItem("工具栏", QIcon("images/images_cli.png"), QIcon("images/images_cli.png"), 4, listView);
	new CommonListViewItem("状态栏", QIcon("images/images_cli.png"), QIcon("images/images_cli.png"), 5, listView);
	new CommonListViewItem("中心部件", QIcon("images/images_cli.png"), QIcon("images/images_cli.png"), 6, listView);
	new CommonListViewItem("QDialog", QIcon("images/images_cli.png"), QIcon("images/images_cli.png"), 7, listView);
	new CommonListViewItem("网格布局", QIcon("images/images_cli.png"), QIcon("images/images_cli.png"), 8, listView);
	new CommonListViewItem("表单布局", QIcon("images/images_cli.png"), QIcon("images/images_cli.png"), 9, listView);
	new CommonListViewItem("横向竖直布局", QIcon("images/images_cli.png"), QIcon("images/images_cli.png"), 10, listView);
	new CommonListViewItem("Stack栈布局", QIcon("images/images_cli.png"), QIcon("images/images_cli.png"), 11, listView);
	new CommonListViewItem("TabLayout布局", QIcon("images/images_cli.png"), QIcon("images/images_cli.png"), 12, listView);
	new CommonListViewItem("TreeWidget布局", QIcon("images/images_cli.png"), QIcon("images/images_cli.png"), 13, listView);
	new CommonListViewItem("Qt的Event事件", QIcon("images/images_cli.png"), QIcon("images/images_cli.png"), 14, listView);

}

CustomExampleListWindow::~CustomExampleListWindow()
{
}
