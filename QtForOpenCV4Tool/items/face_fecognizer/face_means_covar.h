#ifndef FACE_MEANS_COVAR_H
#define FACE_MEANS_COVAR_H

#include <QWidget>
#include <QLabel>
#include "../../common/CommonGraphicsView.h"

/**
 * 求取均值以及协方差
 * @brief The Face_Means_Covar class
 */
class Face_Means_Covar : public CommonGraphicsView
{
    Q_OBJECT
public:
    explicit Face_Means_Covar(QWidget *parent = nullptr);
    void showMeansAndCovar(const char* filePath);
protected:
    void dropEvent(QDropEvent *event)override;
private:
    QString path;
    //均值
    QLabel *labelMeansValue;
    //标准差
    QLabel *labelStddevValue;
    //协方差
    QLabel *labelCovarValue;



signals:

};

#endif // FACE_MEANS_COVAR_H
