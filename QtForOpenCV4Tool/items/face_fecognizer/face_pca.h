#ifndef FACE_PCA_H
#define FACE_PCA_H

#include <QWidget>
#include "../../common/CommonGraphicsView.h"

class Face_PCA : public CommonGraphicsView
{
    Q_OBJECT
public:
    explicit Face_PCA(QWidget *parent = nullptr);
    void showPCA(const char *filePath);
    void pcaAnalyze(vector<Point> &points ,Mat &result);
protected:
    void dropEvent(QDropEvent *event)override;
private:
    QString path;

signals:

};

#endif // FACE_PCA_H
