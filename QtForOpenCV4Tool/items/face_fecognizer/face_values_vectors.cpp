#include "face_values_vectors.h"

Face_Values_Vectors::Face_Values_Vectors(QWidget *parent)
    : CommonGraphicsView{parent}
{
    this->setWindowTitle("输出数据特征及特征向量的值");
    showVectorsAndValues();
}

void Face_Values_Vectors::dropEvent(QDropEvent *event){
//    path = event->mimeData()->urls().at(0).toLocalFile();
//    qDebug()<<"file path:"<<path;
//    showVectorsAndValues(path.toStdString().c_str());

}
void Face_Values_Vectors::showVectorsAndValues(){
    Mat src = (Mat_<double>(5,5)<<
            1,2,3,4,5,
            6,7,8,9,10,
            11,12,13,14,15,
            16,17,18,19,20,
            21,22,23,24,25);
    if(src.empty()){
        qDebug()<<"图片为空";
        return;
    }
    imshow("src",src);

    //计算数据特征值及特征向量
    Mat eigenValues,//特征值
            eigenVectors;//特征向量
    //src的输入必须是 CV_32FC1 or CV_64FC1
    eigen(src,eigenValues,eigenVectors);
    cout << "特征值："<<endl;
    cout <<eigenValues<<endl;
    cout <<"特征向量："<<endl;
    cout <<eigenVectors<<endl;

}
