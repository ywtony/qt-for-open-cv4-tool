#include "face_fisher_facerecognizer.h"

Face_Fisher_FaceRecognizer::Face_Fisher_FaceRecognizer(QWidget *parent)
    : CommonGraphicsView{parent}
{
    this->setWindowTitle("Fisher 人脸识别");
    QPushButton * btn = new QPushButton(this);
    btn->setText("准备数据");
    connect(btn,&QPushButton::clicked,[=](){
        choiceData();
    });
}

void Face_Fisher_FaceRecognizer::choiceData(){
    QString filePath = QFileDialog::getOpenFileName(this,"人脸数据文件","/Users/yangwei/Documents/tony/opencv/orl_faces","数据文件(*.txt)");
    qDebug()<<filePath;
    showFisherFaceRecognizer(filePath.toStdString().c_str());
}

void Face_Fisher_FaceRecognizer::showFisherFaceRecognizer(const char * filePath){
    ifstream file(filePath,ifstream::in);
    if(!file){
        qDebug()<<"file count not found";
        return;
    }
    //准备数据阶段
    string line ,path,classLabel;
    vector<Mat> images;
    vector<int> labels;
    while(getline(file,line)){
        stringstream liness(line);
        getline(liness,path,' ');
        getline(liness,classLabel);
        images.push_back(imread(path,0));
        labels.push_back(atoi(classLabel.c_str()));
    }
    cout <<"准备数据完成："<<images.size()<<endl;

    //检测准备的训练数据是否正常
    if(images.size()<1||labels.size()<1){
        qDebug()<<"准备的数据异常";
        return;
    }

    //测试查样本数据的宽高
    int width = images[0].cols;
    int height = images[0].rows;
    cout << "width:"<<width<<",height:"<<height<<endl;

    //准备测试数据
    Mat testMat = images[images.size()-1];
    int testLabel = labels[labels.size()-1];
    images.pop_back();
    labels.pop_back();

    ////创建Fisher人脸识别实例,并开始训练数据
    //Ptr<BasicFaceRecognizer> model = FisherFaceRecognizer::create();
    //model->train(images,labels);//训练

    ////预测
    //int predictLabel = model->predict(testMat);
    ////如果预测的标签值和测试的标签值一致就说明人脸识别是成功的。
    //cout <<"testLabel:"<<testLabel <<",predictLabel:"<<predictLabel<<endl;




}
