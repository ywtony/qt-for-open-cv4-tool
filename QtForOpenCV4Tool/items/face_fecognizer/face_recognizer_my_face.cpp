#include "face_recognizer_my_face.h"

Face_Recognizer_My_Face::Face_Recognizer_My_Face(QWidget *parent)
    : CommonGraphicsView{parent}
{
    this->setWindowTitle("人脸识别小案例");
    QPushButton *btn = new QPushButton(this);
    btn->setText("选择视频文件");
    connect(btn,&QPushButton::clicked,[=](){
        chooseVideoFile();
    });
}


void Face_Recognizer_My_Face::chooseVideoFile(){
    QString filePath = QFileDialog::getOpenFileName(this,"选择视频文件","/Users/yangwei/Downloads/","数据文件(*.mp4)");
    qDebug()<<filePath;
    recognizerMyFace(filePath.toStdString().c_str());
}

void Face_Recognizer_My_Face::recognizerMyFace(const char * filePath){
    //准备训练数据
    string transFileName = "/Users/yangwei/Documents/tony/opencv/myfaces/targetData.txt";
    ifstream file(transFileName,ifstream::in);
    if(!file){
        qDebug()<<"准备训练数据";
        return;
    }
    string line ,path,classLabel;
    vector<Mat> images;
    vector<int> labels;
    while(getline(file,line)){
        stringstream liness(line);
        getline(liness,path,' ');
        getline(liness,classLabel);
        images.push_back(imread(path, 0));
        labels.push_back(atoi(classLabel.c_str()));
    }
    if(images.size()<1||labels.size()<1){
        qDebug()<<"准备的训练数据不对";
        return;
    }

    int width = images[0].cols;
    int height = images[0].rows;
    cout << "width:"<<width<<",height:"<<height<<endl;
    //测试数据
    Mat testMat = images[images.size()-1];
    int testLabel = labels[labels.size()-1];

    //使用特征脸进行数据检验
//    Ptr<BasicFaceRecognizer> model = EigenFaceRecognizer::create();
//    //训练
//    model->train(images,labels);

//    model->save("/Users/yangwei/Documents/tony/opencv/myfaces/test.txt");

    //Ptr<BasicFaceRecognizer> model =Algorithm::load<EigenFaceRecognizer>("/Users/yangwei/Documents/tony/opencv/myfaces/test.txt");


    ////预测
    //int predictLabel = model->predict(testMat);
    //cout <<"测试标签："<<testLabel<<",预测标签："<<predictLabel<<endl;;

    ////使用级联分类器找到人脸区域
    //string faceFilePath = "/usr/local/share/opencv4/haarcascades/haarcascade_frontalface_alt_tree.xml";
    //CascadeClassifier faceDetector;
    //faceDetector.load(faceFilePath);


    //VideoCapture capture;
    //capture.open(filePath);
    //if(!capture.isOpened()){
    //    qDebug()<<"打开视频文件失败";
    //    return;
    //}

    //Mat frame,dst;
    //vector<Rect> faces;
    //while(capture.read(frame)){
    //    faceDetector.detectMultiScale(frame,faces,1.1,3,0,Size(100,100),Size(400,400));
    //    for(int i=0;i<faces.size();i++){
    //        Mat roi = frame(faces[i]);
    //        cvtColor(roi,dst,COLOR_BGR2GRAY);
    //        cv::resize(dst,testMat,Size(100,100));
    //        int label = model->predict(testMat);
    //        rectangle(frame,faces[i],Scalar(255,0,0),2,8,0);
    //        putText(frame, format("i'm %s", (label == 110 ? "tony" : "Unknow")), faces[i].tl(), FONT_HERSHEY_PLAIN, 5.0, Scalar(0, 0, 255), 2, 8);
    //    }
    //    imshow("frame",frame);
    //    int key = waitKey(1);
    //    if(key==27){
    //        break;
    //    }

    //}

}
