#include "face_pca.h"

Face_PCA::Face_PCA(QWidget *parent)
    : CommonGraphicsView{parent}
{
    this->setWindowTitle("PCA降维");
}


void Face_PCA::dropEvent(QDropEvent *event){
    path = event->mimeData()->urls().at(0).toLocalFile();
    showPCA(path.toStdString().c_str());

}
void Face_PCA::showPCA(const char *filePath){
    Mat src = imread(filePath);
    if(src.empty()){
        qDebug()<<"输入图像为空";
        return;
    }
    imshow("src",src);

    Mat gray,binary;
    cvtColor(src,gray,COLOR_BGR2GRAY);//灰度化
    //二值化
    threshold(gray,binary,0,255,THRESH_BINARY|THRESH_OTSU);
    imshow("binary",binary);
    //发现轮廓
    vector<vector<Point>> contours;
    vector<Vec4i> heri;
    findContours(binary,contours,RETR_LIST,CHAIN_APPROX_NONE);
    Mat result = src.clone();//clone一个副本
    for(size_t i = 0;i<contours.size();i++){
        //过滤掉面积较小的区域
        double area = contourArea(contours[i]);
        if (area > 1e5 || area < 1e2) continue;
        drawContours(result,contours,i,Scalar(0,0,255),3,LINE_8);//绘制轮廓
        pcaAnalyze(contours[i],result);
    }
    imshow("result",result);

}

/**
 * 使用PCA分析轮廓，找到中心点位置以及轮廓方向
 * @brief Face_PCA::pcaAnalyze
 * @param points
 * @param result
 */
void Face_PCA::pcaAnalyze(vector<Point> &points,Mat &result){
    int size = points.size();
    Mat data_pts = Mat(size,2,CV_64FC1);
    for(int i=0;i<size;i++){
        data_pts.at<double>(i,0) = points[i].x;
        data_pts.at<double>(i,1) = points[i].y;
    }
    qDebug()<<"开始pca分析";
    //使用PCA进行分析
    PCA pca_analyze(data_pts,Mat(),PCA::DATA_AS_ROW);//实例化PCA
    //找出轮廓的圆心：均值数据的第一个值即为数据中心点
    Point center = Point(pca_analyze.mean.at<double>(0,0),pca_analyze.mean.at<double>(0,1));
    //绘制轮廓中心点
    circle(result,center,3,Scalar(0,0,255),3,LINE_8);
    qDebug()<<"准备利用pca寻找轮廓方向";
    //找出并绘制出轮廓方向
    vector<Point2d> vecs(2);//特征向量
    vector<double> vals(2);//特征值
    for(int i=0;i<2;i++){
        vals[i] = pca_analyze.eigenvalues.at<double>(i,0);
        vecs[i] = Point2d(pca_analyze.eigenvectors.at<double>(i,0),pca_analyze.eigenvectors.at<double>(i,1));
    }

    Point p1 = center+ 0.02*Point(static_cast<int>(vecs[0].x*vals[0]), static_cast<int>(vecs[0].y*vals[0]));
    Point p2 = center - 0.05*Point(static_cast<int>(vecs[1].x*vals[1]), static_cast<int>(vecs[1].y*vals[1]));
    line(result,center,p1,Scalar(0,255,0),3,LINE_8);
    line(result,center,p2,Scalar(0,255,0),3,LINE_8);

    double angle = atan2(vecs[0].y,vecs[0].x);
    qDebug()<<"角度："<<180*(angle/CV_PI);



}
