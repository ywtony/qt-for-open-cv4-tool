#ifndef FACE_EIGEN_FACE_RECOGNIZER_H
#define FACE_EIGEN_FACE_RECOGNIZER_H

#include <QWidget>
#include "../../common/CommonGraphicsView.h"
#include <fstream>
#include <string>
#include <QPushButton>
#include <sys/types.h>
#include <sys/stat.h>
//#include <dirent.h>
#include <QFileDialog>
#include <stdio.h>
#include <stdlib.h>
#include <opencv2/opencv.hpp>
//#include <opencv2/face.hpp>
using namespace std;
//using namespace cv::face;

/**
 * 特征脸识别
 * @brief The Face_Eigen_Face_Recognizer class
 */
class Face_Eigen_Face_Recognizer : public CommonGraphicsView
{
    Q_OBJECT
public:
    explicit Face_Eigen_Face_Recognizer(QWidget *parent = nullptr);
    void showEgenFaceRecoginzer(const char * filePath);
    void prepareImageData(const char * dirPath,char *appStr);

protected:
    void dropEvent(QDropEvent *event) override;
private:
    QString path;
    ofstream out;

signals:

};

#endif // FACE_EIGEN_FACE_RECOGNIZER_H
