#ifndef FACE_COLLECT_FACE_DATA_H
#define FACE_COLLECT_FACE_DATA_H

#include <QWidget>
#include "../../common/CommonGraphicsView.h"
#include <QPushButton>
#include <QFileDialog>


class Face_Collect_Face_Data : public CommonGraphicsView
{
    Q_OBJECT
public:
    explicit Face_Collect_Face_Data(QWidget *parent = nullptr);
    void saveCollectFaceData(const char * filePath);
    void chooseVideo();

signals:

};

#endif // FACE_COLLECT_FACE_DATA_H
