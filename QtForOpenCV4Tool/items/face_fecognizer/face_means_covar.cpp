#include "face_means_covar.h"

Face_Means_Covar::Face_Means_Covar(QWidget *parent)
    : CommonGraphicsView{parent}
{
    this->setWindowTitle("计算Mat均值、标准差、及协方差");
    QLabel *labelTitle = new QLabel(this);
    labelTitle->setText("输出结果如下：");
    //均值
    QLabel *labelMeansTitle = new QLabel(this);
    labelMeansTitle->setText("均值：");
    labelMeansTitle->move(0,labelTitle->y()+labelTitle->height()+10);
    labelMeansValue = new QLabel(this);
    labelMeansValue->setFixedWidth(150);
    labelMeansValue->move(labelMeansTitle->x()+labelMeansTitle->width()+10,labelMeansTitle->y());
    //标准差
    QLabel *labelStddevTitle = new QLabel(this);
    labelStddevTitle->setText("标准差：");
    labelStddevTitle->move(0,labelMeansTitle->y()+labelMeansTitle->height()+10);
    labelStddevValue = new QLabel(this);
    labelStddevValue->setFixedWidth(150);
    labelStddevValue->move(labelStddevTitle->x()+labelStddevTitle->width()+10,labelStddevTitle->y());
    //协方差
    QLabel *labelCovarTitle = new QLabel(this);
    labelCovarTitle->setText("协方差:");
    labelCovarTitle->move(0,labelStddevTitle->y()+labelStddevTitle->height()+10);
    labelCovarValue = new QLabel(this);
    labelCovarValue->setFixedWidth(150);
    labelCovarValue->move(labelCovarTitle->x()+labelCovarTitle->width()+10,labelCovarTitle->y());

}

void Face_Means_Covar::dropEvent(QDropEvent *event){
    path = event->mimeData()->urls().at(0).toLocalFile();
    qDebug()<<"文件路径："<<path;
    showMeansAndCovar(path.toStdString().c_str());

}

void Face_Means_Covar::showMeansAndCovar(const char *filePath){
    Mat src = imread(filePath);
    if(src.empty()){
        qDebug()<<"图片文件为空";
        return;

    }
    imshow("src",src);

    //计算图像均值以及标准差
    Mat means,stddev;
    meanStdDev(src,means,stddev,noArray());
    //输出均值矩阵，图像有多少通道means就有多少个值。例如：三通道图像，则有第一通道的均值，第二通道的均值，第三通道的均值
    cout << means<<endl;
    //输出标准差矩阵，图像有多少个通道stddev就有多少个值。例如：单通道图像，则0的位置就是通道标准差
    cout<<stddev<<endl;

    //计算所有通道的平均值
    double resultMeans = 0.0;
    for(int i=0;i<means.rows;i++){
        resultMeans+=means.at<double>(i);
         qDebug()<<"均值："<< means.at<double>(i);
    }
    QString rMeans=QString("%1").arg(resultMeans/3);
    labelMeansValue->setText(rMeans);


    //计算所有通道的标准差
    double resultStddev=0.0;
    for(int i=0;i<stddev.rows;i++){
        resultStddev+=stddev.at<double>(i);
        qDebug()<<"标准差："<<stddev.at<double>(i);
    }
    QString rStddev=QString("%1").arg(resultStddev/3);
    labelStddevValue->setText(rStddev);


    //计算协方差
    Mat covar,means2;
    cvtColor(src,src,COLOR_BGR2GRAY);
    //这里的输入需要是单通道的channgle==1
    calcCovarMatrix(src,covar,means2,COVAR_NORMAL|cv::COVAR_ROWS);
    qDebug()<<"以下是协方差矩阵";
    //一定要搞一个小一点的图测试
    cout << covar<<endl;
    cout << means2<<endl;



}
