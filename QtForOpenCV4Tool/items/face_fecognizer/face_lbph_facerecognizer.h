#ifndef FACE_LBPH_FACERECOGNIZER_H
#define FACE_LBPH_FACERECOGNIZER_H

#include <QWidget>
#include <QPushButton>
#include "../../common/CommonGraphicsView.h"
#include <fstream>
#include <QFileDialog>
//#include <opencv2/face.hpp>

//using namespace cv::face;


class Face_LBPH_FaceRecognizer : public CommonGraphicsView
{
    Q_OBJECT
public:
    explicit Face_LBPH_FaceRecognizer(QWidget *parent = nullptr);
    void showLbphFaceRecgnizer(const char * filePath);
    void chooseDataFile();

signals:

};

#endif // FACE_LBPH_FACERECOGNIZER_H
