#ifndef FACE_RECOGNIZER_MY_FACE_H
#define FACE_RECOGNIZER_MY_FACE_H

#include <QWidget>
#include <QPushButton>
#include <QFileDialog>
#include "../../common/CommonGraphicsView.h"
//#include <opencv2/face.hpp>
#include <fstream>

//using namespace cv::face;

class Face_Recognizer_My_Face : public CommonGraphicsView
{
    Q_OBJECT
public:
    explicit Face_Recognizer_My_Face(QWidget *parent = nullptr);
    void recognizerMyFace(const char * filePath);
    void chooseVideoFile();

signals:

};

#endif // FACE_RECOGNIZER_MY_FACE_H
