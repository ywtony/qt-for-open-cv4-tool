#include "face_lbph_facerecognizer.h"

Face_LBPH_FaceRecognizer::Face_LBPH_FaceRecognizer(QWidget *parent)
    : CommonGraphicsView{parent}
{
    this->setWindowTitle("LBPH人脸识别");
    QPushButton *btn = new QPushButton(this);
    btn->setText("选择训练数据文件");
    connect(btn,&QPushButton::clicked,[=](){
        chooseDataFile();
    });
}

void Face_LBPH_FaceRecognizer::chooseDataFile(){
    QString filePath = QFileDialog::getOpenFileName(this,"人脸数据文件","/Users/yangwei/Documents/tony/opencv/orl_faces","数据文件(*.txt)");
    qDebug()<<filePath;
    showLbphFaceRecgnizer(filePath.toStdString().c_str());
}

void Face_LBPH_FaceRecognizer::showLbphFaceRecgnizer(const char * filePath){
    ifstream file(filePath,ifstream::in);
    if(!file){
        qDebug()<<"加载人脸训练数据失败";
        return;
    }
    //准备训练数据
    string line ,path,classLabel;
    vector<Mat> images;
    vector<int> labels;
    while(getline(file,line)){
        stringstream liness(line);
        getline(liness,path,' ');
        getline(liness,classLabel);
        images.push_back(imread(path,0));
        labels.push_back(atoi(classLabel.c_str()));
    }

    //准备测试数据
    Mat testMat = images[images.size()-1];
    int testLabel = labels[labels.size()-1];
    images.pop_back();
    labels.pop_back();

    ////实例化LBPH人脸识别算法实例
    //Ptr<LBPHFaceRecognizer> model = LBPHFaceRecognizer::create();
    //model->train(images,labels);//训练

    ////预测
    //int predictLabel = model->predict(testMat);
    ////如果测试标签值和预测标签值抑制则说明人脸识别成功，否则不成功
    //cout <<"测试标签值："<<testLabel<<",预测标签值："<<predictLabel<<endl;




}
