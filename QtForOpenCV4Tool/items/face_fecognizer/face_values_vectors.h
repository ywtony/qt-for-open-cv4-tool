#ifndef FACE_VALUES_VECTORS_H
#define FACE_VALUES_VECTORS_H

#include <QWidget>
#include "../../common/CommonGraphicsView.h"

class Face_Values_Vectors : public CommonGraphicsView
{
    Q_OBJECT
public:
    explicit Face_Values_Vectors(QWidget *parent = nullptr);
    void showVectorsAndValues();
protected:
    void dropEvent(QDropEvent *event)override;
private:
    QString path;

signals:

};

#endif // FACE_VALUES_VECTORS_H
