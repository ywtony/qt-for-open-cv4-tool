#ifndef FACE_FISHER_FACERECOGNIZER_H
#define FACE_FISHER_FACERECOGNIZER_H

#include <QWidget>
#include <QPushButton>
#include "../../common/CommonGraphicsView.h"
#include <QFileDialog>
#include <fstream>
#include <iostream>
//#include <opencv2/face.hpp>
//using namespace cv::face;


class Face_Fisher_FaceRecognizer : public CommonGraphicsView
{
    Q_OBJECT
public:
    explicit Face_Fisher_FaceRecognizer(QWidget *parent = nullptr);
    void showFisherFaceRecognizer(const char * filePath);
    void choiceData();

signals:

};

#endif // FACE_FISHER_FACERECOGNIZER_H
