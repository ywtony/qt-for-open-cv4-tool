#ifndef HF_OBJECT_TRACKING_H
#define HF_OBJECT_TRACKING_H

#include <QWidget>
#include "../../common/CommonGraphicsView.h"
#include <QPushButton>
#include <QMessageBox>
#include <QFileDialog>

/**
 * 使用稠密光流实现对象跟踪
 * @brief The HF_Object_Tracking class
 */
class HF_Object_Tracking : public CommonGraphicsView
{
    Q_OBJECT
public:
    explicit HF_Object_Tracking(QWidget *parent = nullptr);
    void hfObjectTracking(const char *filePath);
    void choiceVideo();
private:
    QString path;

signals:

};

#endif // HF_OBJECT_TRACKING_H
