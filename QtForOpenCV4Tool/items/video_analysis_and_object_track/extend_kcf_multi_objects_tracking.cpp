#include "extend_kcf_multi_objects_tracking.h"

Extend_KCF_Multi_Objects_Tracking::Extend_KCF_Multi_Objects_Tracking(QWidget *parent)
    : CommonGraphicsView{parent}
{
    this->setWindowTitle("扩展模块中多对象跟踪");
    QPushButton *btn = new QPushButton(this);
    btn->setText("选择视频");
    connect(btn,&QPushButton::clicked,[=](){
        choiceVideo();
    });
}

void Extend_KCF_Multi_Objects_Tracking::choiceVideo(){
    path = QFileDialog::getOpenFileName(this,"请选择视频","/Users/yangwei/Downloads/",tr("Image Files(*.mp4 *.avi)"));
    qDebug()<<"视频路径："<<path;
    showExtendKCFMultiObjectsTracking(path.toStdString().c_str());
}

void Extend_KCF_Multi_Objects_Tracking::showExtendKCFMultiObjectsTracking(const char *filePath){
    VideoCapture capture;
    capture.open(filePath);

    if(!capture.isOpened()){
        qDebug()<<"无法打开视频文件";
        return;
    }
    Mat frame;
    capture.read(frame);//读取第一帧
    //选取frame中的多个对象
    vector<Rect> objects;
    selectROIs("frame",frame,objects);
//    MultiTracker trackers("KCF");

    while(capture.read(frame)){
        imshow("frame",frame);
        int c = waitKey(1);
        if(c==27){
            break;
        }
    }
    capture.release();

}
