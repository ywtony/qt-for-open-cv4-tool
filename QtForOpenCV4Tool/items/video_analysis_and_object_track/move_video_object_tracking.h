#ifndef MOVE_VIDEO_OBJECT_TRACKING_H
#define MOVE_VIDEO_OBJECT_TRACKING_H

#include <QWidget>
#include <QPushButton>
#include <QMessageBox>
#include <QFileDialog>

#include "../../common/CommonGraphicsView.h"

class Move_Video_Object_Tracking : public CommonGraphicsView
{
    Q_OBJECT
public:
    explicit Move_Video_Object_Tracking(QWidget *parent = nullptr);
    void choiceVideo();
    void showMoveVideoObjectTracking(const char * filePath);
private:
    QString path;

signals:

};

#endif // MOVE_VIDEO_OBJECT_TRACKING_H
