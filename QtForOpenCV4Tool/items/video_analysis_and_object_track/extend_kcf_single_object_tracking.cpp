#include "extend_kcf_single_object_tracking.h"

Extend_KCF_Single_Object_Tracking::Extend_KCF_Single_Object_Tracking(QWidget *parent)
    : CommonGraphicsView{parent}
{
    this->setWindowTitle("扩展模块中的但对象跟踪");
    QPushButton *btn = new QPushButton(this);
    btn->setText("选择视频");
    connect(btn,&QPushButton::clicked,[=](){
        choiceVideo();
    });
}

void Extend_KCF_Single_Object_Tracking::choiceVideo(){
    path = QFileDialog::getOpenFileName(this,"请选择视频","/Users/yangwei/Downloads/",tr("Image Files(*.mp4 *.avi)"));
    qDebug()<<"视频路径："<<path;
    showExtendKCFSingleObjectTracking(path.toStdString().c_str());

}

void Extend_KCF_Single_Object_Tracking::showExtendKCFSingleObjectTracking(const char* filePath){
    VideoCapture capture;
    capture.open(filePath);

    if(!capture.isOpened()){
        qDebug()<<"无法加载视频文件";
        return;
    }

    Mat frame;
    //读取第一帧
    capture.read(frame);
    //选取ROI区域
    Rect rect = selectROI(frame);
    if(rect.width<=0||rect.height<=0){
        qDebug()<<"必须选择一个roi区域";
        return;
    }
    //实例化KCF，ps：这属于高版本的4.5.5，低版本的创建方法不是这样的
    //Ptr<TrackerKCF> tracker = TrackerKCF::create();
    ////初始化ROI区域
    //tracker->init(frame,rect);
    ////循环读取视频帧并跟踪
    //while(capture.read(frame)){
    //    //更新frame
    //    tracker->update(frame,rect);
    //    //将roi区域绘制出来
    //    rectangle(frame,rect,Scalar(0,255,0),3,8,0);
    //    //限制最终的跟踪frame
    //    imshow("frame",frame);
    //    int c = waitKey(1);
    //    if(c == 27){
    //        break;
    //    }
    //}
    //capture.release();
}
