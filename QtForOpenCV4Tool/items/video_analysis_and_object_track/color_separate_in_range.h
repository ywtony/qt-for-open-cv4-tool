#ifndef COLOR_SEPARATE_IN_RANGE_H
#define COLOR_SEPARATE_IN_RANGE_H

#include <QWidget>
#include "../../common/CommonGraphicsView.h"

class Color_Separate_In_Range : public CommonGraphicsView
{
    Q_OBJECT
public:
    explicit Color_Separate_In_Range(QWidget *parent = nullptr);
    void showColorSplit(const char * filePath);
protected:
    void dropEvent(QDropEvent *event) override;
private:
    QString path;

signals:

};

#endif // COLOR_SEPARATE_IN_RANGE_H
