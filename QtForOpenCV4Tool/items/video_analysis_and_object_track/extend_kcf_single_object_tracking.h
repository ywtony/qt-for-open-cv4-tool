#ifndef EXTEND_KCF_SINGLE_OBJECT_TRACKING_H
#define EXTEND_KCF_SINGLE_OBJECT_TRACKING_H

#include <QWidget>
#include <QPushButton>
#include <QMessageBox>
#include <QFileDialog>
#include "../../common/CommonGraphicsView.h"
//#include <opencv2/tracking.hpp>

class Extend_KCF_Single_Object_Tracking : public CommonGraphicsView
{
    Q_OBJECT
public:
    explicit Extend_KCF_Single_Object_Tracking(QWidget *parent = nullptr);
    void choiceVideo();
    void showExtendKCFSingleObjectTracking(const char* filePath);
private:
    QString path;

signals:

};

#endif // EXTEND_KCF_SINGLE_OBJECT_TRACKING_H
