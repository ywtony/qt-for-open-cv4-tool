#ifndef CAM_SHIFT_VIDEO_OBJECT_TRACKING_H
#define CAM_SHIFT_VIDEO_OBJECT_TRACKING_H

#include <QWidget>
#include <QPushButton>
#include <QMessageBox>
#include <QFileDialog>
//#include <opencv2/tracking.hpp>
#include "../../common/CommonGraphicsView.h"



class CAM_Shift_Video_Object_Tracking : public CommonGraphicsView
{
    Q_OBJECT
public:
    explicit CAM_Shift_Video_Object_Tracking(QWidget *parent = nullptr);
    void choiceVideo();
    void videoObjectTracking(const char *filePath);
private:
    QString path;

signals:

};

#endif // CAM_SHIFT_VIDEO_OBJECT_TRACKING_H
