#ifndef MOG_VIDEO_BACKGROUND_REMOVE_H
#define MOG_VIDEO_BACKGROUND_REMOVE_H

#include <QWidget>
#include "../../common/CommonGraphicsView.h"
#include <QPushButton>
#include<QLabel>
#include <QButtonGroup>
#include <QRadioButton>
#include <QFileDialog>
#include <QMessageBox>

/**
 * MOG视频背景消除
 * @brief The MOG_Video_Background_Remove class
 */
class MOG_Video_Background_Remove : public CommonGraphicsView
{
    Q_OBJECT
public:
    explicit MOG_Video_Background_Remove(QWidget *parent = nullptr);
    void showMogBackgroundRemove(const char* filePath);
    void showKNNBackgroundRemove(const char* filePath);
private:
    QString path;

signals:

};

#endif // MOG_VIDEO_BACKGROUND_REMOVE_H
