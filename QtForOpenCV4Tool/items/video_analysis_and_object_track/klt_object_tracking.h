#ifndef KLT_OBJECT_TRACKING_H
#define KLT_OBJECT_TRACKING_H

#include <QWidget>
#include <QPushButton>
#include <QMessageBox>
#include <QFileDialog>
#include <QButtonGroup>
#include <QRadioButton>

#include "../../common/CommonGraphicsView.h"

/**
 * KLT稀疏光流对象跟踪
 * @brief The KLT_Object_Tracking class
 */
class KLT_Object_Tracking : public CommonGraphicsView
{
    Q_OBJECT
public:
    explicit KLT_Object_Tracking(QWidget *parent = nullptr);
    void startKltTracking(const char* filePath);
private:
    QString path;
    bool isShowLine;
signals:

};

#endif // KLT_OBJECT_TRACKING_H
