#ifndef COLOR_BASED_OBJECT_TRACKING_H
#define COLOR_BASED_OBJECT_TRACKING_H

#include <QWidget>
#include "../../common/CommonGraphicsView.h"
#include <QMessageBox>
#include <QFileDialog>
#include <QPushButton>

/**
 * 基于颜色的对象跟踪-视频
 * @brief The Color_Based_Object_Tracking class
 */
class Color_Based_Object_Tracking : public CommonGraphicsView
{
    Q_OBJECT
public:
    explicit Color_Based_Object_Tracking(QWidget *parent = nullptr);
    void showColorObjectTracking(const char *filePath);
    void openVideoPath();
private:
    QString path;
signals:

};

#endif // COLOR_BASED_OBJECT_TRACKING_H
