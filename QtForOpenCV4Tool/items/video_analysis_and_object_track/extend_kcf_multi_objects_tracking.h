#ifndef EXTEND_KCF_MULTI_OBJECTS_TRACKING_H
#define EXTEND_KCF_MULTI_OBJECTS_TRACKING_H

#include <QWidget>
#include <QPushButton>
#include <QFileDialog>
#include <QMessageBox>
#include "../../common/CommonGraphicsView.h"
//#include <opencv2/tracking.hpp>

/**
 * 在4.5.5中多目标跟踪方法未找到，有点小无奈
 * @brief The Extend_KCF_Multi_Objects_Tracking class
 */
class Extend_KCF_Multi_Objects_Tracking : public CommonGraphicsView
{
    Q_OBJECT
public:
    explicit Extend_KCF_Multi_Objects_Tracking(QWidget *parent = nullptr);
    void choiceVideo();
    void showExtendKCFMultiObjectsTracking(const char * filePath);
private:
    QString path;

signals:

};

#endif // EXTEND_KCF_MULTI_OBJECTS_TRACKING_H
