#ifndef FACE_RECOGNIZER_WINDOW_H
#define FACE_RECOGNIZER_WINDOW_H

#include <QWidget>
#include <QPixmap>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QMimeData>
#include <QFileInfo>
#include <QMessageBox>
#include <QResizeEvent>
#include <QModelIndex>
#include <QIcon>
#include "../common/utils/ImageUtils.h"
#include "../common/CommonListView.h"
#include "../common/CommonListViewItem.h"
#include "face_fecognizer/face_means_covar.h"
#include "face_fecognizer/face_values_vectors.h"
#include "face_fecognizer/face_pca.h"
#include "face_fecognizer/face_eigen_face_recognizer.h"
#include "face_fecognizer/face_fisher_facerecognizer.h"
#include "face_fecognizer/face_lbph_facerecognizer.h"
#include "face_fecognizer/face_collect_face_data.h"
#include "face_fecognizer/face_recognizer_my_face.h"

/**
 * 人脸识别相关知识及其案例
 * @brief The Face_Recognizer_Window class
 */
class Face_Recognizer_Window : public QWidget
{
	Q_OBJECT
public:
	explicit Face_Recognizer_Window(QWidget* parent = nullptr);
	void createListView();//创建一个ListView

private:
	CommonListView* listView;
	Face_Means_Covar faceMeansCovar;
	Face_Values_Vectors faceValuesVectors;
	Face_PCA facePac;
	Face_Eigen_Face_Recognizer eigenFaceRecognizer;
	Face_Fisher_FaceRecognizer fisherFaceRecognizer;
	Face_LBPH_FaceRecognizer lbphFaceRecognizer;
	Face_Collect_Face_Data faceCollectFaceData;
	Face_Recognizer_My_Face faceRecognizerMyFace;

signals:

};

#endif // FACE_RECOGNIZER_WINDOW_H
