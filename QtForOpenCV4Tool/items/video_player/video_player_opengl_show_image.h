#ifndef VIDEO_PLAYER_OPENGL_SHOW_IMAGE_H
#define VIDEO_PLAYER_OPENGL_SHOW_IMAGE_H

#include <QPaintEvent>
#include <QPainter>
#include <QPushButton>
#include <QImage>
#include <QOpenGLWidget>

class Video_Player_OpenGL_Show_Image : public QOpenGLWidget
{
    Q_OBJECT

public:
    explicit Video_Player_OpenGL_Show_Image(QWidget *parent = nullptr);
    void chooseImage();
protected:
    void paintEvent(QPaintEvent *event) override;
signals:

};

#endif // VIDEO_PLAYER_OPENGL_SHOW_IMAGE_H
