#include "video_player_roate_flip.h"

Video_Player_Roate_Flip::Video_Player_Roate_Flip(QWidget *parent)
    : QWidget{parent}
{
    this->setWindowTitle("图片旋转与镜像");
    this->setFixedSize(320,480);
    //选择图片
    QPushButton *chooseImageBtn = new QPushButton(this);
    chooseImageBtn->setText("选择图片");
    connect(chooseImageBtn,&QPushButton::clicked,[=](){//选择图片
        chooseImage();
    });
    //图像旋转
    QRadioButton *rotate1 = new QRadioButton(this);
    rotate1->move(0,chooseImageBtn->y()+chooseImageBtn->height()+5);
    rotate1->setText("顺时针180°");
    QRadioButton *rotate2 = new QRadioButton(this);
    rotate2->move(rotate1->x()+rotate1->width()+5,chooseImageBtn->y()+chooseImageBtn->height()+5);
    rotate2->setText("顺时针90°");
    QRadioButton *rotate3 = new QRadioButton(this);
    rotate3->move(rotate2->x()+rotate2->width()+5,chooseImageBtn->y()+chooseImageBtn->height()+5);
    rotate3->setText("逆时针90°");
    connect(rotate1,&QRadioButton::clicked,[=](){
        showImageRoate(0);
    });
    connect(rotate2,&QRadioButton::clicked,[=](){
        showImageRoate(1);
    });
    connect(rotate3,&QRadioButton::clicked,[=](){
        showImageRoate(2);
    });

    //图像镜像
    QRadioButton *rotate4 = new QRadioButton(this);
    rotate4->move(0,rotate1->y()+rotate1->height()+5);
    rotate4->setText("沿y轴翻转");
    QRadioButton *rotate5 = new QRadioButton(this);
    rotate5->move(rotate4->x()+rotate4->width(),rotate1->y()+rotate1->height()+5);
    rotate5->setText("沿x轴翻转");
    QRadioButton *rotate6 = new QRadioButton(this);
    rotate6->move(rotate5->x()+rotate5->width(),rotate1->y()+rotate1->height()+5);
    rotate6->setText("沿xy轴翻转");
    connect(rotate4,&QRadioButton::clicked,[=](){
        showImageFlip(0);
    });
    connect(rotate5,&QRadioButton::clicked,[=](){
        showImageFlip(1);
    });
    connect(rotate6,&QRadioButton::clicked,[=](){
        showImageFlip(2);
    });
}

void Video_Player_Roate_Flip::chooseImage(){
    path = QFileDialog::getOpenFileName(this,"选择图像","/Users/yangwei/Downloads/","Image File(*.jpg *.jpeg *.png *.bmp)");
    qDebug()<<path;
}

void Video_Player_Roate_Flip::showImageRoate(int type){
    Mat src = imread(path.toStdString().c_str());
    if(src.empty()){
        qDebug()<<"不能为空";
        return;
    }
    imshow("src",src);
    Mat dst1,dst2,dst3;
    switch(type){
    case 0://
        cv::rotate(src,dst1,ROTATE_180);//顺时针180°
         imshow("dst1",dst1);
        break;
    case 1:
        cv::rotate(src,dst2,ROTATE_90_CLOCKWISE);//顺时针90°
        imshow("dst2",dst2);
        break;
    case 2:
        cv::rotate(src,dst3,ROTATE_90_COUNTERCLOCKWISE);//逆时针90°
        imshow("dst3",dst3);
        break;
    }

}

void Video_Player_Roate_Flip::showImageFlip(int type){
    Mat src = imread(path.toStdString().c_str());
    if(src.empty()){
        qDebug()<<"不能为空";
        return;
    }
    imshow("src",src);
    Mat dst1,dst2,dst3;
    switch(type){
    case 0:
        cv::flip(src,dst1,1);//y轴翻转
        imshow("dst1",dst1);
        break;
    case 1:
        cv::flip(src,dst2,0);//x轴翻转
         imshow("dst2",dst2);
        break;
    case 2:
        cv::flip(src,dst3,-1);//xy轴翻转
         imshow("dst3",dst3);
        break;
    }

}
