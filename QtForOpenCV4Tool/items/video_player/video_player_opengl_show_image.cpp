#include "video_player_opengl_show_image.h"

Video_Player_OpenGL_Show_Image::Video_Player_OpenGL_Show_Image(QWidget *parent)
    : QOpenGLWidget{parent}
{
    this->setWindowTitle("OpenGL显示图片");
    this->setFixedSize(320,480);
    QPushButton *btn = new QPushButton(this);
    btn->setText("选择图像");
    connect(btn,&QPushButton::clicked,[=](){
        chooseImage();
    });
}

void Video_Player_OpenGL_Show_Image::chooseImage(){

}

void Video_Player_OpenGL_Show_Image::paintEvent(QPaintEvent *event){
    QPainter painter;
    painter.begin(this);
    QImage image;
    image.load("/Users/yangwei/Downloads/2.jpeg");
    //设置图像自适应窗口大小
    QImage result = image.scaled(this->width(),this->height(),Qt::KeepAspectRatio,Qt::SmoothTransformation);
    painter.drawImage(QPoint(0,0),result);
    painter.end();

}
