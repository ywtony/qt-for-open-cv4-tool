#ifndef VIDEO_PLAYER_ROATE_FLIP_H
#define VIDEO_PLAYER_ROATE_FLIP_H

#include <QWidget>
#include <QPushButton>
#include <QRadioButton>
#include <QFileDialog>
#include <QString>
#include <QDebug>
#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;



class Video_Player_Roate_Flip : public QWidget
{
    Q_OBJECT
public:
    explicit Video_Player_Roate_Flip(QWidget *parent = nullptr);
    //选择图片
    void chooseImage();
    //旋转
    void showImageRoate(int type);
    //镜像
    void showImageFlip(int type);
private:
    QString path;

signals:

};

#endif // VIDEO_PLAYER_ROATE_FLIP_H
