#ifndef VIDEO_PLAYER_SPLICING_IMAGE_H
#define VIDEO_PLAYER_SPLICING_IMAGE_H

#include <QWidget>
#include <QPushButton>
#include <QDebug>
#include <QFileDialog>
#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;


/**
 * 图像拼接
 * @brief The Video_Player_Splicing_Image class
 */
class Video_Player_Splicing_Image : public QWidget
{
    Q_OBJECT
public:
    explicit Video_Player_Splicing_Image(QWidget *parent = nullptr);
    void chooseOneImage();
    void chooseTwoImage();
    void showResultImage();
private:
    QString oneImagePath;
    QString twoImagePath;

signals:

};

#endif // VIDEO_PLAYER_SPLICING_IMAGE_H
