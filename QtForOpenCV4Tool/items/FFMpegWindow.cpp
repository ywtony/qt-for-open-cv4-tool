#include "FFMpegWindow.h"

FFMpegWindow::FFMpegWindow(QWidget* parent)
	: QMainWindow(parent)
{
	this->resize(QSize(320, 480));
	this->setWindowTitle("FFMPEG");
	this->setWindowIcon(QIcon("images/opencv.png"));
	createListView();
}

void FFMpegWindow::createListView() {
	listView = new CommonListView(this);
	connect(listView, &CommonListView::itemClicked, [=](QListWidgetItem* item) {
		CommonListViewItem* item2 = dynamic_cast<CommonListViewItem*>(item);
		qDebug() << "itemPos:" << item2->mPos;
		switch (item2->mPos) {
		case 1://简单的视频播放器
			fFMpegSamplePlayerWindow.show();
			break;
		case 2://解封装
			fFMpegDemuxerWindow.show();
			break;
		case 3://读取yuv数据并用opengl渲染
			xVideoWindow.show();
			break;
		case 4://简单的播放器，带封装的
			xVideoPlayerWindow.show();
			break;
		case 5://xplayer
			xPlay2.show();
			break;
		case 6://封装
			fFMpegRecordInterfaceWindow.show();
			break;
		case 7://rtmp推流
			rtmpPusherWindow.show();
			break;
		case 8://opencv rtsp to rtmp push stream
			openCVRtspToRtmpWindow.show();
			break;
		case 9://利用qt录制音频
			useQtRecordAudioWindow.show();
			break;
		case 10://ffmpeg + rtmp+audio push
			rtmpPushAudioUseFFmpegWindow.show();
			break;
		}
		});
	new CommonListViewItem("FFMPEG播放器", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 1, listView);
	new CommonListViewItem("FFMPEG解封装", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 2, listView);
	new CommonListViewItem("读取yuv数据并用OpenGL渲染", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 3, listView);
	new CommonListViewItem("ffmpeg+opengl简单的播放器", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 4, listView);
	new CommonListViewItem("xplayer", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 5, listView);
	new CommonListViewItem("FFmpeg rtsp 转rtmp推流", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 6, listView);
	new CommonListViewItem("FFmpeg Rtmp推流（从文件中推流）", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 7, listView);
	new CommonListViewItem("利用OpenCV将rtsp流转rtmp并推送至服务器", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 8, listView);
	new CommonListViewItem("利用Qt录制音频小案例", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 9, listView);
	new CommonListViewItem("FFmpeg_rtmp_audio推流", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 10, listView);

}

FFMpegWindow::~FFMpegWindow()
{
}
