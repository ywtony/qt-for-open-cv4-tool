#include "NetworkWindow.h"

NetworkWindow::NetworkWindow(QWidget* parent)
	: QWidget(parent)
{
	this->setWindowTitle("网络请求");
	this->setFixedSize(QSize(320, 480));
	createListView();
}

void NetworkWindow::createListView() {
    listView = new CommonListView(this);
    connect(listView, &CommonListView::itemClicked, [=](QListWidgetItem* item) {
        CommonListViewItem* item2 = dynamic_cast<CommonListViewItem*>(item);
        qDebug() << "itemPos:" << item2->mPos;
        switch (item2->mPos) {
              case 0:
                  loginWindow.show();
                  break;
        }
        });
    new CommonListViewItem("登录/获取用户信息", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 0, listView);
    //new CommonListViewItem("合并两张图像为一张图像", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 1, listView);
}

NetworkWindow::~NetworkWindow()
{

}
