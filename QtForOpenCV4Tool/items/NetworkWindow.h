#pragma once

#include <QWidget>
#include <QPixmap>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QMimeData>
#include <QFileInfo>
#include <QMessageBox>
#include <QResizeEvent>
#include <QModelIndex>
#include <QIcon>
#include "../common/utils/ImageUtils.h"
#include "../common/CommonListView.h"
#include "../common/CommonListViewItem.h"

#include "net/LoginWindow.h"

using namespace std;

class NetworkWindow : public QWidget
{
	Q_OBJECT

public:
	NetworkWindow(QWidget* parent = nullptr);
	~NetworkWindow();

public:
	void login(QString username, QString password);


private:
	EditText* etUserName;
	EditText* etPassword;
	Button* btnLogin;

public:
	void createListView();//创建一个ListView

private:
	CommonListView* listView;
	LoginWindow loginWindow;
};
