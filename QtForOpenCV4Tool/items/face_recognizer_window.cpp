#include "face_recognizer_window.h"

Face_Recognizer_Window::Face_Recognizer_Window(QWidget *parent)
    : QWidget(parent)
{
    this->setFixedSize(QSize(320,480));
    createListView();
}

void Face_Recognizer_Window::createListView(){
    listView = new CommonListView(this);
    connect(listView, &CommonListView::itemClicked, [=](QListWidgetItem* item) {
        CommonListViewItem* item2 = dynamic_cast<CommonListViewItem*>(item);
        qDebug() << "itemPos:" << item2->mPos;
        switch (item2->mPos) {
        case 0:
            faceMeansCovar.show();
            break;
        case 1:
            faceValuesVectors.show();
            break;
        case 2:
            facePac.show();
            break;
        case 3:
            eigenFaceRecognizer.show();
            break;
        case 4:
            fisherFaceRecognizer.show();
            break;
        case 5:
            lbphFaceRecognizer.show();
            break;
        case 6:
            faceCollectFaceData.show();
            break;
        case 7:
            faceRecognizerMyFace.show();
            break;
        }
        });
    new CommonListViewItem("求取均值、标准差及协方差", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 0, listView);
    new CommonListViewItem("求取特征值及特征向量", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 1, listView);
    new CommonListViewItem("PCA降维", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 2, listView);
    new CommonListViewItem("特征脸识-Eigen", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 3, listView);
    new CommonListViewItem("人脸识别-Fisher", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 4, listView);
    new CommonListViewItem("人脸识别-LBPH", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"),5, listView);
    new CommonListViewItem("人脸识别-收集人脸数据", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 6, listView);
    new CommonListViewItem("人脸识别小案例-识别自己的人脸", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 7, listView);
}
