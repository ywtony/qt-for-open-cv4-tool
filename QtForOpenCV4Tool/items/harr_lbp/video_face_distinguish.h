#ifndef VIDEO_FACE_DISTINGUISH_H
#define VIDEO_FACE_DISTINGUISH_H

//识别摄像头中的人脸

#include <QWidget>
#include "../../common/CommonGraphicsView.h"

class Video_Face_Distinguish : public CommonGraphicsView
{
    Q_OBJECT
public:
    explicit Video_Face_Distinguish(QWidget *parent = nullptr);
    void showVideoFaceDistinguish();
protected:
    void dropEvent(QDropEvent *event) override;

signals:

};

#endif // VIDEO_FACE_DISTINGUISH_H
