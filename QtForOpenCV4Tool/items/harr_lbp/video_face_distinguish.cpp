#include "video_face_distinguish.h"
#include <QThread>


Video_Face_Distinguish::Video_Face_Distinguish(QWidget *parent)
    : CommonGraphicsView{parent}
{
    this->setWindowTitle("识别摄像头中的人脸");
//    showVideoFaceDistinguish();
}


void Video_Face_Distinguish::dropEvent(QDropEvent *event){
    QString filePath = event->mimeData()->urls().at(0).toLocalFile();
    showVideoFaceDistinguish();
}

void Video_Face_Distinguish::showVideoFaceDistinguish(){
    String filePath = "/usr/local/share/opencv4/haarcascades/haarcascade_frontalface_alt.xml";
    CascadeClassifier face_classifier;
    if(!face_classifier.load(filePath)){

        qDebug()<<"Haar特征数据文件不能为空";
        return;
    }

    VideoCapture videoCapture;
    videoCapture.open("/Users/yangwei/Downloads/1649470407696618.mp4");
    if(!videoCapture.isOpened()){
        qDebug()<<"打开了摄像头";
        return;
    }

    Mat frame,gray;
    while(videoCapture.read(frame)){
        qDebug()<<"读取视频帧";
        cvtColor(frame,gray,COLOR_BGR2GRAY);
        equalizeHist(gray,gray);
        vector<Rect> faces;
        face_classifier.detectMultiScale(gray,faces,1.3,15,0,Size(30,30));
        for(size_t i = 0;i<faces.size();i++){
            rectangle(frame,faces[i],Scalar(0,0,255),3,LINE_8);
        }
        imshow("src",frame);
        waitKey(1000 / videoCapture.get(CAP_PROP_FPS)-10);
    }
    videoCapture.release();




}
