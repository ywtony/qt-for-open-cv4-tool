#include "image_face_distinguish.h"

Image_Face_Distinguish::Image_Face_Distinguish(QWidget *parent)
    : CommonGraphicsView{parent}
{
    this->setWindowTitle("显示图片中的人脸");
}
void Image_Face_Distinguish::dropEvent(QDropEvent *event){
    QString path = event->mimeData()->urls().at(0).toLocalFile();
    showDistinguishFace(path.toStdString().c_str());
}

void Image_Face_Distinguish::showDistinguishFace(const char * filePath){
    Mat src = imread(filePath);
    if(src.empty()){
        qDebug()<<"图片为空";
        return;
    }

    //人脸识别特征文件路径haar特征文件
    String face_path = "/usr/local/share/opencv4/haarcascades/haarcascade_frontalface_alt.xml";
    CascadeClassifier face_classifier;//初始化
    if(!face_classifier.load(face_path)){//加载特征数据
        qDebug()<<"特征数据加载失败";
        return;
    }

    //gray
    Mat gray;
    cvtColor(src,gray,COLOR_BGR2GRAY);//转灰度图像
    equalizeHist(gray,gray);//直方图均衡化

    vector<Rect> faces;//存储检测到的人脸
    face_classifier.detectMultiScale(gray,faces,1.1,3,0);//在不同的尺度空间检测人脸

    for(size_t i=0;i<faces.size();i++){
       rectangle(src,faces[i],Scalar(0,0,255),1,LINE_8);//将人脸绘制出来
    }


    QImage image = ImageUtils::matToQImage(src);
    QPixmap pixmap = QPixmap::fromImage(image);
    QGraphicsPixmapItem *item = new QGraphicsPixmapItem(pixmap.scaled(this->size(),Qt::KeepAspectRatio,Qt::SmoothTransformation));
    scene.addItem(item);


}
