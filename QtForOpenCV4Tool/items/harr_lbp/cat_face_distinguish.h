#ifndef CAT_FACE_DISTINGUISH_H
#define CAT_FACE_DISTINGUISH_H

#include <QWidget>
#include "../../common/CommonGraphicsView.h"

class Cat_Face_Distinguish : public CommonGraphicsView
{
    Q_OBJECT
public:
    explicit Cat_Face_Distinguish(QWidget *parent = nullptr);
    void showDistinguishCatFace(const char * filePath);
protected:
   void dropEvent(QDropEvent *event) override;
signals:

};

#endif // CAT_FACE_DISTINGUISH_H
