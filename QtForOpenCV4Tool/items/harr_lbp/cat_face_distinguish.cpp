#include "cat_face_distinguish.h"

Cat_Face_Distinguish::Cat_Face_Distinguish(QWidget *parent)
    : CommonGraphicsView{parent}
{
    this->setWindowTitle("检测猫脸");
}


void Cat_Face_Distinguish::showDistinguishCatFace(const char* filePath){
    String catFile = "/usr/local/share/opencv4/haarcascades/haarcascade_frontalcatface.xml";
    CascadeClassifier cat_face;
    if(!cat_face.load(catFile)){
        qDebug()<<"猫脸特征数据加载失败";
        return;
    }

    Mat src = imread(filePath);
    if(src.empty()){
        qDebug()<<"输入图片为空";
        return;
    }
    Mat gray;
    vector<Rect> faces;
    cvtColor(src,gray,COLOR_BGR2GRAY);
    equalizeHist(gray,gray);

    cat_face.detectMultiScale(gray,faces,1.1,3,0,Size(100,100));
    for(size_t i=0;i<faces.size();i++){
        rectangle(src,faces[i],Scalar(0,0,255),3,LINE_8);
    }
    imshow("src",src);

}

void Cat_Face_Distinguish::dropEvent(QDropEvent *event){
    QString filePath = event->mimeData()->urls().at(0).toLocalFile();
    showDistinguishCatFace(filePath.toStdString().c_str());
}
