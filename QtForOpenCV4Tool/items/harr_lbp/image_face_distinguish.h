#ifndef IMAGE_FACE_DISTINGUISH_H
#define IMAGE_FACE_DISTINGUISH_H
//图像人脸识别
#include "../../common/CommonGraphicsView.h"

#include <QWidget>

class Image_Face_Distinguish : public CommonGraphicsView
{
    Q_OBJECT
public:
    explicit Image_Face_Distinguish(QWidget *parent = nullptr);
    void showDistinguishFace(const char * filePath);
protected:
   void dropEvent(QDropEvent *event) override;

signals:

};

#endif // IMAGE_FACE_DISTINGUISH_H
