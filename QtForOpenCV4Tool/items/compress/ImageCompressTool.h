#pragma once

#include <QWidget>
#include "../../common/CommonGraphicsView.h"
#include "../../common/button/Button.h"
#include "../../common/edittext/EditText.h"
#include "../../common/seekbar/Seekbar.h"
#include "../../common/combobox/ComboBox.h"
#include "../../common/radiobutton/RadioButton.h"
#include <QFileDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QVBoxLayout>
#include <iostream>
#include <QSize>
#include <QFile>
#include <QButtonGroup>
#include <Qt>
#include <QFileInfo>
#include <fstream>
#include <QMessageBox>
#include <QPDFWriter>

class ImageCompressTool : public CommonGraphicsView
{
    Q_OBJECT
public:
    explicit ImageCompressTool(QWidget* parent = nullptr);
    ~ImageCompressTool();
	//压缩图片
    void compressImage();
	//一次性压缩多张图片
	void compressImages();
	//获取文件大小，以kb为单位
	long matSizeInKB(std::string filePath);
private:
	QString filePath;
	QLabel* imageViewSrc;
	QLabel* imageViewResult;
	QLabel* qualitLabel;
	QLabel* srcImageKb;
	QLabel* compressImageKb;
	ComboBox* comBox;
	int mKsize = 30;//必须是奇数，3,5,7,9,11,13,15
	int imageType = 0;//0=jpeg,1=png
	QStringList filePaths;
signals:

};