#pragma once

#include <QtWidgets/QMainWindow>
#include "../common/CommonListView.h"
#include "../common/CommonListView.h"
#include <QSize>
#include <QIcon>
#include "opengl/GLTriangleWindow.h"
#include "opengl/GLSquareWindow.h"
#include "opengl/GLTextureWindow.h"
#include "opengl/GLTwoTriangleWindow.h"
#include "opengl/GLTwoVAOTriangleWindow.h"
#include "opengl/GLTwoShaderTriangleWindow.h"
#include "opengl/GLColorDataFromVertexWindow.h"
#include "opengl/GLTextureGirlAndFaceWindow.h"
#include "opengl/GLTextureColorWindow.h"
#include "opengl/GLTextureMixWindow.h"
#include "opengl/GLTextureTranslateRotateScaleWindow.h"
#include "opengl/GLTextureMvpTestWindow.h"
#include "opengl/GLTextureMVP3DWindow.h"

#include <QMetaType>
//OpenGL学习资料->从入门到精通
//https://learnopengl-cn.github.io/01%20Getting%20started/02%20Creating%20a%20window/
//FBO讲解
// https://blog.csdn.net/liuyizhou95/article/details/89279230
//比较有特色的着色器
class OpenGLSampleWindow : public QMainWindow
{
	Q_OBJECT

public:
	OpenGLSampleWindow(QWidget* parent = nullptr);
	~OpenGLSampleWindow();
public:
	void createListView();//创建一个ListView

private:
	CommonListView* listView;
	GLTriangleWindow glTriangleWindow;
	GLSquareWindow glSquareWindow;
	GLTextureWindow glTextureWindow;
	GLTwoTriangleWindow glTwoTriangleWindow;
	GLTwoVAOTriangleWindow glTwoVAOTriangleWindow;
	GLTwoShaderTriangleWindow glTwoShaderTriangleWindow;
	GLColorDataFromVertexWindow glColorDataFromVertexWindow;
	GLTextureGirlAndFaceWindow glTextureGirlAndFaceWindow;
	GLTextureColorWindow glTextureColorWindow;
	GLTextureMixWindow glTextureMixWindow;
	GLTextureTranslateRotateScaleWindow glTextureTranslateRotateScaleWindow;
	GLTextureMvpTestWindow glTextureMvpTestWindow;
	GLTextureMVP3DWindow glTextureMVP3DWindow;
};
