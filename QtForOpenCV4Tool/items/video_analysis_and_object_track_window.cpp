#include "video_analysis_and_object_track_window.h"

Video_Analysis_And_Object_Track_Window:: Video_Analysis_And_Object_Track_Window(QWidget *parent)
    : QWidget(parent)
{
    //this->setWindowTitle("视频分析与对象跟踪");
    this->setFixedSize(QSize(320,480));
    createListView();
}

void Video_Analysis_And_Object_Track_Window::createListView(){
    listView = new CommonListView(this);
    connect(listView, &CommonListView::itemClicked, [=](QListWidgetItem* item) {
        CommonListViewItem* item2 = dynamic_cast<CommonListViewItem*>(item);
        qDebug() << "itemPos:" << item2->mPos;
        switch (item2->mPos) {
        case 0:
            mogVideoBackgroundRemove.show();
            break;
        case 1:
            colorBasedObjectTracking.show();
            break;
        case 2:
            kltObjectTracking.show();
            break;
        case 3:
            hfObjectTracking.show();
            break;
        case 4:
            camShiftVideoObjectTracking.show();
            break;
        case 5:
            colorSpearateInRang.show();
            break;
        case 6:
            moveVideoObjectTracking.show();
            break;
        case 7:
            extendKCFSingleObjectTracking.show();
            break;
        case 8://
            extendKCFMulitObjectsTracking.show();
            break;
        }
        });
    new CommonListViewItem("MOG2视频背景消除", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 0, listView);
    new CommonListViewItem("基于颜色的对象跟踪", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 1, listView);
    new CommonListViewItem("KLT稀疏光流实现对象跟踪", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 2, listView);
    new CommonListViewItem("稠密光流对象跟踪", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 3, listView);
    new CommonListViewItem("CAMShift视频对象跟踪", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 4, listView);
    new CommonListViewItem("HSV+inRange色彩空间分离", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 5, listView);
    new CommonListViewItem("视频中移动对象计数", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 6, listView);
    new CommonListViewItem("扩展模块中的单对象跟踪", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 7, listView);
    new CommonListViewItem("扩展模块中的多对象跟踪", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 8, listView);
}
