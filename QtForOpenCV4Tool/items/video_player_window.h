#ifndef VIDEO_PLAYER_WINDOW_H
#define VIDEO_PLAYER_WINDOW_H

#include <QWidget>
#include <QPixmap>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QMimeData>
#include <QFileInfo>
#include <QMessageBox>
#include <QResizeEvent>
#include <QModelIndex>
#include <QIcon>
#include "../common/utils/ImageUtils.h"
#include "../common/CommonListView.h"
#include "../common/CommonListViewItem.h"
//#include "video_player/video_player_opengl_show_image.h"
#include "video_player/video_player_roate_flip.h"
#include "video_player/video_player_splicing_image.h"

class Video_Player_Window : public QWidget
{
    Q_OBJECT
public:
    explicit Video_Player_Window(QWidget *parent = nullptr);
    void createListView();//创建一个ListView

private:
    CommonListView * listView;
    //Video_Player_OpenGL_Show_Image videoPlayerOpenGLShowImage;
    Video_Player_Roate_Flip videoPlayerRoateFlip;
    Video_Player_Splicing_Image videoPlayerSplicingImage;

signals:

};

#endif // VIDEO_PLAYER_WINDOW_H
