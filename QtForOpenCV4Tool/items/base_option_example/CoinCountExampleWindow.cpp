#include "CoinCountExampleWindow.h"

CoinCountExampleWindow::CoinCountExampleWindow(QWidget* parent)
	: BaseSceneView(parent)
{
	this->setWindowTitle("检测硬币个数");
	this->setFixedSize(1000, 480);
	this->setGraphicsViewSize(1000, 480);

	//选择图片
	ChoiceImageWidget* choiceImageWidget = new ChoiceImageWidget(this);
	choiceImageWidget->setFixedWidth(200);
	choiceImageWidget->setCallback([=](QString filePath) {
		this->filePath = filePath;
		qDebug() << "选择图片完成开始打印路径：" << filePath;
		//指针测试
		int a = 0;
		int  b = 11;
		int* p = &a;//把a的地址赋值给p

		*p = 100;//给变量p赋值
		qDebug() << "指针测试：a=" << a << ",p=" << *p;

		p = &b;
		*p = 22;
		qDebug() << "指针测试：a=" << a << ",b=" << b << ",p=" << *p;

		//字符串copy
		char dest[20] = "123456789";
		char src[] = "hello world";
		strcpy(dest, src);
		qDebug() << "dest:" << dest;
		this->execute();
		});

	QHBoxLayout* hLayout = new QHBoxLayout(this);
	QVBoxLayout* vLayout = new QVBoxLayout(this);


	vLayout->addWidget(choiceImageWidget);
	//vLayout->addWidget(minusPlusWidget);
	vLayout->setAlignment(Qt::AlignTop);

	QVBoxLayout* vLayout2 = new QVBoxLayout(this);
	gridView = new VariableGridView(this);
	gridView->setStyleSheet("background-color:#000000");
	gridView->resize(QSize(this->width() / 5 * 4, this->height()));
	gridView->setViews(8, 1);

	vLayout2->addWidget(gridView);
	hLayout->addLayout(vLayout, 1);
	hLayout->addLayout(vLayout2, 3);

	

}

//耗时方法(高斯不同演示)
QPixmap CoinCountExampleWindow::handle() {
	//vector<QPixmap> pixmaps;
	//Mat src = imread(filePath.toStdString().c_str());
	//pixmaps.push_back(ImageUtils::getPixmap(src.clone()));//[1]
	//blur(src, src, Size(5, 5), Point(-1, -1));//均值模糊去除一些噪声
	//pixmaps.push_back(ImageUtils::getPixmap(src.clone()));//[2]
	//Mat gray;
	//cvtColor(src, gray, COLOR_BGR2GRAY);//转为灰度图像
	//pixmaps.push_back(ImageUtils::getPixmap8(gray));//[3]
	////对图像进行二值分割,使用自动预值的方法
	//threshold(gray, gray, 0, 255, THRESH_BINARY_INV | THRESH_OTSU);
	//pixmaps.push_back(ImageUtils::getPixmap8(gray));//[4]
	////执行形态学开操作消除图像中的造点
	//Mat kernel = getStructuringElement(MORPH_RECT, Size(3, 3));
	//morphologyEx(gray, gray, MORPH_OPEN, kernel, Point(-1, -1), 2);//最后一个参数：1代表连续开操作两次
	//pixmaps.push_back(ImageUtils::getPixmap8(gray));//[5]
	////进行距离变换
	//Mat distanceImage = Mat(gray.size(), CV_32FC1);
	//distanceTransform(gray, distanceImage, DIST_L2, 5);
	//imshow("distanceImage", distanceImage);
	//normalize(distanceImage, distanceImage, 0, 1, NORM_MINMAX);
	//imshow("distanceImage1", distanceImage);
	//threshold(distanceImage, distanceImage, 0, 1, THRESH_BINARY);
	//imshow("distanceImage2", distanceImage);
	////边缘检测
	//normalize(distanceImage, distanceImage, 0, 255, NORM_MINMAX);
	//imshow("distanceImage3", distanceImage);
	//pixmaps.push_back(ImageUtils::getPixmap8(distanceImage));//[6]
	//Mat mat8;
	//distanceImage.convertTo(mat8, CV_8UC1);
	//Canny(mat8, mat8, 0, 255);
	//pixmaps.push_back(ImageUtils::getPixmap8(mat8));//[7]
	////执行轮廓发现
	//vector<vector<Point>> contours;
	//vector<Vec4i> hierarchy;//拓扑结构
	//Mat myResult = Mat::zeros(mat8.size(), CV_8UC3);
	//findContours(mat8, contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);
	//RNG rng(12345);
	//for (size_t i = 0;i < contours.size();i++) {
	//	drawContours(myResult, contours, i, Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255)), -1);
	//}
	//pixmaps.push_back(ImageUtils::getPixmap(myResult));//[8]


	////轮廓计数
	//qDebug() << "轮廓个数：" << contours.size();
	////label->setPixmap(pixmap.scaled(label->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
	//gridView->setAdapter(pixmaps);

	Mat src, distanceImage,gray;

	src = imread(filePath.toStdString().c_str());
	imshow("src", src);
	blur(src, src, Size(3, 3), Point(-1, -1));//均值模糊去除一些噪声
	cvtColor(src, gray, COLOR_BGR2GRAY);//转为灰度图像
	//对图像进行二值分割,使用自动预值的方法
	threshold(gray, gray, 0, 255, THRESH_BINARY_INV | THRESH_OTSU);
	imshow("threshold", gray);
	//执行形态学开操作消除图像中的造点
	Mat kernel = getStructuringElement(MORPH_RECT, Size(3, 3));
	morphologyEx(gray, gray, MORPH_OPEN, kernel, Point(-1, -1), 2);//2代表连续开操作两次
	imshow("morphologyEx", gray);
	//    dilate(gray,gray,kernel,Point(-1,-1),3);
	//进行距离变换
	distanceImage = Mat(gray.size(), CV_32FC1);
	distanceTransform(gray, distanceImage, DIST_L2, 5);
	normalize(distanceImage, distanceImage, 0, 1, NORM_MINMAX);
	imshow("distanceTransForm", distanceImage);
	//局部二值化
	//    normalize(distanceImage,distanceImage,0,255,NORM_MINMAX);
	//    adaptiveThreshold(MorphImg, MorphImg, 255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY, 85, 0.0);//使
	threshold(distanceImage, distanceImage, 0.6, 1, THRESH_BINARY);
	imshow("thre", distanceImage);
	//边缘检测
	normalize(distanceImage, distanceImage, 0, 255, NORM_MINMAX);
	Mat mat8;
	distanceImage.convertTo(mat8, CV_8UC1);
	imshow("result1", mat8);
	Canny(mat8, mat8, 0, 255);

	//执行轮廓发现
	vector<vector<Point>> contours;
	vector<Vec4i> hierarchy;//拓扑结构
	Mat myResult = Mat::zeros(mat8.size(), CV_8UC3);
	findContours(mat8, contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);
	RNG rng(12345);
	for (size_t i = 0;i < contours.size();i++) {
		drawContours(myResult, contours, i, Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255)), -1);
	}
	//轮廓计数
	cout << "number of:" << contours.size() << endl;
	imshow("result", myResult);
	cv::waitKey(0);
	return NULL;
}

CoinCountExampleWindow::~CoinCountExampleWindow()
{
}
