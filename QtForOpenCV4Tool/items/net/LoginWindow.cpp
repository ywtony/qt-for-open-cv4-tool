#include "LoginWindow.h"

LoginWindow::LoginWindow(QWidget* parent)
	: QWidget(parent)
{
	httpUtils = new HttpUtils(this);
	this->setWindowTitle("登录");
	etUserName = new EditText(this);
	etUserName->setPlaceholderText("请输入用户名");
	etPassword = new EditText(this);
	etPassword->setPlaceholderText("请输入密码");
	btnLogin = new Button(this);
	btnLogin->setText("登录");

	QFormLayout* formLayout = new QFormLayout(this);
	formLayout->addRow(tr("用户名："), etUserName);
	formLayout->addRow(tr("密   码："), etPassword);
	formLayout->addRow(btnLogin);
	//formLayout->setMargin(50);
	formLayout->setVerticalSpacing(20);
	formLayout->setContentsMargins(50, 50, 50, 200);
	this->setLayout(formLayout);
	connect(btnLogin, &Button::clicked, [=]() {
		const QString username = etUserName->text();
		const QString password = etPassword->text();
		if (username.isEmpty()) {
			QMessageBox::warning(this, "温馨提示", "用户名不能为空");
		}
		else if (password.isEmpty()) {
			QMessageBox::warning(this, "温馨提示", "用户密码不能为空");
		}
		else {
			login(QString(username), QString(password));
		}


		});
}

void LoginWindow::login(QString username, QString password) {
	/*QMap<QString, QString> param;
	param.insert("username", username);
	param.insert("password", password);*/
	QMap<QString, QString> headers;
	headers.insert("Authorization", "");
	headers.insert("appVersion", "1.0.0");
	headers.insert("apiVersion", "1.0");
	headers.insert("channel", "android");
	headers.insert("deviceType", "2");
	headers.insert("deviceCode", "8f6f465f-e91d-4a97-8f4e-e6a410e4f0e7_android_1702617469162");
	headers.insert("deviceName", "LNA-AL00");
	headers.insert("content-type", "application/json; charset=UTF-8");
	/*httpUtils->post("http://192.168.100.12:8384/aic-user/sso/login", param, headers,
		[=](int statusCode, int code, QString msg, QJsonObject result) {
			if (code == 200) {
				QMessageBox::critical(this, "提示", "登录成功");
				getUserInfo();
			}
			else {
				QMessageBox::critical(this, "提示", "登录失败");
			}
		});*/

		/*HttpClient("http://192.168.100.12:8384/aic-user/sso/login").success([=](const QString& response) {
			QMessageBox::information(this, "响应", response);
			}).param("username", username).param("password", password).headers(headers).post();*/
	QJsonObject jsonObject;
	jsonObject.insert("username", username);
	jsonObject.insert("password", password);
	HttpClient("http://192.168.100.12:8384/aic-user/sso/login").success([=](const QString& response) {
		QMessageBox::information(this, "响应", response);
		}).json(JsonUtils().toJsonString(jsonObject)).post();
}

//根据token获取用户信息
void LoginWindow::getUserInfo() {
	QMap<QString, QString> headers;
	headers.insert("Authorization", "Bearer " + CacheUtils::getInstance().getToken());
	headers.insert("appVersion", "1.0.0");
	headers.insert("apiVersion", "1.0");
	headers.insert("channel", "android");
	headers.insert("deviceType", "2");
	headers.insert("deviceCode", "8f6f465f-e91d-4a97-8f4e-e6a410e4f0e7_android_1702617469162");
	headers.insert("deviceName", "LNA-AL00");
	httpUtils->get("http://192.168.100.12:8384/aic-user/member/detail", headers,
		[=](int statusCode, int code, QString msg, QJsonObject result) {
			if (code == 200) {
				QMessageBox::critical(this, "提示", "获取用户信息成功");
			}
			else {
				QMessageBox::critical(this, "提示", "获取用户信息失败");
			}
		});
}

LoginWindow::~LoginWindow()
{

}
