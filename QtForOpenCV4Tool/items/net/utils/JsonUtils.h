#pragma once
#include <QJsonObject>
#include <QJsonDocument>
#include <QByteArray>
#include <QString>
#include <QDebug>


class JsonUtils
{

public:
	JsonUtils();
	~JsonUtils();
	QString toJsonString(QJsonObject jsonObj);
	QJsonObject* getObject(QString json);
};
