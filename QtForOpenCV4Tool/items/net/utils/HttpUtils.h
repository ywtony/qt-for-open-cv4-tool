#pragma once

#include <QObject>
#include <QJsonObject>
#include <QJsonDocument>
#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QEventLoop>
#include <QString>
#include <QMap>
#include <functional>
#include "CacheUtils.h"


//Qt的Http请求的工具类
class HttpUtils : public QObject
{
	Q_OBJECT

public:
	HttpUtils(QObject *parent);
	~HttpUtils();
public:
	void post(QString url,QMap<QString,QString> param, QMap<QString, QString> headers, std::function<void(int, int, QString, QJsonObject)> onResponseResult);
	void get(QString url,  QMap<QString, QString> headers, std::function<void(int, int, QString, QJsonObject)> onResponseResult);
};
