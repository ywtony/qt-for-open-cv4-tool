#include "JsonUtils.h"

JsonUtils::JsonUtils()
{

}

QString JsonUtils::toJsonString(QJsonObject jsonObj) {
	// 将 QJsonObject 对象转换为 JSON 文档
	QJsonDocument doc(jsonObj);
	// 将 JSON 文档转换为字符串
	QByteArray byteArr = doc.toJson();
	QString str = QString::fromUtf8(byteArr);
	return str;
}

QJsonObject* JsonUtils::getObject(QString json) {
	// 创建QJsonDocument对象并加载JSON数据
	QJsonDocument doc(QJsonDocument::fromJson(json.toUtf8()));
	if (!doc.isNull()) {
		// 获取根节点（QJsonObject）
		QJsonObject rootObj = doc.object();
		// 输出结果
		qDebug() << "Name:" << rootObj["name"].toString();
		qDebug() << "Age:" << rootObj["age"].toInt();
		return &rootObj;
	}
	else {
		qWarning() << "Invalid JSON data.";
		return nullptr;
	}
}

JsonUtils::~JsonUtils()
{

}
