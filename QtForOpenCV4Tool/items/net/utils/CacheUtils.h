#pragma once

#include <QObject>
#include <QSettings>
#include <QString>

class CacheUtils : public QObject
{
	Q_OBJECT

public:
	CacheUtils();
	static CacheUtils& getInstance() {
		static CacheUtils instance;
		return instance;
	};
	~CacheUtils();
	void initSettings();
	void setToken(QString token);
	QString getToken();
	void removeToken();
private:
	QSettings *settings;
};
