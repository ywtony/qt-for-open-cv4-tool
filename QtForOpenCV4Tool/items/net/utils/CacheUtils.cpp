#include "CacheUtils.h"

CacheUtils::CacheUtils(){
	initSettings();
}

void CacheUtils::initSettings() {
	//QSettings构造函数的第一个参数是ini文件的路径,第二个参数表示针对ini文件,第三个参数可以缺省
	settings = new QSettings("config.ini", QSettings::IniFormat);
}

void CacheUtils::setToken(QString token) {
	settings->setValue("token", token);
	//写入完成后删除指针
	//delete settings;
}
QString CacheUtils::getToken() {
	return settings->value("token").toString();
}

void CacheUtils::removeToken() {
	settings->remove("token");
}

CacheUtils::~CacheUtils()
{
}
