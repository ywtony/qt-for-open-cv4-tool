#pragma once

#include <QWidget>
#include "../../common/edittext/EditText.h"
#include "../../common/button/Button.h"
#include <QMessageBox>
#include "utils/HttpUtils.h"
#include <QFormLayout>
#include "utils/HttpClient.h"
#include "utils/JsonUtils.h"

class LoginWindow : public QWidget
{
	Q_OBJECT

public:
	LoginWindow(QWidget *parent = nullptr);
	~LoginWindow();
	void login(QString username, QString password);
	void getUserInfo();
	

private:
	EditText* etUserName;
	EditText* etPassword;
	Button* btnLogin;
	HttpUtils *httpUtils;
};
