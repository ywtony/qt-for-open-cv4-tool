#include "features_detection_window.h"

Features_Detection_Window::Features_Detection_Window(QWidget* parent)
	: QWidget(parent)
{
	this->setFixedSize(QSize(320, 480));
	createListView();
}

void Features_Detection_Window::createListView() {
	listView = new CommonListView(this);
	connect(listView, &CommonListView::itemClicked, [=](QListWidgetItem* item) {
		CommonListViewItem* item2 = dynamic_cast<CommonListViewItem*>(item);
		qDebug() << "itemPos:" << item2->mPos;
		switch (item2->mPos) {
		case 0:
			cornerHarrisWindow.show();
			break;
		case 1:
			goodFeaturesToTrack.show();
			break;
		case 2:
			surfWindow.show();
			break;
		case 3://hog特征检测-->检测行人
			hogFeatureTestingWindow.show();
			break;
		case 4:
			integralGraphicsWindow.show();
			break;
		case 5://cv::ORB图像匹配
			oRBImageMatchWindow.show();
			break;
		case 6:
			planeDetectionWindow.show();
			break;
		}
		});
	new CommonListViewItem("角点检测", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 0, listView);
	new CommonListViewItem("更好的角点检测", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 1, listView);
	new CommonListViewItem("SURF特征检测", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 2, listView);
	new CommonListViewItem("HOG特征检测->行人", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 3, listView);
	new CommonListViewItem("积分图", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 4, listView);
	new CommonListViewItem("匹配仓库图（cv::ORB）", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 5, listView);
	new CommonListViewItem("平面检测", QIcon("images/tezhengjiance.png"), QIcon("images/tezhengjiance.png"), 6, listView);
}

