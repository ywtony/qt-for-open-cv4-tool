#ifndef VIDEO_ANALYSIS_AND_OBJECT_TRACK_WINDOW_H
#define VIDEO_ANALYSIS_AND_OBJECT_TRACK_WINDOW_H

#include <QWidget>
#include <QPixmap>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QMimeData>
#include <QFileInfo>
#include <QMessageBox>
#include <QResizeEvent>
#include <QModelIndex>
#include "../common/utils/ImageUtils.h"
#include "../common/CommonListView.h"
#include "../common/CommonListViewItem.h"
#include <QIcon>
#include "video_analysis_and_object_track/mog_video_background_remove.h"
#include "video_analysis_and_object_track/color_based_object_tracking.h"
#include "video_analysis_and_object_track/klt_object_tracking.h"
#include "video_analysis_and_object_track/hf_object_tracking.h"
#include "video_analysis_and_object_track/cam_shift_video_object_tracking.h"
#include "video_analysis_and_object_track/color_separate_in_range.h"
#include "video_analysis_and_object_track/move_video_object_tracking.h"
#include "video_analysis_and_object_track/extend_kcf_single_object_tracking.h"
#include "video_analysis_and_object_track/extend_kcf_multi_objects_tracking.h"

/**
 * 视频分析与对象跟踪
 * @brief The Video_Analysis_And_Object_Track_Window class
 */
class Video_Analysis_And_Object_Track_Window : public QWidget
{
    Q_OBJECT
public:
    explicit Video_Analysis_And_Object_Track_Window(QWidget *parent = nullptr);
    void createListView();//创建一个ListView

private:
    CommonListView * listView;
    MOG_Video_Background_Remove mogVideoBackgroundRemove;
    Color_Based_Object_Tracking colorBasedObjectTracking;
    KLT_Object_Tracking kltObjectTracking;
    HF_Object_Tracking hfObjectTracking;
    CAM_Shift_Video_Object_Tracking camShiftVideoObjectTracking;
    Color_Separate_In_Range colorSpearateInRang;
    Move_Video_Object_Tracking moveVideoObjectTracking;
    Extend_KCF_Single_Object_Tracking extendKCFSingleObjectTracking;
    Extend_KCF_Multi_Objects_Tracking extendKCFMulitObjectsTracking;
signals:

};

#endif // VIDEO_ANALYSIS_AND_OBJECT_TRACK_WINDOW_H
