#pragma once
#include<windows.h>
#include <QtWidgets/QMainWindow>
#include "ui_QtForOpenCV4Tool.h"
#include "common/CommonListView.h"
#include "common/CommonListView.h"
#include <QSize>
#include <QIcon>
#include "items/features_detection_window.h"
#include "items/image_segmentation_window.h"
#include "items/haar_lbp_window.h"
#include "items/video_analysis_and_object_track_window.h"
#include "items/ai_window.h"
#include "items/face_recognizer_window.h"
#include "items/video_player_window.h"
#include "items/BaseOptionWindow.h"
#include "items/custom/CustomExampleListWindow.h"
#include "items/ImageBasicOptionExampleWindow.h"
#include "items/NetworkWindow.h"
#include "items/compress/ImageCompressTool.h"
#include "items/OpenGLSampleWindow.h"
#include "items/FFMpegWindow.h"

#include <QMetaType> 
/**
* 主窗口
*/
class QtForOpenCV4Tool : public QMainWindow
{
    Q_OBJECT

public:
    QtForOpenCV4Tool(QWidget *parent = nullptr);
    ~QtForOpenCV4Tool();
public:
    void createListView();//创建一个ListView

private:
    Ui::QtForOpenCV4ToolClass ui;
    CommonListView *listView;
    BaseOptionWindow baseOptionWindow;//基础操作
    Features_Detection_Window featuresDetectionWindow;
    Image_Segmentation_Window imageSegmentationWindow;
    Haar_LBP_Window harrLbpWindow;
    Video_Analysis_And_Object_Track_Window videoAnalysisAndObjectTrackWindow;
    AI_Window aiWindow;
    Face_Recognizer_Window faceRecognizer;
    Video_Player_Window videoPlayerWindow;
    //自定义View
    CustomExampleListWindow customExampleListWindow;
    //图片基本操作案例
    ImageBasicOptionExampleWindow imageBasicOptionExampleWindow;
    //Qt网络相关
    NetworkWindow networkWindow;
    //图片压缩工具
    ImageCompressTool imageCompressTool;
    //OpenGL测试案例
    OpenGLSampleWindow openGLSampleWindow;
    //FFMPEG案例
    FFMpegWindow fFMpegWindow;

};
