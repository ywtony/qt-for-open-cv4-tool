#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;
uniform float mX;
uniform float mY;
uniform float mXT;
out vec3 ourColor;
void main(void)
{
	gl_Position = vec4(aPos.x*mX+mXT,aPos.y*mY,aPos.z,1.0f);
	ourColor = aColor;
}