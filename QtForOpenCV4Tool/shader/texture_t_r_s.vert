#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 aTexCoord;
out vec2 ourTexCoord;
uniform mat4 uMatrix;
void main(void)
{
	gl_Position = uMatrix*vec4(aPos.x,aPos.y,aPos.z,1.0f);
	ourTexCoord = aTexCoord;
}
