#version 330 core
uniform vec4 ourColor;
void main(void)
{
    gl_FragColor = ourColor;
}
