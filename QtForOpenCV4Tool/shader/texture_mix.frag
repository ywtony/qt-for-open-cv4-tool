#version 330 core
out vec4 FragColor;
in vec2 ourTexCoord;
uniform float maxScale;
uniform float flip_x;
uniform sampler2D ourTexture1;
uniform sampler2D ourTexture2;
void main()
{
	FragColor = mix(texture(ourTexture1,ourTexCoord),texture(ourTexture2,vec2(ourTexCoord.x*flip_x,ourTexCoord.y)),maxScale);
}