#version 300 es
precision highp float;

uniform sampler2D uni_textureY;
uniform sampler2D uni_textureU;
uniform sampler2D uni_textureV;

in vec2 out_uv;
out vec4 fragColor;

void main(void)
{
    vec3 yuv;
    vec3 rgb;
    yuv.x = texture(uni_textureY, out_uv).r;
    //这里和标清、高清、超高清的yuv转rgb的公式有关系，0.5（0~1）=128（0~255）
    // 因为UV的默认值是127，所以我们这里减去0.5（OpenGL ES的Shader中会把内存中的0~255的整数数值换算为0.0~1.0的浮点数）
    yuv.y = texture(uni_textureU, out_uv).r - 0.5;
    yuv.z = texture(uni_textureV, out_uv).r - 0.5;
    rgb = mat3( 1.0,1.0,1.0, 0.0,-0.39465,2.03211,1.13983,-0.58060,0) * yuv;
    fragColor = vec4(rgb, 1.0);
}
