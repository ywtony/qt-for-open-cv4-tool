#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 aTexCoord;
out vec2 ourTexCoord;
uniform mat4 uModel;
uniform mat4 uView;
uniform mat4 uProjection;
void main(void)
{
	gl_Position = uProjection*uView*uModel*vec4(aPos.x,aPos.y,aPos.z,1.0f);
	ourTexCoord = aTexCoord;
}
