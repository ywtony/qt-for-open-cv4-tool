#include "CustomWidgetWindow.h"
#include "../common/edittext/EditText.h"
#include "../common/radiobutton/RadioButton.h"
#include "../common/scroll/ScrollView.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include "../common/checkbox/CheckBox.h"
#include "../common/combobox/ComboBox.h"

CustomWidgetWindow::CustomWidgetWindow(QWidget* parent)
	: QWidget(parent)
{
	this->setWindowTitle("自定义组件展示");
	this->setFixedSize(320, 480);

	ScrollView* scrollView = new ScrollView(this);
	scrollView->resize(320, 480);
	//scrollView->setRange(0, 100);//设置滚动条的最小值和最大值
	//scrollView->setValue(50);//设置滚动条的当前值
	scrollView->setOrientation(Qt::Vertical);//设置竖向滚动调Qt::Horizontal
	connect(scrollView, &ScrollView::valueChanged, [=](int value) {
		qDebug() << "Value changed to:" << value;
		});

	/*scrollView->setMinimum(0);
	scrollView->setMaximum(100);*/

	

	Button* btn = new Button();
	btn->move(0, 5);
	btn->setText("点击按钮应用样式");

	//给按钮加上图标
	Button* btn2 = new Button();
	btn2->setCustomStyleSheet(StyleConfig::COMMON_BUTTON_SHAPE_BLACK_R10());
	btn2->setText("第二个按钮");
	btn2->setIcon(QIcon("images/ai_suanfa.png"));//给按钮设置图标
	btn2->move(0, btn->y() + btn->height() + 5);

	//给按钮加上背景图片
	Button* btn3 = new Button();
	btn3->resize(100, 36);
	btn3->setCustomStyleSheet(StyleConfig::COMMON_BUTTON_IMAGE_BG());
	btn3->move(5, btn2->y() + btn2->height() + 5);


	//普通文本框通用样式
	EditText* et = new EditText();
	et->move(5, btn3->y() + btn3->height() + 5);
	et->setPlaceholderText("请输入用户名");
	et->resize(200, 36);

	//设置只允许输入数字
	EditText* et2 = new EditText();
	et2->move(5, et->y() + et->height() + 5);
	et2->setPlaceholderText("请输入手机号");
	et2->resize(200, 36);
	et2->setMaxLength(11);//限制输入框可输入的最大长度，其输入内容为长度-1，以此输入框为例，只能输入11个数字
	QRegExp regExp("[0-9]*"); // 只允许输入数字
	et2->setValidator(new QRegExpValidator(regExp));

	//设置密码框模式
	EditText* et3 = new EditText();
	et3->move(5, et2->y() + et2->height() + 5);
	et3->setPlaceholderText("请输入密码");
	et3->resize(200, 36);
	et3->setMaxLength(16);
	et3->setEchoMode(EditText::Password);//设置输入框为密码模式

	//RadioButton
	RadioButton* rb = new RadioButton();
	rb->move(5, et3->y() + et3->height() + 5);

	//CheckBox
	CheckBox* cb = new CheckBox();

	//ComboBox
	ComboBox* cbo = new ComboBox();
	cbo->addItem(QIcon("images/icon_checked.png"), "德玛西亚");
	cbo->addItem(QIcon("images/icon_checked.png"), "无极剑圣");
	cbo->addItem(QIcon("images/icon_checked.png"), "嘉文四世");
	cbo->addItem(QIcon("images/icon_checked.png"), "恶魔天使");
	cbo->addItem(QIcon("images/icon_checked.png"), "寡妇");

	QVBoxLayout* vLayout = new QVBoxLayout();
	vLayout->addWidget(btn);
	vLayout->addWidget(btn2);
	vLayout->addWidget(btn3);
	vLayout->addWidget(et);
	vLayout->addWidget(et2);
	vLayout->addWidget(et3);
	vLayout->addWidget(rb);
	vLayout->addWidget(cb);
	vLayout->addWidget(cbo);
	scrollView->setLayout(vLayout);
}

CustomWidgetWindow::~CustomWidgetWindow()
{
}
