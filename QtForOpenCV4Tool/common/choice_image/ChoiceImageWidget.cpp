#include "ChoiceImageWidget.h"

ChoiceImageWidget::ChoiceImageWidget(QWidget* parent)
	: QWidget(parent)
{
	QHBoxLayout* hLayout = new QHBoxLayout(this);
	EditText* et = new EditText(this);
	et->setEnabled(false);
	et->setFixedHeight(30);
	Button* btnChoiceBtn = new Button(this);
	btnChoiceBtn->setText("请选择图片");
	hLayout->addWidget(et);
	hLayout->addWidget(btnChoiceBtn);
	hLayout->setAlignment(Qt::AlignTop);
	this->setLayout(hLayout);

	connect(btnChoiceBtn, &Button::clicked, this, [=]() {
		QString filePath = QFileDialog::getOpenFileName(this, tr("请选择图片"), "C:/Users/DBF-DEV-103/Downloads/", tr("Image Files(*.jpg *.png *.webp *.jpeg)"));
		et->setText(filePath);
		func(filePath);

		});
}
void ChoiceImageWidget::setCallback(std::function<void(QString)> func) {
	this->func = func;
}
ChoiceImageWidget::~ChoiceImageWidget()
{
}
