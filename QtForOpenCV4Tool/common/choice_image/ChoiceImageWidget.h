#pragma once

#include <QWidget>
#include "../../common/button/Button.h"
#include "../../common/edittext/EditText.h"
#include "../../common/seekbar/Seekbar.h"
#include <QFileDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QVBoxLayout>
#include <iostream>
#include <QFileDialog>
#include <functional>

class ChoiceImageWidget : public QWidget
{
	Q_OBJECT

public:
	ChoiceImageWidget(QWidget *parent = nullptr);
	~ChoiceImageWidget();
public:
	void setCallback(std::function<void(QString)> func);

private:
	std::function<void(QString)> func;
};
