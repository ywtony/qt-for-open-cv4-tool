#include "ScrollView.h"

ScrollView::ScrollView(QWidget *parent)
	: QScrollBar(parent)
{
	//这里设置一个默认的样式
	QString css = StyleHelper::getStyleSheet(StyleConfig::COMMON_SCROLLVIEW_DEFAULT());
	this->setStyleSheet(css);
}

ScrollView::~ScrollView()
{

}
