#pragma once

#include <QLineEdit>
#include "../utils/StyleConfig.h"
#include "../utils/StyleHelper.h"
#include <QIntValidator>

class EditText : public QLineEdit
{
	Q_OBJECT

public:
	EditText(QWidget *parent = nullptr);
	~EditText();

private:
};
