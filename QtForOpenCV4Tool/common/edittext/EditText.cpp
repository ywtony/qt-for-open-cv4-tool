#include "EditText.h"

EditText::EditText(QWidget *parent)
	: QLineEdit(parent)
{
	//这里设置一个默认的样式
	QString css = StyleHelper::getStyleSheet(StyleConfig::COMMON_EDITTEXT_DEFAULT());
	this->setStyleSheet(css);
}

EditText::~EditText()
{
}
