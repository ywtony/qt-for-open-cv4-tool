#include "RadioButton.h"

RadioButton::RadioButton(QWidget *parent)
	: QRadioButton(parent)
{
	//这里设置一个默认的样式
	QString css = StyleHelper::getStyleSheet(StyleConfig::COMMON_RADIOBUTTON_DEFAULT());
	this->setStyleSheet(css);
}

RadioButton::~RadioButton()
{

}
