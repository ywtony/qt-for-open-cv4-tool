#pragma once

#include <QRadioButton>
#include "../utils/StyleConfig.h"
#include "../utils/StyleHelper.h"

class RadioButton : public QRadioButton
{
	Q_OBJECT

public:
	RadioButton(QWidget *parent = nullptr);
	~RadioButton();

private:
};
