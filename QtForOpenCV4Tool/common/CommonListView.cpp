#include "CommonListView.h"

CommonListView::CommonListView(QWidget* parent)
	: QListWidget(parent)
{
	//this->onItemClicked = onItemClicked;
	setMouseTracking(true);
	setEnabled(true);
	setViewMode(QListView::ListMode);
	setFocusPolicy(Qt::NoFocus);  //这样可禁用tab键和上下方向键并且除去复选框
	setFont(QFont("宋体", 14, QFont::DemiBold));
	setFixedSize(QSize(parent->width(), parent->height()));
	//item点击事件
	//connect(this, &CommonListView::itemClicked, this,&CommonListView::onItemClicked);
	//连接信号与槽函数，如果选中项发生变化则触发item图标的更新.都是当前对象发生
	connect(this, &CommonListView::itemSelectionChanged, this, &CommonListView::updateSelectedIcon);
	/*connect(this, &CommonListView::itemClicked, [=](QListWidgetItem* item) {
		Utils::log("触发了ItemClicked的函数");
		});*/

	setStyleSheet(
		"{outline:0px;}"  //除去复选框
		"CommonListViewItem{background:rgb(245, 245, 247); border:0px; margin:0px 0px 0px 0px;}"
		"CommonListViewItem::Item{height:40px; border:0px; padding-left:14px; color:rgba(200, 40, 40, 255);}"
		"CommonListViewItem::Item:hover{color:rgba(40, 40, 200, 255);}"
		"CommonListViewItem::Item:selected{background:rgb(230, 231, 234); color:rgba(40, 40, 200, 255); border-left:4px solid rgb(180, 0, 0);}"
		"QListWidget::Item:selected:active{background:rgb(230, 231, 234); color:rgba(40, 40, 200, 255); border-left:4px solid rgb(180, 0, 0);}");
}

void CommonListView::updateSelectedIcon() {
	oldSelectedItem = theSelectedItem;
	theSelectedItem = static_cast<CommonListViewItem*>(currentItem());

	//之前被选中的item图标回复原样
	//新被选中的item图标变成hover状态
	if (oldSelectedItem != theSelectedItem) {
		if (oldSelectedItem) oldSelectedItem->setIcon(oldSelectedItem->mIcon);
		if (theSelectedItem) theSelectedItem->setIcon(theSelectedItem->mIconHover);
	}
}


//这个槽函数并没有执行
//void CommonListView::onItemClicked(QListWidgetItem* item) {
//	Utils::log("触发了ListView Item的点击事件");
//	Utils::log("ListView item 的点击事件");
//}
CommonListView::~CommonListView()
{
}
