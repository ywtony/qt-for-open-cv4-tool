#pragma once

#include <QWidget>
#include "../../common/button/Button.h"
#include "../../common/edittext/EditText.h"
#include <QFileDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QVBoxLayout>
#include <iostream>
#include <functional>

//�Ӽ��ؼ�
class MinusPlusWidget : public QWidget
{
	Q_OBJECT

public:
	MinusPlusWidget(QWidget* parent = nullptr);
	~MinusPlusWidget();

public:
	void setMinusClick(std::function<void(int)> minusClick);
	void setPlusClick(std::function<void(int)> plusClick);
	void setTipsContent(QString tips);
	void setMinusAndPlus(QString tips,std::function<void(int)> minusClick, std::function<void(int)> plusClick);
private:
	EditText* etKsize;
	int kSize=3;
	QLabel* labelTips;
	std::function<void(int)> minusClick;
	std::function<void(int)> plusClick;

};
