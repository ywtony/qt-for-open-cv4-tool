#include "MinusPlusWidget.h"

MinusPlusWidget::MinusPlusWidget(QWidget* parent)
	: QWidget(parent)
{
	this->setFixedSize(180, 60);
	QVBoxLayout* vLayout = new QVBoxLayout(this);
	labelTips = new QLabel(this);
	//以下是加减组件的内容
	QHBoxLayout* hLayout = new QHBoxLayout(this);
	Button* btnMinus = new Button(this);
	btnMinus->setFixedSize(50, 30);
	btnMinus->setText("-");
	etKsize = new EditText(this);
	etKsize->setFixedSize(50, 30);
	etKsize->setText(QString::number(kSize));
	Button* btnPlus = new Button(this);
	btnPlus->setFixedSize(50, 30);
	btnPlus->setText("+");
	hLayout->addWidget(btnMinus, 1);
	hLayout->addWidget(etKsize, 1);
	hLayout->addWidget(btnPlus, 1);
	vLayout->addWidget(labelTips);
	vLayout->addLayout(hLayout);
	this->setLayout(vLayout);

	connect(btnMinus, &Button::clicked, this, [=]() {
		kSize -= 2;
		if (kSize <= 1) {
			kSize = 1;
		}
		this->minusClick(kSize);
		etKsize->setText(QString::number(kSize));

		});
	connect(btnPlus, &Button::clicked, this, [=]() {
		kSize += 2;
		this->plusClick(kSize);
		etKsize->setText(QString::number(kSize));

		});
}

void MinusPlusWidget::setMinusClick(std::function<void(int)> minusClick) {
	this->minusClick = minusClick;
}

void MinusPlusWidget::setPlusClick(std::function<void(int)> plusClick) {
	this->plusClick = plusClick;
}
void MinusPlusWidget::setTipsContent(QString tips) {
	this->labelTips->setText(tips);
}

void MinusPlusWidget::setMinusAndPlus(QString tips, std::function<void(int)> minusClick, std::function<void(int)> plusClick) {
	this->minusClick = minusClick;
	this->plusClick = plusClick;
	this->labelTips->setText(tips);
}
MinusPlusWidget::~MinusPlusWidget()
{
}
