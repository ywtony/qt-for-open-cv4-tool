#pragma once

#include <QWidget>
#include <QGraphicsScene>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QResizeEvent>
#include <QSize>
#include <QGraphicsView>
#include <QMessageBox>
#include <QMimeData>
#include <QFileInfo>
#include <QStringList>
#include <QGraphicsPixmapItem>
#include <QDebug>
#include <QGraphicsEllipseItem>
#include <QGraphicsLineItem>
#include <QGraphicsRectItem>
#include <QGraphicsPolygonItem>
#include <QSlider>
#include <QDebug>
#include "../items/feature_detection/CornerHarris.h"
#include "utils/ImageUtils.h"

class CommonGraphicsView : public QWidget
{
	Q_OBJECT

public:
	CommonGraphicsView(QWidget *parent = nullptr);
	~CommonGraphicsView();
	QGraphicsScene scene;
	CornerHarris* cornerHarrs;
	QString filePath;
	Mat resultImage;

private:
	QGraphicsView* view;

protected:
	void dragEnterEvent(QDragEnterEvent* event);
	void dropEvent(QDropEvent* event);
	void resizeEvent(QResizeEvent* event);

public slots:
	void onHSliderValueChanged(int value);
};
