#include "CheckBox.h"

CheckBox::CheckBox(QWidget *parent)
	: QCheckBox(parent)
{
	//这里设置一个默认的样式
	QString css = StyleHelper::getStyleSheet(StyleConfig::COMMON_EDITTEXT_DEFAULT());
	this->setStyleSheet(css);
}

CheckBox::~CheckBox()
{
}
