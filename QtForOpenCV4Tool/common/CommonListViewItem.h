#pragma once

#include <QListWidgetItem>
#include <QIcon>
#include <QString>
#include <qlistwidget.h>


class CommonListViewItem : public QListWidgetItem
{
	//Q_OBJECT

public:
	CommonListViewItem(QString itemTitle, const QIcon& icon, const QIcon& iconHover, int pos, QListWidget* parent = nullptr);
	~CommonListViewItem();
//protected:
//	void itemClicked(QListWidgetItem* item);
public:
	QIcon mIcon;//默认图标
	QIcon mIconHover;//选中时图标
	int mPos;//第一个编辑

};
