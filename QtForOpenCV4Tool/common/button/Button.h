#pragma once

#include <QPushButton>
#include "../utils/StyleConfig.h"
#include "../utils/StyleHelper.h"

class Button : public QPushButton
{
	Q_OBJECT

public:
	Button(QWidget *parent = nullptr);
	~Button();
public:
	void setCustomStyleSheet(QString qss);

private:
};
