#include "Button.h"
#include <QDebug>
#include <QIcon>

Button::Button(QWidget* parent)
	: QPushButton(parent)
{
	//this->resize(80, 36);
	this->setFixedHeight(36);
	QString css = StyleHelper::getStyleSheet(StyleConfig::COMMON_BUTTON_STYLE());
	this->setStyleSheet(css);
}
/**
* 设置自定义按钮样式
* 样式直接从StyleConfig中获取，里面存放了一些通用样式
*/
void Button::setCustomStyleSheet(QString qss) {
	QString css = StyleHelper::getStyleSheet(qss);
	this->setStyleSheet(css);
}

Button::~Button()
{
}
