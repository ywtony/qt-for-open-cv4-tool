#include "CommonListViewItem.h"

CommonListViewItem::CommonListViewItem(QString itemTitle, const QIcon& icon, const QIcon& iconHover, int pos, QListWidget* parent)
	: QListWidgetItem(parent)
{
    setText(itemTitle);
    mIcon = icon;
    mIconHover = iconHover;
    setIcon(mIcon);
    setSizeHint(QSize(360, 47));
    mPos = pos;
}
//void  CommonListViewItem::itemClicked(QListWidgetItem* item) {
//    Utils::log("�������Item");
//}
CommonListViewItem::~CommonListViewItem()
{
}
