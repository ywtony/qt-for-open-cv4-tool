#include "CommonGraphicsView.h"

CommonGraphicsView::CommonGraphicsView(QWidget *parent)
	: QWidget(parent)
{
    cornerHarrs = new CornerHarris();
    this->setAcceptDrops(true);//允许往窗口拖放
    this->setFixedSize(QSize(320, 480));
    view = new QGraphicsView(this);//实例化View
    view->setMouseTracking(true);
    view->setFixedSize(this->size());
    view->setAcceptDrops(false);
    view->setScene(&scene);//给View设置场景

    //设置槽函数以监听阀值变化
//    connect(slider,&QSlider::valueChanged,[=](int value){
//        qDebug()<<"value:"<<value;
//        onHSliderValueChanged(value);//执行角点检测
//    });
}

void CommonGraphicsView::dragEnterEvent(QDragEnterEvent* event) {
    QStringList acceptedFileTypes;
    acceptedFileTypes.append("jpeg");
    acceptedFileTypes.append("png");
    acceptedFileTypes.append("bmp");
    acceptedFileTypes.append("jpg");
    if (event->mimeData()->hasUrls() && event->mimeData()->urls().count() == 1) {
        QFileInfo file(event->mimeData()->urls().at(0).toLocalFile());
        if (acceptedFileTypes.contains(file.suffix().toLower())) {
            event->acceptProposedAction();
        }
    }
    else {
        QMessageBox::critical(this, "Error", "count not load mulit files");
    }
}

void CommonGraphicsView::dropEvent(QDropEvent* event) {
    scene.clear();
    QFileInfo file(event->mimeData()->urls().at(0).toLocalFile());
    //    QPixmap pixmap;
    //    if(pixmap.load(file.absoluteFilePath())){
    //        scene.addPixmap(pixmap.scaled(this->width(),this->height(),Qt::KeepAspectRatio,Qt::SmoothTransformation));

    filePath = file.absoluteFilePath();
    //        onHSliderValueChanged(130);
    //    }else{
    //        QMessageBox::critical(this,tr("Error"),tr("Error count not load file"));
    //    }


}

void CommonGraphicsView::resizeEvent(QResizeEvent* event) {

}

void CommonGraphicsView::onHSliderValueChanged(int value) {
    //    cornerHarrs->showCornerHarris(filePath.toStdString().c_str(),value,resultImage);
    QImage image = ImageUtils::matToQImage(resultImage);
    QPixmap pixmap = QPixmap::fromImage(image);
    QGraphicsPixmapItem* item = new QGraphicsPixmapItem(pixmap.scaled(this->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
    scene.addItem(item);
    qDebug() << "image count " << scene.items().count();

}

CommonGraphicsView::~CommonGraphicsView()
{
}
