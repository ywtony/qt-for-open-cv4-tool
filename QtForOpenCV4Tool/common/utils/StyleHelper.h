#pragma once
#include <QFile>
#include <QString>
#include <QDebug>

class StyleHelper {
public:
	static QString getStyleSheet(QString filePath) {
		qDebug() << filePath;
		//加载qss文件
		QFile qss(filePath);
		if (qss.open(QFile::ReadOnly)) {//打开
			qDebug() << "already open file";
			QString styleSheet = QLatin1String(qss.readAll());
			qDebug() << styleSheet;
			return styleSheet;
		}
		else {
			qDebug() << "could not open file";
		}
		return nullptr;
	}
};