#pragma once
#include <QString>

class StyleConfig {
public:
	static QString COMMON_BUTTON_STYLE() {
		return ":/QtForOpenCV4Tool/common/qss/button.qss";
	}
	static QString COMMON_BUTTON_SHAPE_BLACK_R10() {
		return ":/QtForOpenCV4Tool/common/qss/button_shape_black_r10.qss";
	}
	static QString COMMON_BUTTON_IMAGE_BG() {
		return ":/QtForOpenCV4Tool/common/qss/button_bg.qss";
	}
	static QString COMMON_EDITTEXT_DEFAULT() {
		return ":/QtForOpenCV4Tool/common/qss/edittext.qss";
	}
	static QString COMMON_RADIOBUTTON_DEFAULT() {
		return ":/QtForOpenCV4Tool/common/qss/radiobutton.qss";
	}

	static QString COMMON_SCROLLVIEW_DEFAULT() {
		return ":/QtForOpenCV4Tool/common/qss/scrollbar.qss";
	}
	static QString COMMON_COMBOBOX_DEFAULT() {
		return ":/QtForOpenCV4Tool/common/qss/combobox.qss";
	}

	static QString COMMON_SEEKBAR_DEFAULT() {
		return ":/QtForOpenCV4Tool/common/qss/seekbar.qss";
	}
};