#pragma once
#include <QString>
#include <QFileDialog>
#include <QWidget>
#include <functional>
/// <summary>
/// C++设计模式：https://blog.csdn.net/weixin_40514381/article/details/142846878
/// C++多态详解：https://blog.csdn.net/m0_72532428/article/details/139437834
/// </summary>
class DialogUtils {
public:
	DialogUtils() {}
	~DialogUtils() {
	}

public:
	/// <summary>
	/// 展示视频选择框
	/// </summary>
	/// <param name="qWidght"></param>
	/// <param name="callback"></param>
	static void showVideo(QWidget *qWidght,std::function<void(QString)> callback) {
		QString filePath = QFileDialog::getOpenFileName(qWidght, "请选择视频", "", "视频文件(*.mp4 * .avi * .mkv * .flv)");
		callback(filePath);
	}
	/// <summary>
	/// 展示图片选择框
	/// </summary>
	/// <param name="function"></param>
	/// <param name=""></param>
	static void showImage(QWidget* qWidght, std::function<void(QString)> callback) {
		QString filePath = QFileDialog::getOpenFileName(qWidght, "请选择图片", "C:/Users/DBF-DEV-103/Downloads/", "Image Files(*.jpg *.png *.webp *.jpeg)");
		callback(filePath);
	}

};