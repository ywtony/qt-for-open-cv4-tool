#include "ImageUtils.h"

Mat ImageUtils::qImageToMat(QImage qImage) {//QImage转Mat三通道图片
	QImage image = qImage.convertToFormat(QImage::Format_RGB888);
	Mat mat = Mat(image.height(), image.width(), CV_8UC3, image.bits(), image.bytesPerLine());
	return mat;
}

QImage ImageUtils::matToQImage(Mat src) {
	cvtColor(src, src, COLOR_BGR2RGB);//转换色彩空间，因为Mar默认是BGR，要转换为RGB
	QImage qImage(src.data, src.cols, src.rows, src.step, QImage::Format_RGB888);
	return qImage;
}

QImage ImageUtils::matToQImage8(Mat src) {//QImage显示Mat的8位单通道图片
	QImage qimage(src.data, src.cols, src.rows, src.step, QImage::Format_Grayscale8);
	return qimage;
}

QPixmap ImageUtils::getPixmap(Mat mat, int width, int height) {
	QPixmap pixmap = QPixmap::fromImage(ImageUtils::matToQImage(mat));
	return pixmap.scaled(QSize(width, height), Qt::KeepAspectRatio);
}
QPixmap ImageUtils::getPixmap8(Mat mat, int width, int height) {
	QPixmap pixmap = QPixmap::fromImage(ImageUtils::matToQImage8(mat));
	return pixmap.scaled(QSize(width, height), Qt::KeepAspectRatio);
}

QPixmap ImageUtils::getPixmap(Mat mat) {
	QPixmap pixmap = QPixmap::fromImage(ImageUtils::matToQImage(mat));
	return pixmap;
}
QPixmap ImageUtils::getPixmap8(Mat mat) {
	QPixmap pixmap = QPixmap::fromImage(ImageUtils::matToQImage8(mat));
	return pixmap;
}
