#ifndef IMAGEUTILS_H
#define IMAGEUTILS_H
#include "opencv2/opencv.hpp"
#include "opencv2/highgui.hpp"
#include <QImage>
#include <QPixmap>
using namespace cv;

class ImageUtils{

public:
    /**
     * 将Mat转为QImage
     * @brief matToQImage
     * @param src
     * @return
     */
    static QImage matToQImage(Mat src);
    /**
    * 把mat单通道图片转转换为QImage
    */
    static QImage matToQImage8(Mat src);
    /**
     * 将Mat转换为QImage对象
     * @brief qImageToMat
     * @param qImage
     * @return
     */
    static Mat qImageToMat(QImage qImage);
    static QPixmap getPixmap(Mat mat, int width, int height);
    static QPixmap getPixmap8(Mat mat, int width, int height);
    static QPixmap getPixmap(Mat mat);
    static QPixmap getPixmap8(Mat mat);
};



#endif // IMAGEUTILS_H
