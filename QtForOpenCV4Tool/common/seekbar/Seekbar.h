#pragma once

#include <QSlider>
#include "../utils/StyleConfig.h"
#include "../utils/StyleHelper.h"
#include <QIntValidator>

class Seekbar : public QSlider
{
	Q_OBJECT

public:
	Seekbar(QWidget *parent = nullptr);
	~Seekbar();
public:
	void setValueRange(int min, int max, int step);
private:
};
