#pragma once

#include <QWidget>
#include "../../common/button/Button.h"
#include "../../common/edittext/EditText.h"
#include "../../common/seekbar/Seekbar.h"
#include <QFileDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QVBoxLayout>
#include <iostream>
#include <functional>

class SeekBarTipsWidget : public QWidget
{
	Q_OBJECT

public:
	SeekBarTipsWidget(QWidget *parent = nullptr);
	~SeekBarTipsWidget();

public:
	void initContent(int seekBarMin,int seekbarMax,int seekbarStep,QString tips, std::function<void(int)> callback);
	void setSeekBarValue(int value);
private:
	QLabel* labelTips;
	Seekbar* seekbar;
	int value;
	std::function<void(int)> callback;
};
