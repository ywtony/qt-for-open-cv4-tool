#include "Seekbar.h"

Seekbar::Seekbar(QWidget *parent)
	: QSlider(parent)
{
	this->setOrientation(Qt::Horizontal);
	this->setMinimum(0);
	this->setMaximum(100);
	this->setSingleStep(1);
	QString css = StyleHelper::getStyleSheet(StyleConfig::COMMON_SEEKBAR_DEFAULT());
	this->setStyleSheet(css);
}

void Seekbar::setValueRange(int min, int max,int step) {
	this->setMinimum(min);
	this->setMaximum(max);
	this->setSingleStep(step);
}

Seekbar::~Seekbar()
{

}
