#include "SeekBarTipsWidget.h"

SeekBarTipsWidget::SeekBarTipsWidget(QWidget* parent)
	: QWidget(parent)
{
	QVBoxLayout* vLayout = new QVBoxLayout(this);
	labelTips = new QLabel(this);
	seekbar = new Seekbar(this);
	vLayout->addWidget(labelTips);
	vLayout->addWidget(seekbar);

	this->setLayout(vLayout);

	connect(seekbar, &Seekbar::valueChanged, this, [=](int value) {
		this->callback(value);
		qDebug() << "value:" << value;
		});
}

void SeekBarTipsWidget::initContent(int seekbarMin, int seekbarMax, int seekbarStep, QString tips, std::function<void(int)> callback) {
	this->seekbar->setValueRange(seekbarMin, seekbarMax, seekbarStep);
	this->labelTips->setText(tips);
	this->callback = callback;
}

void SeekBarTipsWidget::setSeekBarValue(int value) {
	this->seekbar->setValue(value);
}
SeekBarTipsWidget::~SeekBarTipsWidget()
{

}
