#pragma once

#include <QListWidget>
#include <QMouseEvent>
#include <QEvent>
#include <QPoint>
#include <QString>
#include <QFileDialog>
#include <QDebug>
#include <QObject>
#include "CommonListViewItem.h"
#include <QListWidgetItem>
class CommonListView :public QListWidget
{
	Q_OBJECT

public:
	CommonListView(QWidget* parent = nullptr);
	~CommonListView();

private:
	QPoint startPoint;
	CommonListViewItem* theHighlightItem = nullptr;
	CommonListViewItem* oldHighlightItem = nullptr;
	CommonListViewItem* theSelectedItem = nullptr;
	CommonListViewItem* oldSelectedItem = nullptr;


signals:
	//void itemClicked(CommonListViewItem* item);

public slots:
	//void onItemClicked(QListWidgetItem* item);
	void updateSelectedIcon();


};
