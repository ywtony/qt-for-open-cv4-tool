#include "VariableGridView.h"


VariableGridView::VariableGridView(QWidget* parent)
	: QWidget(parent)
{
}

//设置数据源
void VariableGridView::setViews(int mSize, int type) {
	this->mWidth = this->width();
	this->mHeight = this->height();
	for (int i = 0;i < mSize;i++) {
		QLabel* label = new QLabel(this);
		label->setScaledContents(true);
		imageTips.push_back(label);
	}

	if (mSize > 0) {
		switch (mSize) {
		case 1: {
			QLabel* tip = imageTips[0];
			tip->setScaledContents(true);
			if (type == 1) {
				tip->resize(mWidth, mHeight);
			}
			else {
				tip->resize(mWidth / 2, mHeight / 2);
			}
			tip->move(0, 0);
		}
			  break;
		case 2: {
			QLabel* tip = imageTips[0];
			tip->setScaledContents(true);
			if (type == 1) {
				tip->resize((mWidth - 20) / 2, mHeight);
			}
			else {
				tip->resize((mWidth - 20) / 2, mHeight / 2);
			}

			tip->move(0, 0);
			QLabel* tip1 = imageTips[1];
			tip1->setScaledContents(true);
			if (type == 1) {
				tip1->resize((mWidth - 20) / 2, mHeight);
			}
			else {
				tip1->resize((mWidth - 20) / 2, mHeight / 2);
			}
			tip1->move(tip->x() + tip->width() + 5, 0);
		}
			  break;
		case 3: {
			QLabel* tip = imageTips[0];
			tip->setScaledContents(true);
			tip->resize(mWidth / 2, mHeight / 2);
			tip->move(0, 0);
			QLabel* tip1 = imageTips[1];
			tip1->setScaledContents(true);
			tip1->resize(mWidth / 2, mHeight / 2);
			tip1->move(tip->x() + tip->width() + 5, 0);
			QLabel* tip2 = imageTips[2];
			tip2->setScaledContents(true);
			tip2->resize(mWidth / 2, mHeight / 2);
			tip2->move(0, tip1->y() + 5 + tip1->height());
		}
			  break;
		case 4: {
			QLabel* tip = imageTips[0];
			tip->setScaledContents(true);
			tip->resize(mWidth / 2, mHeight / 2);
			tip->move(0, 0);
			QLabel* tip1 = imageTips[1];
			tip1->setScaledContents(true);
			tip1->resize(mWidth / 2, mHeight / 2);
			tip1->move(tip->x() + tip->width() + 5, 0);
			QLabel* tip2 = imageTips[2];
			tip2->setScaledContents(true);
			tip2->resize(mWidth / 2, mHeight / 2);
			tip2->move(0, tip1->y() + 5 + tip1->height());
			QLabel* tip3 = imageTips[3];
			tip3->setScaledContents(true);
			tip3->resize(mWidth / 2, mHeight / 2);
			tip3->move(tip2->x() + tip2->width() + 5, tip1->y() + tip1->height() + 5);
		}
			  break;
		case 5: {
			QLabel* tip = imageTips[0];
			tip->setScaledContents(true);
			tip->resize(mWidth / 3, mHeight / 2);
			tip->move(0, 0);
			QLabel* tip1 = imageTips[1];
			tip1->setScaledContents(true);
			tip1->resize(mWidth / 3, mHeight / 2);
			tip1->move(tip->x() + tip->width() + 5, 0);
			QLabel* tip2 = imageTips[2];
			tip2->setScaledContents(true);
			tip2->resize(mWidth / 3, mHeight / 2);
			tip2->move(tip1->x() + tip1->width() + 5, 0);

			QLabel* tip3 = imageTips[3];
			tip3->setScaledContents(true);
			tip3->resize(mWidth / 3, mHeight / 2);
			tip3->move(0, tip->y() + tip->height() + 5);
			QLabel* tip4 = imageTips[4];
			tip4->setScaledContents(true);
			tip4->resize(mWidth / 3, mHeight / 2);
			tip4->move(tip3->x() + tip3->width() + 5, tip->y() + tip->height() + 5);
		}
			  break;
		case 6: {
			QLabel* tip = imageTips[0];
			tip->setScaledContents(true);
			tip->resize(mWidth / 3, mHeight / 2);
			tip->move(0, 0);
			QLabel* tip1 = imageTips[1];
			tip1->setScaledContents(true);
			tip1->resize(mWidth / 3, mHeight / 2);
			tip1->move(tip->x() + tip->width() + 5, 0);
			QLabel* tip2 = imageTips[2];
			tip2->setScaledContents(true);
			tip2->resize(mWidth / 3, mHeight / 2);
			tip2->move(tip1->x() + tip1->width() + 5, 0);

			QLabel* tip3 = imageTips[3];
			tip3->setScaledContents(true);
			tip3->resize(mWidth / 3, mHeight / 2);
			tip3->move(0, tip->y() + tip->height() + 5);
			QLabel* tip4 = imageTips[4];
			tip4->setScaledContents(true);
			tip4->resize(mWidth / 3, mHeight / 2);
			tip4->move(tip3->x() + tip3->width() + 5, tip->y() + tip->height() + 5);
			QLabel* tip5 = imageTips[5];
			tip5->setScaledContents(true);
			tip5->resize(mWidth / 3, mHeight / 2);
			tip5->move(tip4->x() + tip4->width() + 5, tip->y() + tip->height() + 5);
		}
			  break;
		case 7: {
			QLabel* tip = imageTips[0];
			tip->setScaledContents(true);
			tip->resize(mWidth / 3, mHeight / 3);
			tip->move(0, 0);
			QLabel* tip1 = imageTips[1];
			tip1->setScaledContents(true);
			tip1->resize(mWidth / 3, mHeight / 3);
			tip1->move(tip->x() + tip->width() + 5, 0);
			QLabel* tip2 = imageTips[2];
			tip2->setScaledContents(true);
			tip2->resize(mWidth / 3, mHeight / 3);
			tip2->move(tip1->x() + tip1->width() + 5, 0);

			QLabel* tip3 = imageTips[3];
			tip3->setScaledContents(true);
			tip3->resize(mWidth / 3, mHeight / 3);
			tip3->move(0, tip->y() + tip->height() + 5);
			QLabel* tip4 = imageTips[4];
			tip4->setScaledContents(true);
			tip4->resize(mWidth / 3, mHeight / 3);
			tip4->move(tip3->x() + tip3->width() + 5, tip->y() + tip->height() + 5);
			QLabel* tip5 = imageTips[5];
			tip5->setScaledContents(true);
			tip5->resize(mWidth / 3, mHeight / 3);
			tip5->move(tip4->x() + tip4->width() + 5, tip->y() + tip->height() + 5);

			QLabel* tip6 = imageTips[6];
			tip6->setScaledContents(true);
			tip6->resize(mWidth / 3, mHeight / 3);
			tip6->move(0, tip5->y() + tip5->height() + 5);
		}
			  break;
		case 8: {
			QLabel* tip = imageTips[0];
			tip->setScaledContents(true);
			tip->resize(mWidth / 3, mHeight / 3);
			tip->move(0, 0);
			QLabel* tip1 = imageTips[1];
			tip1->setScaledContents(true);
			tip1->resize(mWidth / 3, mHeight / 3);
			tip1->move(tip->x() + tip->width() + 5, 0);
			QLabel* tip2 = imageTips[2];
			tip2->setScaledContents(true);
			tip2->resize(mWidth / 3, mHeight / 3);
			tip2->move(tip1->x() + tip1->width() + 5, 0);

			QLabel* tip3 = imageTips[3];
			tip3->setScaledContents(true);
			tip3->resize(mWidth / 3, mHeight / 3);
			tip3->move(0, tip->y() + tip->height() + 5);
			QLabel* tip4 = imageTips[4];
			tip4->setScaledContents(true);
			tip4->resize(mWidth / 3, mHeight / 3);
			tip4->move(tip3->x() + tip3->width() + 5, tip->y() + tip->height() + 5);
			QLabel* tip5 = imageTips[5];
			tip5->setScaledContents(true);
			tip5->resize(mWidth / 3, mHeight / 3);
			tip5->move(tip4->x() + tip4->width() + 5, tip->y() + tip->height() + 5);

			QLabel* tip6 = imageTips[6];
			tip6->setScaledContents(true);
			tip6->resize(mWidth / 3, mHeight / 3);
			tip6->move(0, tip5->y() + tip5->height() + 5);
			QLabel* tip7 = imageTips[7];
			tip7->setScaledContents(true);
			tip7->resize(mWidth / 3, mHeight / 3);
			tip7->move(tip6->x() + tip6->width() + 5, tip5->y() + tip5->height() + 5);
		}
			  break;
		case 9: {
			int itemWidth = (mWidth - 50) / 3;
			int itemHeight = (mHeight - 50) / 3;
			QLabel* tip = imageTips[0];
			tip->setScaledContents(true);
			tip->resize(itemWidth, itemHeight);
			tip->move(0, 0);
			QLabel* tip1 = imageTips[1];
			tip1->setScaledContents(true);
			tip1->resize(itemWidth, itemHeight);
			tip1->move(tip->x() + tip->width() + 5, 0);
			QLabel* tip2 = imageTips[2];
			tip2->setScaledContents(true);
			tip2->resize(itemWidth, itemHeight);
			tip2->move(tip1->x() + tip1->width() + 5, 0);

			QLabel* tip3 = imageTips[3];
			tip3->setScaledContents(true);
			tip3->resize(itemWidth, itemHeight);
			tip3->move(0, tip->y() + tip->height() + 5);
			QLabel* tip4 = imageTips[4];
			tip4->setScaledContents(true);
			tip4->resize(itemWidth, itemHeight);
			tip4->move(tip3->x() + tip3->width() + 5, tip->y() + tip->height() + 5);
			QLabel* tip5 = imageTips[5];
			tip5->setScaledContents(true);
			tip5->resize(itemWidth, itemHeight);
			tip5->move(tip4->x() + tip4->width() + 5, tip->y() + tip->height() + 5);

			QLabel* tip6 = imageTips[6];
			tip6->setScaledContents(true);
			tip6->resize(itemWidth, itemHeight);
			tip6->move(0, tip5->y() + tip5->height() + 5);
			QLabel* tip7 = imageTips[7];
			tip7->setScaledContents(true);
			tip7->resize(itemWidth, itemHeight);
			tip7->move(tip6->x() + tip6->width() + 5, tip5->y() + tip5->height() + 5);
			QLabel* tip8 = imageTips[8];
			tip8->setScaledContents(true);
			tip8->resize(itemWidth, itemHeight);
			tip8->move(tip7->x() + tip7->width() + 5, tip5->y() + tip5->height() + 5);
		}
			  break;
		}
	}
}

void VariableGridView::setAdapter(vector<QPixmap> pixmaps) {
	for (int i = 0;i < pixmaps.size();i++) {
		this->imageTips[i]->setPixmap(pixmaps[i]);
		//this->imageTips[i]->setItemsPixmap(pixmaps[i]);
	}
}

VariableGridView::~VariableGridView()
{

}
