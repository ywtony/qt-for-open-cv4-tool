#pragma once

#include <QWidget>
#include <QDebug>
#include "../image_tip/ImageTip.h"
#include <QHBoxLayout>
#include <QSize>
#include <vector>
#include <list>
#include <array>

using namespace std;

class VariableGridView : public QWidget
{
	Q_OBJECT

public:
	VariableGridView(QWidget* parent = nullptr);
	~VariableGridView();
public:
	/**
	* @param mSize item个数
	* @param type 0九宫格 1自定义
	*/
	void setViews(int mSize,int type);
	void setAdapter(vector<QPixmap> pixmaps);

private:
	int mWidth;
	int mHeight;
	vector<QLabel*> imageTips;
};
