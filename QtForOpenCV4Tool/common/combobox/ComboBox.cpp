#include "ComboBox.h"

ComboBox::ComboBox(QWidget *parent)
	: QComboBox(parent)
{
	//这里设置一个默认的样式
	QString css = StyleHelper::getStyleSheet(StyleConfig::COMMON_COMBOBOX_DEFAULT());
	this->setStyleSheet(css);
}

ComboBox::~ComboBox()
{
}
