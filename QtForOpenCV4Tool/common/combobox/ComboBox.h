#pragma once

#include <QComboBox>
#include "../utils/StyleConfig.h"
#include "../utils/StyleHelper.h"

class ComboBox : public QComboBox
{
	Q_OBJECT

public:
	ComboBox(QWidget *parent = nullptr);
	~ComboBox();

private:
};
