#pragma once
#include <QDebug>
#include <QThread>
#include <iostream>
#include <functional>

class WorkerThread :QThread
{
	Q_OBJECT

public:
	std::function<void()> runThreadFunc;
public:
	void run() override {
		runThreadFunc();
	}

};