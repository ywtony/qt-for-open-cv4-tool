#include "HandlerThread.h"

HandlerThread::HandlerThread(QObject* parent, Handler* handler)
	: QThread(parent)
{
	qRegisterMetaType<cv::Mat>("cv::Mat");
	qRegisterMetaType<QList<cv::Mat*>>("QList<cv::Mat*>");
	this->handler = handler;
}

void HandlerThread::run() {
	QPixmap pixmap = handler->handle();
	emit updateUI(pixmap);
}

HandlerThread::~HandlerThread()
{

}
