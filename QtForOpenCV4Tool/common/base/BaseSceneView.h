#pragma once

#include <stdint.h>
#include <QWidget>
#include <QGraphicsScene>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QResizeEvent>
#include <QSize>
#include <QGraphicsView>
#include <QMessageBox>
#include <QMimeData>
#include <QFileInfo>
#include <QStringList>
#include <QGraphicsPixmapItem>
#include <QDebug>
#include <QGraphicsEllipseItem>
#include <QGraphicsLineItem>
#include <QGraphicsRectItem>
#include <QGraphicsPolygonItem>
#include <QSlider>
#include <QDebug>
#include <QPixmap>
#include <QImage>
#include <opencv2/opencv.hpp>
#include "../utils/ImageUtils.h"
#include "HandlerThread.h"

class BaseSceneView : public QWidget,public Handler
{
	Q_OBJECT

public:
	BaseSceneView(QWidget* parent = nullptr);
	~BaseSceneView();
	HandlerThread* mThread;

private:
	QGraphicsView* view;
	QGraphicsScene scene;
	

protected:
	void dragEnterEvent(QDragEnterEvent* event);
	void dropEvent(QDropEvent* event);
	void resizeEvent(QResizeEvent* event);
	
public:
	void setGraphicsViewSize(int width, int height);
public:
	//调用此方法开始执行耗时任务，此方法必须调用否则耗时任务不执行，
	//此处的耗时任务就是,handle()方法中的代码块
	void execute();//执行耗时任务
	//此方法必须要重写，需要把耗时任务放进去
	QPixmap handle();
	

private:
	//向场景中添加Bitmap
	void addPixmap(QPixmap pixmap);
	void addImage(QImage image);
	void addMat(cv::Mat mat);
	//更新UI4
	void updateImage(QPixmap pixmap);

public slots:
	void onHSliderValueChanged(int value);



private:
};
