#pragma once

#include <QThread>
#include <QPixmap>
#include "Handler.h"
#include <opencv2/opencv.hpp>
#include <QList>
#include <QMetaType> 

class HandlerThread : public QThread
{
	Q_OBJECT

public:
	HandlerThread(QObject* parent, Handler* handler);
	~HandlerThread();

private:
	Handler* handler;

protected:
	void run();//线程执行体

signals:
	void updateUI(QPixmap pixmap);//发送更新UI的信号
	void done(cv::Mat* mats);

};
