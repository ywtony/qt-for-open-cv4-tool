#pragma once

#include <QWidget>
#include <QLabel>
#include <QVBoxLayout>
#include <QString>
#include <QPixmap>

class ImageTip : public QWidget
{
	Q_OBJECT

public:
	ImageTip(QWidget* parent = nullptr);
	~ImageTip();
	void setItemsContent(QString title, QPixmap pixmap);
	void setItemsTitle(QString title);
	void setItemsPixmap(QPixmap pixmap);
	void resizeWH(int width, int height);
	void resizeFixedWH(int width, int height);

private:
	QLabel* tip;
	QLabel* imageView;

};
