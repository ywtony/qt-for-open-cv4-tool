#include "ImageTip.h"

ImageTip::ImageTip(QWidget* parent)
	: QWidget(parent)
{
	QVBoxLayout* vLayout = new QVBoxLayout(this);
	tip = new QLabel(this);
	imageView = new QLabel(this);
	vLayout->addWidget(tip);
	vLayout->addWidget(imageView);
	this->setLayout(vLayout);
}

void ImageTip::setItemsContent(QString title, QPixmap pixmap) {
	this->tip->setText(title);
	this->imageView->setPixmap(pixmap);
}

void ImageTip::setItemsTitle(QString title) {
	this->tip->setText(title);
}

void ImageTip::setItemsPixmap(QPixmap pixmap) {
	this->imageView->setPixmap(pixmap);
}

void ImageTip::resizeWH(int width, int height) {
	this->imageView->resize(width / 3 * 2, height - 50);
}

void ImageTip::resizeFixedWH(int width, int height) {
	this->tip->setVisible(true);
	this->setFixedSize(width, height);
	this->imageView->setFixedSize(width, height);
}

ImageTip::~ImageTip()
{
}
